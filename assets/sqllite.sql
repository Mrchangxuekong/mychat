
--messages表包含了个人消息和群组消息的内容，字段如下：
--id: 每条消息的唯一标识，使用INTEGER类型，并设置为主键。
--sender_id: 发送者的ID，使用INTEGER类型。
--receiver_id: 接收者的ID，使用INTEGER类型。对于个人消息，这个字段表示接收者的ID；对于群组消息，这个字段表示群组的ID。
--content: 消息内容，使用TEXT类型。
--timestamp: 消息发送时间戳，使用INTEGER类型。
--is_read: 消息是否已读，使用INTEGER类型表示，可以是0或1，其中0表示未读，1表示已读。
--is_group_message: 标识消息是否为群组消息，使用INTEGER类型表示，可以是0或1，其中0表示个人消息，1表示群组消息。
--groups表用于存储群组信息，字段如下：
--id: 群组的唯一标识，使用INTEGER类型，并设置为主键。
--name: 群组的名称，使用TEXT类型。
--group_members表用于存储群组成员信息，字段如下：
--id: 每个成员的唯一标识，使用INTEGER类型，并设置为主键。
--group_id: 成员所属的群组ID，使用INTEGER类型。
--member_id: 成员的ID，使用INTEGER类型。

CREATE TABLE messages (
    id INTEGER PRIMARY KEY AUtoin,
    sender INTEGER NOT NULL,
    receiver INTEGER NOT NULL,
    content TEXT,
    timestamp INTEGER,
    is_read INTEGER,
    is_group_message INTEGER
);

CREATE TABLE groups (
    id INTEGER PRIMARY KEY,
    name TEXT
);

CREATE TABLE group_members (
    id INTEGER PRIMARY KEY,
    group_id INTEGER,
    member_id INTEGER
);
