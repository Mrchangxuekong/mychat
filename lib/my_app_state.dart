
class MyAppState {
   static MyPermanentObject? _permanentObject;

  // 静态方法来实例化永久对象
  static void initializePermanentObject() {
    _permanentObject = MyPermanentObject();
  }

  // 获取永久对象的方法
  static MyPermanentObject? get permanentObject => _permanentObject;
}

class MyPermanentObject {

}