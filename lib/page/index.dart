import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/local_img_path.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/color/themeText.dart';
import 'package:zhongdingapp/routerHelper.dart';

class Index extends StatelessWidget {
  const Index({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
            child: Image.asset(
          LocalImgPath.initLoginBg,
          fit: BoxFit.cover,
        )),
        Positioned(
          bottom: ThemeText.double20,
          left: 20,
          child: loginButton(
            "登录",
            ThemeColor.mainGreen,
            1,
          ),
        ),
        Positioned(
          bottom: ThemeText.double20,
          right: ThemeText.double20,
          child: loginButton("注册", ThemeColor.black1, ThemeText.int2),
        )
      ],
    );
  }
}

@override
Widget build1(BuildContext context) {
  return GetMaterialApp(
      home: Stack(
    children: [
      Positioned.fill(
          child: Image.asset(
            LocalImgPath.initLoginBg,
        fit: BoxFit.cover,
      )),
      Positioned(
        bottom: ThemeText.double20,
        left: ThemeText.double20,
        child: loginButton(
          "登录",
          Colors.green,
          1,
        ),
      ),
      Positioned(
        bottom: ThemeText.double20,
        right: ThemeText.double20,
        child: loginButton("注册", ThemeColor.black1, ThemeText.int2),
      )
    ],
  ));
}

Widget loginButton(String name, Color color, fn) {
  return ElevatedButton(
    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(color)),
    onPressed: fn == 1 ? loginfn : registfn,
    child: Text(
      name,
      style: const TextStyle(
          fontSize: ThemeText.double17, color: ThemeColor.white),
    ),
  );
}

loginfn() {
  Get.toNamed(RouterHelper.login);
}

registfn() {
  Get.toNamed(RouterHelper.registe);
}
