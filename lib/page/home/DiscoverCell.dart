import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/themeColor.dart';

//自定义组件cell  页面跳转可调用
class DiscoverCell extends StatefulWidget {
  final String title;
  final String imageName;
  final String? subTitle;
  final String? subImageName;
  const DiscoverCell(
      {super.key,
      required this.title,
      this.subTitle,
      required this.imageName,
      this.subImageName});

  @override
  State<DiscoverCell> createState() => _DiscoverCellState();
}

class _DiscoverCellState extends State<DiscoverCell> {
  Color _cellColor = Colors.white;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) =>
                DiscoverCell(title: widget.title, imageName: widget.imageName),
          ),
        );
        _cellColor = Colors.white;
        setState(() {});
      },
      onTapCancel: () {
        _cellColor = Colors.white;
        setState(() {});
      },
      onTapDown: (TapDownDetails details) {
        _cellColor = ThemeColor.white;
        setState(() {});
      },
      child: Container(
        color: _cellColor,
        height: 54,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  //left
                  Image(
                    image: AssetImage(widget.imageName),
                    height: 25,
                  ),
                  const SizedBox(width: 15),
                  Text(widget.title)
                ],
              ),
            ),
            //right
            Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  Text(widget.subTitle ?? "",
                      style: const TextStyle(color: Colors.grey)),
                  widget.subImageName != null
                      ? Image(
                          image: AssetImage(widget.subImageName!), width: 15)
                      : Container(),
                  const Image(
                      image: AssetImage("images/wx/t15.png"), width: 20),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
