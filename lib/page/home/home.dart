import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/color/themeText.dart';
import 'package:zhongdingapp/controller/communication_controller.dart';
import 'package:zhongdingapp/controller/homeController.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';
import 'package:zhongdingapp/page/home/message/message/message_logic.dart';
import 'package:zhongdingapp/routerHelper.dart';
import 'package:zhongdingapp/widget/actions._widget.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    MessageLogic messageLogic = Get.put(MessageLogic());
    HomeController controller = Get.find<HomeController>();
    controller.onReady();
    double markMessage = (MediaQuery.of(context).size.width / 4 / 2) + 2;
    double markPhoneBook = (MediaQuery.of(context).size.width / 2) - (MediaQuery.of(context).size.width / 8) + 4;

    return GetBuilder<HomeController>(builder: (c)=>  Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          //关闭appbar的返回按钮
          actions: actions(),
          titleTextStyle: const TextStyle(
              fontSize: ThemeText.appbarTextSize, color: ThemeColor.gray200),
          centerTitle: true,
          title: Text(controller.list.value[controller.currPageIndex.value]
                ['title'] as String),
          ),
        body: controller.list.value[controller.currPageIndex.value]['page'] as Widget,
        bottomNavigationBar:Stack(
              children: [
                BottomNavigationBar(
                  selectedItemColor: ThemeColor.mainGreen,
                  currentIndex: controller.currPageIndex.value,
                  unselectedItemColor: ThemeColor.black1,
                  onTap: (currPageIndex) {
                    controller.getInitData(currPageIndex);
                  },
                  items: listItem(),
                  type: BottomNavigationBarType.fixed,
                ),
                addRedMark(markMessage, messageLogic.getButtonNum()),
                addRedMark(
                    markPhoneBook, controller.newFriendNum.value.toString()),
              ],
            )));
  }

  List<BottomNavigationBarItem> listItem() {
    List<BottomNavigationBarItem> list = [];
    BottomNavigationBarItem message = const BottomNavigationBarItem(
      icon: Icon(MyIcons.message),
      label: "消息",
    );
    list.add(message);
    BottomNavigationBarItem book = const BottomNavigationBarItem(
        icon: Icon(MyIcons.phoneBook), label: "通讯录");
    list.add(book);

    BottomNavigationBarItem find =
        const BottomNavigationBarItem(icon: Icon(MyIcons.find), label: "发现");
    list.add(find);
    BottomNavigationBarItem my = const BottomNavigationBarItem(
        icon: Icon(MyIcons.myicon), label: "我", tooltip: "1");
    list.add(my);
    return list;
  }

  List<Widget> actions() {
    List<ItemTitle> actionsItems = [];
    actionsItems.add(ItemTitle(
        title: "发起群聊",
        icon: MyIcons.addGroup1,
        dismissHandle: () {
          Get.toNamed(RouterHelper.selectContactsPage);
        }));
    actionsItems.add(ItemTitle(
        title: "添加朋友",
        icon: MyIcons.newfriend,
        dismissHandle: () {
          Get.toNamed(RouterHelper.addFriend);
        }));
    actionsItems.add(ItemTitle(
        title: "扫一扫",
        icon: MyIcons.codepick,
        dismissHandle: () {
          Get.snackbar("提示", "点击了扫一扫");
        }));
    List<Widget> list = [];
    ItemTitle tips = ItemTitle(
        title: "搜索按钮",
        icon: MyIcons.codepick,
        dismissHandle: () {
          print("扫一扫");
        });
    Container row = Container(
      padding: const EdgeInsets.only(right: ThemeText.double18),
      child: Row(
        children: [
          InkWell(
            child: const Icon(
              Icons.search,
              size: ThemeText.double26,
            ),
            onTap: () => {Get.toNamed(RouterHelper.search)},
          ),
          const SizedBox(width: ThemeText.double9), // 50宽度
          ActionsWidget(tip: tips, listItem: actionsItems)
        ],
      ),
    );
    list.add(row);
    return list;
  }

  ///添加底部红点
  addRedMark(double markMessage, String num) {
    if (num == "0") {
      return const Text("");
    }
    var remark = Positioned(
        left: markMessage,
        top: 2,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
          decoration: BoxDecoration(
              color: Colors.redAccent, borderRadius: BorderRadius.circular(18)),
          child: Text(
            num,
            style: const TextStyle(color: Colors.white, fontSize: 8),
          ),
        ));
    return remark;
  }
}
