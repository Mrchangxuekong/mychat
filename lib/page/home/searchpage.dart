import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/color/themeText.dart';

class Searchpage extends StatefulWidget {
  const Searchpage({Key? key}) : super(key: key);

  @override
  _SearchpageState createState() => _SearchpageState();
}

class _SearchpageState extends State<Searchpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("搜索"),
      ),
      body: getpge(),
    );
  }

  Widget getpge() {
    return Container(
      height: 45,
      margin: EdgeInsets.only(left: 20, right: 20),
      padding: EdgeInsets.only(left: 20, right: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          border: Border.all(color: ThemeColor.grayLine100)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
              child: TextField(
            onEditingComplete: () => {Get.snackbar("提示", "该功能待完善")},
            decoration: const InputDecoration(
                hintText: "请输入搜索的内容",
                border: InputBorder.none,
                isCollapsed: true),
          )),
          InkWell(
            child: const Icon(Icons.search_rounded, size: ThemeText.double26),
            onTap: () {
              Get.snackbar("提示", "该功能待完善");
            },
          ),
        ],
      ),
    );
  }
}
