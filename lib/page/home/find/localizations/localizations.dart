import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DemoLocalizations {
  DemoLocalizations(this.locale);

  final Locale locale;

  static DemoLocalizations? of(BuildContext context) {
    return Localizations.of<DemoLocalizations>(context, DemoLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'title': 'Flutter Demo Home Page',
      'hello': 'Hello World',
    },
    'zh': {
      'title': 'Flutter演示主页',
      'hello': '你好，世界',
    },
  };

  String? get title {
    return _localizedValues[locale.languageCode]?['title'];
  }

  String? get hello {
    return _localizedValues[locale.languageCode]?['hello'];
  }
}

class DemoLocalizationsDelegate
    extends LocalizationsDelegate<DemoLocalizations> {
  const DemoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'zh'].contains(locale.languageCode);

  @override
  Future<DemoLocalizations> load(Locale locale) {
    return SynchronousFuture<DemoLocalizations>(DemoLocalizations(locale));
  }

  @override
  bool shouldReload(DemoLocalizationsDelegate old) => false;

  static const DemoLocalizationsDelegate delegate = DemoLocalizationsDelegate();
}
