import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PickerLocalizations {
  PickerLocalizations(this.locale);

  final Locale locale;

  static PickerLocalizations? of(BuildContext context) {
    return Localizations.of<PickerLocalizations>(context, PickerLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {},
    'zh': {
      "picker_select": "选择",
      "picker_selected": "已选",
      "picker_preview": "预览",
      "picker_original": "原图",
      "picker_confirm": "确定",
      "picker_edit": "编辑",
      "picker_back": "返回",
      "picker_all": "所有",
      "picker_title_albums": "相册",
      "picker_title_camera": "相机",
      "picker_title_videos": "视频",
    },
  };

  String? get pickerSelect {
    return _localizedValues[locale.languageCode]?['picker_select'];
  }

  String? get pickerSelected {
    return _localizedValues[locale.languageCode]?['picker_selected'];
  }

  String? get pickerPreview {
    return _localizedValues[locale.languageCode]?['picker_preview'];
  }

  String? get pickerOriginal {
    return _localizedValues[locale.languageCode]?['picker_original'];
  }

  String? get pickerConfirm {
    return _localizedValues[locale.languageCode]?['picker_confirm'];
  }

  String? get pickerEdit {
    return _localizedValues[locale.languageCode]?['picker_edit'];
  }

  String? get pickerBack {
    return _localizedValues[locale.languageCode]?['picker_back'];
  }

  String? get pickerAll {
    return _localizedValues[locale.languageCode]?['picker_all'];
  }

  String? get pickerTitleAlbums {
    return _localizedValues[locale.languageCode]?['picker_title_albums'];
  }

  String? get pickerTitleCamera {
    return _localizedValues[locale.languageCode]?['picker_title_camera'];
  }

  String? get pickerTitleVideos {
    return _localizedValues[locale.languageCode]?['picker_title_videos'];
  }
}

class PickerLocalizationsDelegate
    extends LocalizationsDelegate<PickerLocalizations> {
  const PickerLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'zh'].contains(locale.languageCode);

  @override
  Future<PickerLocalizations> load(Locale locale) {
    return SynchronousFuture<PickerLocalizations>(PickerLocalizations(locale));
  }

  @override
  bool shouldReload(PickerLocalizationsDelegate old) => false;

  static const PickerLocalizationsDelegate delegate =
      PickerLocalizationsDelegate();
}
