import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zhongdingapp/api/timeline.dart';
import 'package:zhongdingapp/entity/timeline/comment.dart';
import 'package:zhongdingapp/entity/timeline/like.dart';
import 'package:zhongdingapp/entity/timeline/timeline.dart';
import 'package:zhongdingapp/entity/user.dart';
import 'package:zhongdingapp/page/home/find/post.dart';
import 'package:zhongdingapp/style/Text.dart';
import 'package:zhongdingapp/util/bottom_sheet.dart';
import 'package:zhongdingapp/util/config.dart';
import 'package:zhongdingapp/util/tools.dart';
import 'package:zhongdingapp/widget/appbar.dart';
import 'package:zhongdingapp/page/home/find/gallery.dart';
import 'package:zhongdingapp/widget/space.dart';
import 'package:zhongdingapp/widget/text.dart';

class TimeLinePage extends StatefulWidget {
  const TimeLinePage({super.key});

  @override
  State<TimeLinePage> createState() => _TimeLinePageState();
}

class _TimeLinePageState extends State<TimeLinePage>
    with SingleTickerProviderStateMixin {
  //用户资料
  UserModel? _user;

  //时间线
  // ignore: unused_field
  List<TimelineModel> _items = [];

  //层管理
  OverlayState? _overlayState;

  //遮罩层
  OverlayEntry? _shadeoverlayEntry;

  // 更多按钮位置 offset
  Offset _btnOffset = Offset.zero;

  // 动画控制器
  late AnimationController _animationController;

  // 动画 tween
  late Animation<double> _sizeTween;

  //滚动控制器
  final ScrollController _scrollController = ScrollController();

  //appbar颜色
  Color? _appBarColor;

  //当前操作的item
  TimelineModel? _currentItem;

  //是否显示评论输入框
  bool _isShowInput = false;

  //是否展开表情列表
  bool _isShowEmoji = false;

  //是否输入内容
  bool _isInputWords = false;

  //评论输入框
  final TextEditingController _textEditingController = TextEditingController();

  //输入框焦点
  final FocusNode _focusNode = FocusNode();

  //键盘高度
  final double _keyboardHeight = 200;

  //////////////////////////////////////////////////////////////////

  //载入数据
  Future _loadData() async {
    var result = await TimelineApi.pageList();
    if (mounted) {
      setState(() {
        _items = result;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    //监控输入
    _textEditingController.addListener(() {
      setState(() {
        //当输入的内容发生变化时，输入的内容不为空，表示为正在输入，改变状态
        _isInputWords = _textEditingController.text.isNotEmpty;
      });
    });

    //监听 scrollControlle 滚动高度
    _scrollController.addListener(() {
      //滚动超过200时，开始渐变
      if (_scrollController.position.pixels > 200) {
        //透明度系数
        double opacity = (_scrollController.position.pixels - 200) / 100;
        if (opacity < 0.85) {
          setState(() {
            _appBarColor = Colors.black.withOpacity(opacity);
          });
        }
      } else {
        setState(() {
          _appBarColor = null;
        });
      }
    });

    //初始化层管理对象
    _overlayState = Overlay.of(context);

    //初始化动画控制器
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
    );

    //设置动画取值范围
    _sizeTween = Tween(begin: 0.0, end: 200.0).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.easeInOut),
    );

    //设置用户资料
    _user = UserModel(
      nickname: "柒玖",
      avator:
          "https://ducafecat.oss-cn-beijing.aliyuncs.com/ducafecat/logo-500.png",
      conver:
          "https://ducafecat-pub.oss-cn-qingdao.aliyuncs.com/cover/activeprogrammer.jpg",
    );
    //刷新界面
    if (mounted) {
      setState(() {});
    }

    //载入数据
    _loadData();
  }

  //获取更多位置  offset   可通用
  void _getButtonOffset(GlobalKey key) {
    final RenderBox? renderBox =
        key.currentContext?.findRenderObject() as RenderBox?;
    final Offset offset = renderBox?.localToGlobal(Offset.zero) ?? Offset.zero;
    _btnOffset = offset;
  }

  @override
  void dispose() {
    _animationController.dispose();
    _scrollController.dispose();
    _textEditingController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

/////////////////////////////////////////////////////////////////////

  //是否喜欢菜单
  Widget _buildIslikeMenu(TimelineModel? item) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black87,
        borderRadius: BorderRadius.circular(4),
      ),
      child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.min,
          children: [
            //喜欢
            if (constraints.maxWidth > 80)
              Expanded(
                child: TextButton.icon(
                    onPressed: () {
                      _onLike();
                    },
                    icon: Icon(
                      Icons.favorite,
                      color: item?.isLike == false
                          ? Colors.redAccent
                          : Colors.white,
                      size: 16,
                    ),
                    label: Text(
                      item?.isLike == true ? "喜欢" : "取消",
                      style: textStylePopMeun,
                    )),
              ),
            //评论
            if (constraints.maxWidth > 150)
              Expanded(
                child: TextButton.icon(
                    onPressed: () {
                      //显示评论栏
                      _onSwitchCommentBar();
                      //关闭菜单
                      _onCloseMenu();
                    },
                    icon: const Icon(
                      Icons.chat_bubble_outline_rounded,
                      color: Colors.white,
                      size: 16,
                    ),
                    label: Text(
                      "评论",
                      style: textStylePopMeun,
                    )),
              ),
          ],
        );
      }),
    );
  }

  //头部
  Widget _builHeader() {
    //获取屏幕宽度
    final width = MediaQuery.of(context).size.width;
    return _user == null
        ? const Text("loading")
        : Stack(
            children: [
              //背景
              SizedBox(
                width: width,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Image.network(
                    _user?.conver ?? "",
                    height: width * 0.75,
                    fit: BoxFit.cover,
                  ),
                ),
              ),

              //昵称，头像
              Positioned(
                right: spacing,
                bottom: 0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    //昵称
                    Text(
                      _user?.nickname ?? "",
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          letterSpacing: 2,
                          height: 1.8,
                          //阴影
                          shadows: [
                            Shadow(
                                color: Colors.black,
                                offset: Offset(1, 1),
                                blurRadius: 2),
                          ]),
                    ),

                    //头像
                    Image.network(
                      _user?.avator ?? "",
                      width: 48,
                      height: 48,
                      fit: BoxFit.cover,
                    )
                  ],
                ),
              )
            ],
          );
  }

  //列表
  Widget _buildList() {
    return SliverList(
        delegate: SliverChildBuilderDelegate(
      (context, index) {
        var item = _items[index];
        return _bulidListItem(item);
      },
      childCount: _items.length,
    ));
  }

  //正文、图片、视频
  Widget _buikdContent(TimelineModel item) {
    int imgCount = item.images?.length ?? 0;

    GlobalKey buttonkey = GlobalKey();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //头像
          Image.network(
            item.user?.avator ?? "",
            width: 48,
            height: 48,
            fit: BoxFit.cover,
          ),
          const SpaceHorizontalWidget(),
          //右侧
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                //昵称
                Text(
                  item.user?.nickname ?? "",
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                  ),
                ),
                const SpaceHorizontalWidget(),

                TextMaxWidget(content: item.content ?? ""),
                const SpaceHorizontalWidget(),

                // 视频
                if (item.postType == "2")
                  LayoutBuilder(
                    builder:
                        (BuildContext context, BoxConstraints constraints) {
                      double ingWidth = constraints.maxWidth * 0.7;
                      return GestureDetector(
                        onTap: () {
                          _onGallery(item: item);
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            // 图
                            Image.network(
                              DuTools.imageUrl(
                                item.video?.cover ?? "",
                                width: 400,
                              ),
                              height: ingWidth,
                              width: ingWidth,
                              fit: BoxFit.cover,
                            ),
                            // 播放图标
                            const Positioned(
                                child: Icon(
                              Icons.play_circle_fill_outlined,
                              color: Colors.white,
                              size: 32,
                            ))
                          ],
                        ),
                      );
                    },
                  ),

                //九宫格图片列表
                if (item.postType == "1")
                  LayoutBuilder(builder:
                      (BuildContext context, BoxConstraints constraints) {
                    // ignore: unused_local_variable
                    double imgWidget = imgCount == 1
                        ? constraints.maxWidth * 0.7
                        : imgCount == 2
                            ? (constraints.maxWidth - spacing) / 2
                            : (constraints.maxWidth - spacing * 2) / 3;
                    return Wrap(
                      spacing: spacing,
                      runSpacing: spacing,
                      children: item.images!.map((e) {
                        return GestureDetector(
                          onTap: () {
                            _onGallery(src: e, item: item);
                          },
                          child: SizedBox(
                            width: imgWidget,
                            height: imgWidget,
                            child: Image.network(
                              DuTools.imageUrl(e),
                              width: imgWidget,
                              height: imgWidget,
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      }).toList(),
                    );
                  }),
                const SpaceHorizontalWidget(),

                //位置
                Text(
                  item.location ?? "",
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.black54,
                  ),
                ),
                const SpaceHorizontalWidget(),

                //时间,更多按钮
                Row(children: [
                  //时间
                  Text(
                    item.publishDate ?? "",
                    style: const TextStyle(
                      fontSize: 15,
                      color: Colors.black54,
                    ),
                  ),
                  const SpaceHorizontalWidget(),

                  const Spacer(),

                  //更多按钮
                  GestureDetector(
                    onTap: () {
                      //获取按钮位置
                      _getButtonOffset(buttonkey);

                      //设置当前操作的动态数据
                      setState(() {
                        _currentItem = item;
                      });

                      //显示遮罩层
                      _onShowMenu(onTap: _onCloseMenu);
                    },
                    child: Container(
                      key: buttonkey,
                      decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.circular(5),
                      ),
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: const Icon(
                        Icons.more_horiz_outlined,
                        color: Colors.black54,
                      ),
                    ),
                  )
                ]),
                const SpaceHorizontalWidget(space: 20),
              ],
            ),
          )
        ],
      ),
    );
  }

  //点赞列表
  Widget _buildLikeList(TimelineModel item) {
    return Container(
      padding: const EdgeInsets.all(spacing),
      color: Colors.grey[100],
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //图标
          const Padding(
            padding: EdgeInsets.only(top: spacing),
            child: Icon(
              Icons.favorite_border_outlined,
              size: 20,
              color: Colors.black54,
            ),
          ),
          const SpaceHorizontalWidget(),

          //列表
          Expanded(
            child: Wrap(
              spacing: 5,
              runSpacing: 5,
              children: [
                for (LikeModel item in item.likes ?? [])
                  Image.network(
                    item.avator ?? "",
                    height: 32,
                    width: 32,
                    fit: BoxFit.cover,
                  )
              ],
            ),
          )
        ],
      ),
    );
  }

  //评论列表
  Widget _buildCommentList(TimelineModel item) {
    return Container(
      padding: const EdgeInsets.all(spacing),
      color: Colors.grey[100],
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //图标
          const Padding(
            padding: EdgeInsets.only(top: spacing),
            child: Icon(
              Icons.chat_bubble_outline,
              size: 20,
              color: Colors.black54,
            ),
          ),
          const SpaceHorizontalWidget(),

          //右侧评论列表
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                for (CommentModel comment in item.comments ?? [])
                  //评论列表
                  Row(
                    children: [
                      //头像
                      Image.network(
                        comment.user?.avator ?? "",
                        height: 32,
                        width: 32,
                        fit: BoxFit.cover,
                      ),
                      const SpaceHorizontalWidget(),

                      //昵称，时间，内容
                      Expanded(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //行  1  昵称  时间
                            Row(
                              children: [
                                //昵称
                                Text(
                                  comment.user?.nickname ?? "",
                                  style: textStyleComment,
                                ),
                                const Spacer(),
                                // 时间
                                Text(
                                  DuTools.dateTimeFormat(
                                      comment.publishDate ?? ""),
                                  style: textStyleComment,
                                ),
                              ],
                            ),
                            // 行 2 内容
                            Text(
                              comment.content ?? "",
                              style: textStyleComment,
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }

  //列表项
  Widget _bulidListItem(TimelineModel item) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      //正文、图片、视频
      _buikdContent(item),

      //点赞列表
      _buildLikeList(item),

      //评论列表
      _buildCommentList(item)
    ]);
  }

  //底部弹出评论栏
  Widget _buildCommentBat() {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 100),
      padding: MediaQuery.of(context).viewInsets,
      child: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            color: Colors.grey[100],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              //评论输入框
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: _textEditingController,
                      focusNode: _focusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        counterText: "", //去掉显示最大输入
                        hintText: "评论",
                        //圆角边框
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide.none,
                        ),
                        contentPadding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        filled: true,
                        fillColor: Colors.white,
                        hintStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.black26,
                          letterSpacing: 2,
                        ),
                      ),
                      maxLength: 3,
                      minLines: 1,
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black87,
                      ),
                    ),
                  ),
                  const SpaceHorizontalWidget(),

                  //表情图标
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _isShowEmoji = !_isShowEmoji;
                      });
                      if (_isShowEmoji) {
                        _focusNode.unfocus();
                      } else {
                        _focusNode.requestFocus();
                      }
                    },
                    child: Icon(
                      _isShowEmoji
                          ? Icons.keyboard_alt_outlined
                          : Icons.mood_outlined,
                      size: 32,
                      color: Colors.black54,
                    ),
                  ),
                  const SpaceHorizontalWidget(),

                  //发送按钮
                  ElevatedButton(
                    onPressed: !_isInputWords ? null : _onComment,
                    child: const Text(
                      "发送",
                      style: TextStyle(
                        fontSize: 10,
                      ),
                    ),
                  ),
                ],
              ),
              //微信表情列表
              if (_isShowEmoji)
                Container(
                  padding: const EdgeInsets.all(spacing),
                  height: _keyboardHeight,
                  child: GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      // 横轴上的组件数量
                      crossAxisCount: 7,
                      // 沿主轴的组件之间的逻辑像素数。
                      mainAxisSpacing: 10,
                      // 沿横轴的组件之间的逻辑像素数。
                      crossAxisSpacing: 10,
                    ),
                    itemCount: 100,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        color: Colors.grey[200],
                      );
                    },
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }

  //主视图
  Widget _mainView() {
    return GestureDetector(
      onTap: () {
        if (_isShowInput) {
          _onSwitchCommentBar();
        }
      },
      child: CustomScrollView(
        //滚动控制器
        controller: _scrollController,
        slivers: [
          //头部
          SliverToBoxAdapter(
            child: _builHeader(),
          ),

          //padding
          const SliverPadding(padding: EdgeInsets.only(bottom: spacing)),

          //列表
          _buildList(),
        ],
      ),
    );
  }

////////////////////////////////////////////////////////////////

  //切换评论输入栏
  void _onSwitchCommentBar() {
    setState(() {
      _isShowInput = !_isShowInput;
      _isShowEmoji = false;
      //如果当前正在显示输入框，给焦点，否则的话不给焦点
      if (_isShowInput) {
        _focusNode.requestFocus();
      } else {
        _focusNode.unfocus();
      }
      //清空输入框
      _textEditingController.text = "";
    });
  }

  //评论操作
  void _onComment() {
    //安全检查
    if (_currentItem == null) return;

    //设置状态
    setState(() {
      _currentItem?.comments?.add(
        CommentModel(
          content: _textEditingController.text,
          user: _user,
          publishDate: DateTime.now().toString(),
        ),
      );
    });

    //关闭
    _onSwitchCommentBar();

    //执行请求，异步处理
    TimelineApi.comment(_currentItem!.id!, _textEditingController.text);
  }

  //发布事件
  _onPublish() async {
    final result = await DuBottomSheet().wxPicker<List<AssetEntity>>(context);
    if (result == null || result.isEmpty) {
      return;
    }
    //把数据压入发布界面
    if (mounted) {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
        return PostEditPage(
          selectedAssets: result,
          postType: (result.length == 1 && result.first.type == AssetType.video)
              ? PostType.video
              : PostType.image,
        );
      }));
    }
  }

  //显示菜单
  _onShowMenu({Function()? onTap}) {
    _shadeoverlayEntry = OverlayEntry(builder: (context) {
      return Positioned(
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        child: GestureDetector(
          onTap: onTap,
          child: Stack(
            children: [
              //遮罩
              AnimatedContainer(
                duration: const Duration(milliseconds: 100),
                color: Colors.black.withOpacity(0.4),
              ),
              //菜单
              AnimatedBuilder(
                  animation: _animationController,
                  builder: (context, chid) {
                    return Positioned(
                      left: _btnOffset.dx - 5 - _sizeTween.value,
                      top: _btnOffset.dy - 10,
                      child: SizedBox(
                        width: _sizeTween.value,
                        height: 40,
                        child:
                            _buildIslikeMenu(_currentItem ?? TimelineModel()),
                      ),
                    );
                  })
            ],
          ),
        ),
      );
    });
    _overlayState?.insert(_shadeoverlayEntry!);

    //延迟显示菜单
    Future.delayed(const Duration(milliseconds: 100), () {
      if (_animationController.status == AnimationStatus.dismissed) {
        _animationController.forward();
      }
    });
  }

  //关闭菜单
  _onCloseMenu() async {
    if (_animationController.status == AnimationStatus.completed) {
      await _animationController.reverse();
      _shadeoverlayEntry?.remove();
      _shadeoverlayEntry?.dispose();
    }
  }

  //点赞操作
  void _onLike() {
    //安全检查
    if (_currentItem == null) return;

    //设置检查
    setState(() {
      _currentItem?.isLike = !(_currentItem?.isLike ?? false);
    });

    //关闭菜单
    _onCloseMenu();

    //执行请求，异步处理
    TimelineApi.like(_currentItem!.id!);
  }

  //跳转相册
  void _onGallery({String? src, TimelineModel? item}) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return GalleryWidget(
        initiaIndex: src == null ? 1 : item?.images?.indexOf(src) ?? 1,
        timeline: item,
        imgUrls: item?.images ?? [],
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //如果为 true，并且指定了 [appBar],
      //则 [body] 的高度为 扩展到包括应用栏的高度和主体的顶部 与应用栏的顶部对齐。
      extendBodyBehindAppBar: true,

      //appbar导航
      appBar: AppBarWidget(
        backgroundColor: _appBarColor,
        leading: GestureDetector(
            onTap: () {
              Get.back();
            },
            child: const Icon(
              Icons.chevron_left,
              size: 30,
              color: Colors.white,
            )),
        actions: [
          //拍照
          GestureDetector(
            onTap: _onPublish,
            child: const Padding(
              padding: EdgeInsets.only(right: spacing),
              child: Icon(
                Icons.camera_alt,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      //内容
      body: _mainView(),
      bottomNavigationBar: _isShowInput ? _buildCommentBat() : null,
    );
  }
}
