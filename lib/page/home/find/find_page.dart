import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/page/home/message/information/widgets/click_on_avatar.dart';
import 'package:zhongdingapp/page/home/message/information/widgets/click_on_peopl_avatr.dart';

import 'package:zhongdingapp/routerHelper.dart';

class FindPage extends StatefulWidget {
  const FindPage({Key? key}) : super(key: key);

  @override
  _FindPageState createState() => _FindPageState();
}

class _FindPageState extends State<FindPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: MediaQuery.of(context).size.width,
          child: ListView(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                        color: Colors.grey.withOpacity(0.2), width: 1),
                    bottom: BorderSide(
                        color: Colors.grey.withOpacity(0.2), width: 1),
                  ),
                ),
                child: ListTile(
                  trailing: const Icon(Icons.navigate_next_rounded),
                  leading: const Icon(
                    MyIcons.friendCircle,
                    color: Colors.blue,
                  ),
                  title: const Text("朋友圈"),
                  onTap: () {
                    Get.toNamed(RouterHelper.circleoffriends);
                  },
                ),
              ),
              Container(color: Colors.grey.withOpacity(0.2), height: 10),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Colors.grey.withOpacity(0.2), width: 1))),
                child: ListTile(
                  trailing: const Icon(Icons.navigate_next_rounded),
                  leading: const Icon(
                    MyIcons.videoID,
                    color: Colors.blue,
                  ),
                  title: const Text("视频号"),
                  onTap: () {
                    Get.toNamed(RouterHelper.information);
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Colors.grey.withOpacity(0.2), width: 1))),
                child: ListTile(
                  trailing: const Icon(Icons.navigate_next_rounded),
                  leading: const Icon(
                    MyIcons.scanit,
                    color: Colors.blue,
                  ),
                  title: const Text("扫一扫"),
                  onTap: () {
                    Get.to(const Click_on_avatarWidget());
                  },
                ),
              ),
              Container(color: Colors.grey.withOpacity(0.2), height: 10),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Colors.grey.withOpacity(0.2), width: 1))),
                child: ListTile(
                  trailing: const Icon(Icons.navigate_next_rounded),
                  leading: const Icon(
                    MyIcons.video,
                    color: Colors.blue,
                  ),
                  title: const Text("看一看"),
                  onTap: () {
                    Get.to(const Click_on_peopl_avatrWidget());
                  },
                ),
              ),
              Container(color: Colors.grey.withOpacity(0.2), height: 10),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Colors.grey.withOpacity(0.2), width: 1))),
                child: ListTile(
                  trailing: const Icon(Icons.navigate_next_rounded),
                  leading: const Icon(
                    MyIcons.shopping,
                    color: Colors.blue,
                  ),
                  title: const Text("购物"),
                  onTap: () {
                    Get.toNamed("/");
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Colors.grey.withOpacity(0.2), width: 1))),
                child: ListTile(
                  trailing: const Icon(Icons.navigate_next_rounded),
                  leading: const Icon(
                    MyIcons.applet,
                    color: Colors.blue,
                  ),
                  title: const Text("小程序"),
                  onTap: () {
                    Get.toNamed("/");
                  },
                ),
              ),
              Container(color: Colors.grey.withOpacity(0.2), height: 10),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Colors.grey.withOpacity(0.2), width: 1))),
                child: ListTile(
                  trailing: const Icon(Icons.navigate_next_rounded),
                  leading: const Icon(
                    MyIcons.game,
                    color: Colors.blue,
                  ),
                  title: const Text("游戏"),
                  onTap: () {
                    Get.toNamed("/");
                  },
                ),
              ),
            ],
          )),
    );
  }
}
