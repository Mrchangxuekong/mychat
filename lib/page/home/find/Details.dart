import 'package:flutter/material.dart';

//详情视图
class Details extends StatefulWidget {
  const Details({
    super.key,
  });

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("详情"),
        backgroundColor: Colors.grey[300],
      ),
      body: Column(
        children: [],
      ),
    );
  }
}
