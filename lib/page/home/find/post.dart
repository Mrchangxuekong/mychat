import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zhongdingapp/entity/menu.dart';
import 'package:zhongdingapp/util/bottom_sheet.dart';
import 'package:zhongdingapp/util/compress.dart';
import 'package:zhongdingapp/util/config.dart';
import 'package:zhongdingapp/widget/appbar.dart';
import 'package:zhongdingapp/widget/divider.dart';
import 'package:zhongdingapp/page/home/find/gallery.dart';
import 'package:zhongdingapp/widget/player.dart';

enum PostType { image, video }

//发布界面
class PostEditPage extends StatefulWidget {
  const PostEditPage({
    super.key,
    this.postType,
    this.selectedAssets,
  });

  //发布类型
  final PostType? postType;

  //已选中图片列表
  final List<AssetEntity>? selectedAssets;

  @override
  State<PostEditPage> createState() => _PostEditPageState();
}

class _PostEditPageState extends State<PostEditPage> {
  //发布类型
  PostType? _postType;

  //已选中图片列表
  late List<AssetEntity> _selectedAssets;

  //是否开始拖拽
  bool _isDragNow = false;

  //是否将要删除
  bool _isWillRemove = false;

  //是否将要排序
  bool _isWillOrder = false;

  //被拖拽到的  target id
  String _tragetAssetId = "";

  //视频压缩文件
  // ignore: unused_field
  CompressMediaFile? _videoCompressFile;

  //内容输入控制器
  final TextEditingController _contenController = TextEditingController();

  // 最大内容长度
  // ignore: unused_field
  final int _maxContentLength = 20;

  //菜单列表
  // ignore: unused_field
  List<MenuItemModel> _menus = [];

  @override
  void initState() {
    super.initState();
    _postType = widget.postType;
    _selectedAssets = widget.selectedAssets ?? [];
    _menus = [
      MenuItemModel(icon: Icons.location_on_outlined, title: "所在位置"),
      MenuItemModel(icon: Icons.alternate_email_outlined, title: "提醒谁看"),
      MenuItemModel(icon: Icons.person_outline, title: "谁可以看", right: "公开"),
    ];
  }

  @override
  void dispose() {
    _contenController.dispose();
    super.dispose();
  }

  //内容输入框
  Widget _buildContentInput() {
    return LimitedBox(
      maxHeight: 180,
      child: TextField(
        maxLines: null,
        maxLength: 20, //输入框字数限制
        controller: _contenController,
        decoration: InputDecoration(
          hintText: "这一刻的想法...",
          hintStyle: const TextStyle(
            color: Colors.black12,
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
          border: InputBorder.none,
          counterText: _contenController.text.isEmpty ? "" : null,
        ),
        onChanged: (value) {
          setState(() {});
        },
      ),
    );
  }

  //菜单项目
  Widget _buildMenus() {
    List<Widget> ws = [];
    ws.add(const DividerWidget());
    //循环菜单
    for (var menu in _menus) {
      ws.add(
        ListTile(
          leading: Icon(menu.icon),
          title: Row(
            children: [
              Text(menu.title ?? ""),
              if (menu.right != null) const Spacer(),
              if (menu.right != null) Text(menu.right!),
            ],
          ),
          trailing: const Icon(Icons.navigate_next_rounded),
          horizontalTitleGap: -5,
          onTap: menu.onTap,
        ),
      );
      ws.add(const DividerWidget());
    }
    return Column(children: ws);

    /*  Padding(
      padding: const EdgeInsets.only(top: 100),
      child: Column(
        children: ws,
      ),
    ); */
  }

  //图片列表
  Widget _pictureList() {
    return Padding(
      padding: const EdgeInsets.all(spacing),
      child: LayoutBuilder(
          //计算宽高度，使用LayouBuilder 包裹Wrap
          builder: (BuildContext context, BoxConstraints constrains) {
        final double width =
            (constrains.maxWidth - spacing * 2 - spacing * 2 * 3) / 3;
        return Wrap(
          spacing: spacing, //横向图片之间的间隙
          runSpacing: spacing, //上下图片之间的间隙
          children: [
            //图片
            for (final asset in _selectedAssets) _pickture(asset, width),

            //选择按钮  判断图片数量
            if (_selectedAssets.length < maxAssets)
              _picktureButton(context, width)
          ],
        );
      }),
    );
  }

  //图片项
  Widget _pickture(AssetEntity asset, double width) {
    return Draggable<AssetEntity>(
      //此处可拖动对象将拖放的数据
      data: asset,
      //当可拖动对象开始被拖动时调用。
      onDragStarted: () {
        setState(() {
          _isDragNow = true;
        });
      },
      //当可拖动对象被放下时调用。
      onDragEnd: (details) {
        setState(() {
          _isDragNow = false;
          _isWillOrder = false;
        });
      },
      //当可拖动对象被放置并被 [DragTarget] 接受时调用。
      onDragCompleted: () {},
      //当可拖动对象未被 [DragTarget] 接受而被放置时调用。
      onDraggableCanceled: (Velocity velocity, Offset offset) {
        _isDragNow = false;
      },
      //拖拽进行时显示在指针下方的小部件
      feedback: Container(
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3),
        ),
        child: AssetEntityImage(
          asset,
          width: width,
          height: width,
          fit: BoxFit.cover,
          isOriginal: false,
        ),
      ),
      //拖拽后的显示，当正在进行一个活多个拖动时显示的小部件，而不是child
      childWhenDragging: Container(
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3),
        ),
        child: AssetEntityImage(
          asset,
          width: width,
          height: width,
          fit: BoxFit.cover,
          isOriginal: false,
          opacity: const AlwaysStoppedAnimation(0.2),
        ),
      ),
      //子组件
      child: DragTarget<AssetEntity>(
        builder: (context, candidateData, rejectedData) {
          return GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return GalleryWidget(
                  initiaIndex: _selectedAssets.indexOf(asset),
                  items: _selectedAssets,
                );
              }));
            },
            child: Container(
              clipBehavior: Clip.antiAlias,
              padding: (_isWillOrder && _tragetAssetId == asset.id)
                  ? EdgeInsets.zero
                  : const EdgeInsets.all(imagePadding),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3), //图片圆角
                border: (_isWillOrder && _tragetAssetId == asset.id)
                    ? Border.all(
                        color: accentColor,
                        width: imagePadding,
                      )
                    : null,
              ),
              child: AssetEntityImage(
                asset,
                width: 90,
                height: 100,
                fit: BoxFit.cover,
                isOriginal: false, //是否原图，注意：开启原图会导致选择完成之后闪动
              ),
            ),
          );
        },
        //调用以确定此小部件是否有兴趣接收给定的 被拖动到这个拖动目标上的数据片段
        // ignore: deprecated_member_use
        onWillAccept: (data) {
          if (data?.id == asset.id) {
            return false;
          }
          setState(() {
            _isWillOrder = true;
            _tragetAssetId = asset.id;
          });
          return true;
        },

        //当可接受的数据片段被拖放到该拖动目标上时调用。
        // ignore: deprecated_member_use
        onAccept: (data) {
          // 0 当前元素位置
          int targetIndex = _selectedAssets.indexWhere((element) {
            return element.id == asset.id;
          });

          // 1 删除原来的
          _selectedAssets.removeWhere((element) {
            return element.id == data.id;
          });

          // 2 插入到目标前面
          _selectedAssets.insert(targetIndex, data);

          setState(() {
            _isWillOrder = false;
            _tragetAssetId = "";
          });
        },

        onLeave: (data) {
          setState(() {
            _isWillOrder = false;
            _tragetAssetId = "";
          });
        },
      ),
    );
  }

  //添加按钮
  Widget _picktureButton(BuildContext context, double width) {
    return GestureDetector(
      onTap: () async {
        final result = await DuBottomSheet(selectedAssets: _selectedAssets)
            .wxPicker<List<AssetEntity>>(context);
        if (result == null || result.isEmpty) return;

        //视频
        if (result.length == 1 && result.first.type == AssetType.video) {
          setState(() {
            _postType = PostType.video;
            _selectedAssets = result;
          });
        }
        //图片
        else {
          setState(() {
            _postType = PostType.image;
            _selectedAssets = result;
          });
        }
      },
      child: Container(
        color: Colors.black12,
        width: width,
        height: width,
        child: const Icon(
          Icons.add,
          size: 48,
          color: Colors.black38,
        ),
      ),
    );
  }

  //删除bar
  Widget _buildRemoveBar() {
    return DragTarget(
      //调用以构建此小部件的内容。
      builder: (context, candidateData, rejectedData) {
        return SizedBox(
          width: double.infinity,
          child: Container(
            height: 120,
            color: _isWillRemove ? Colors.red[300] : Colors.red[200],
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //图标
                Icon(
                  Icons.delete,
                  size: 32,
                  color: _isWillRemove ? Colors.white : Colors.white70,
                ),
                //文字
                Text(
                  "拖拽到此处删除",
                  style: TextStyle(
                    color: _isWillRemove ? Colors.white : Colors.white70,
                  ),
                )
              ],
            ),
          ),
        );
      },
      //调用以确定此小部件是否有兴趣接收给定的 被拖动到该拖动目标上的数据片段。
      onWillAcceptWithDetails: (data) {
        setState(() {
          _isWillRemove = true;
        });
        return true;
      },
      //当可接受的数据片段被拖放到该拖动目标上时调用。
      onAcceptWithDetails: (data) {
        setState(() {
          _selectedAssets.remove(data.data as AssetEntity);
          _isWillRemove = false;
        });
      },
      //当给定的数据被拖过该目标离开时调用 目标。
      onLeave: (data) {
        _isWillRemove = false;
      },
    );
  }

  //主视图
  Widget _mainView() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(pagePadding),
        child: Column(
          children: [
            //内容输入
            _buildContentInput(),

            //相册列表
            if (_postType == PostType.image) _pictureList(),

            //视频播放器
            if (_postType == PostType.video)
              VideoPlayerWidget(
                initAsset: _selectedAssets.first,
                onCompleted: (value) =>
                    _videoCompressFile = value as CompressMediaFile?,
              ),

            //添加按钮
            if (_postType == null && _selectedAssets.isEmpty)
              Padding(
                padding: const EdgeInsets.all(spacing),
                child: _picktureButton(context, 100),
              ),
            //菜单
            Padding(
              padding: const EdgeInsets.only(top: 100),
              child: _buildMenus(),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*  appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true, //标题居中
        title: const Text("发布"),
      ), */
      appBar: AppBarWidget(
        //左侧返回
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(
            Icons.arrow_back_ios_new_outlined,
            color: Colors.black38,
          ),
        ),
        //右侧发布
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: pagePadding),
            child: ElevatedButton(
              onPressed: () {
                Get.back();
              },
              child: const Text("发表"),
            ),
          ),
        ],
      ),
      body: _mainView(),
      bottomSheet: _isDragNow ? _buildRemoveBar() : null, //底部弹出删除，拖拽弹出提示
    );
  }
}
