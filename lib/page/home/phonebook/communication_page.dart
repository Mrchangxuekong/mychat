import 'dart:collection';
import 'dart:ffi';

import 'package:azlistview/azlistview.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/controller/communication_controller.dart';
import 'package:zhongdingapp/controller/homeController.dart';
import 'package:zhongdingapp/entity/chat/friend_vo06.dart';
import 'package:zhongdingapp/routerHelper.dart';
import 'package:zhongdingapp/widget/positioned_red.dart';

class CommunicationPage extends StatelessWidget {
  const CommunicationPage({super.key});

  @override
  Widget build(BuildContext context) {
    print("ddd");
    final homeController = Get.find<HomeController>();
    return Column(children: [
      Container(
        decoration: const BoxDecoration(
          color: ThemeColor.black2,
        ),
        child: Container(
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(5),
          color: ThemeColor.white,
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Icon(Icons.search_sharp), Text("搜索")],
          ),
        ),
      ),
      Expanded(
        child: GetBuilder<CommunicationController>(
            init: CommunicationController(),
            builder: (c) => AzListView(
                data: c.dateList.value,
                indexBarData: c.kIndexBarData.value,
                itemCount: c.dateList.value.length,
                itemBuilder: (context, index) {
                  bool needLine = true;

                  ///需要划线
                  if (index != c.dateList.value.length - 1) {
                    FriendVo06 nextItem = c.dateList.value[index + 1];
                    if (nextItem.isShowSuspension) {
                      needLine = false;
                    }
                  }
                  if (index == c.dateList.value.length - 1) {
                    ///最后一条不需要划线
                    needLine = false;
                  }
                  if (index == 0) {
                    return Column(
                      children: [
                        getNewFriend(homeController),
                        ...getDefaultItem(c.defaultList.value),
                        getDataItem(c.dateList[index], needLine)
                      ],
                    );
                  } else {
                    return getDataItem(c.dateList[index], needLine);
                  }
                })),
      )
    ]);
  }

  ///生成默认的通讯通头部item
  getDefaultItem(List<FriendVo06> list) {
    List<Widget> res = [];
    for (int i = 0; i < list.length; i++) {
      var item = Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        child: Row(
          children: [
            Container(
              decoration: const BoxDecoration(color: ThemeColor.yellow),
              width: 30,
              height: 30,
              child: const Icon(
                MyIcons.addfriend,
                color: ThemeColor.white,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: i != list.length - 1
                                ? ThemeColor.black2
                                : Colors.transparent))),
                child: Text(list[i].nickName!),
              ),
            )
          ],
        ),
      );
      res.add(item);
    }
    return res;
  }

  getDataItem(FriendVo06 item, bool needLine) {
    return InkWell(
      onTap: () {
        //构造消息体
        //baseC.message.value=MessageDao(nickName: item.nickName,receiverId: int.parse(item.userId!),portrait: item.portrait);
        Get.toNamed(RouterHelper.chatSinglePage,
            arguments: {"friendVo6": item});
      },
      child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            children: [
              ///是否有
              if (item.isShowSuspension)
                Container(
                  color: ThemeColor.gray25,
                  padding: const EdgeInsets.only(left: 10),
                  height: 32,
                  alignment: Alignment.centerLeft,
                  child: Text(item.getSuspensionTag()),
                ),
              Row(
                children: [
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      decoration: const BoxDecoration(color: ThemeColor.yellow),
                      width: 30,
                      height: 40,
                      child: Image.network(item.portrait!, fit: BoxFit.cover)),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: needLine
                                      ? ThemeColor.black2
                                      : Colors.transparent))),
                      child: Text(item.nickName!),
                    ),
                  )
                ],
              ),
            ],
          )),
    );
  }

  ///添加新的朋友Ui
  getNewFriend(HomeController c) {
    return InkWell(
      onTap: () {
        Get.toNamed(RouterHelper.addFriendListPage);
      },
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
            child: Row(
              children: [
                Container(
                  decoration: const BoxDecoration(color: ThemeColor.yellow),
                  width: 30,
                  height: 30,
                  child: const Icon(
                    MyIcons.addGroup,
                    color: ThemeColor.white,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(bottom: 10),
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: ThemeColor.black2))),
                    child: const Text("新的朋友"),
                  ),
                )
              ],
            ),
          ),
          PositionedRed(
            num: c.newFriendNum.value,
            left: 30,
            top: 3,
          )
        ],
      ),
    );
  }
}
