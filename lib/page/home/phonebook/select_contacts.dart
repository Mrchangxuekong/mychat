import 'package:azlistview/azlistview.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/controller/select_contacts_controller.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';


///选择联系人
class SelectContacts extends StatefulWidget {
  const SelectContacts({super.key});


  @override
  State<SelectContacts> createState() => _SelectContactsState();
}

class _SelectContactsState extends State<SelectContacts> {
  var getC = Get.find<SelectContactsController>();
  List<ItemTitle> defaultList = [];
  List<String> kIndexBarData=[];


  @override
  void initState() {
    defaultList = [];
    defaultList.add(ItemTitle(title: "新的朋友", icon: MyIcons.addGroup, dismissHandle: () {  }));
    defaultList.add(ItemTitle(title: "仅聊天的朋友", icon: MyIcons.addGroup1, dismissHandle: () {  }));
    getC.getDateList();
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("选择联系人"),
        centerTitle: true,
        backgroundColor: ThemeColor.appbarBackground,
      ),
      body:Column(
        children: [
          Container(
            padding: const EdgeInsets.only(left: 16),
            decoration: const BoxDecoration(
                border: Border(bottom: BorderSide(color: ThemeColor.hintText))),
            child: Row(
              children: [
                Container(
                  width: 35,
                  height: 35,
                  child: const Icon(Icons.search_sharp),
                ),
                const Expanded(
                  child: TextField(
                    style: (TextStyle(fontSize: 16)),
                    decoration: InputDecoration(
                        hintText: "搜索",
                        border:
                            OutlineInputBorder(borderSide: BorderSide.none)),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: const Text("选择一个和多个联系人聊天"),
          ),
          Expanded(
            child:   AzListView(
                data: getC.dateList.value,
                indexBarData: kIndexBarData,
                itemCount: getC.dateList.value.length,
                itemBuilder: (context, index) {
                  bool needLine = true;

                  ///需要划线
                  if (index != getC.dateList.value.length - 1) {
                    ItemTitle nextItem = getC.dateList.value[index + 1];
                    if (nextItem.isShowSuspension) {
                      needLine = false;
                    }
                  }
                  if (index == getC.dateList.value.length - 1) {
                    ///最后一条不需要划线
                    needLine = false;
                  }
                  if (index == 0) {
                    return Column(
                      children: [
                        getDataItem(getC.dateList.value[index], needLine,index)
                      ],
                    );
                  } else {
                    return getDataItem(getC.dateList.value[index], needLine,index);
                  }
                })

          ),
          Container(
            //防止底部太低
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
            decoration: const BoxDecoration(color: ThemeColor.gray25,border: Border(top: BorderSide(color: ThemeColor.gray))),
            child: Obx(()=> ButtonBar(
              children: [
                Container(
                  width: 80,
                  height: 50,
                  alignment: Alignment.center,
                  color: getC.selectedNum.value==0?ThemeColor.gray50:ThemeColor.green200,
                  child:Text("完成(${getC.selectedNum.value})",style: const TextStyle(color:ThemeColor.white),) ,
                ),
              ],
            )),
          )
        ],
      ),
    );
  }
  bool? cc=false;
  getDataItem(ItemTitle item, bool needLine,int index) {


    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: Obx(() => Column(
          children: [
            ///是否有
            if (item.isShowSuspension)
              Container(
                color: ThemeColor.gray25,
                padding: const EdgeInsets.only(left: 10),
                height: 32,
                alignment: Alignment.centerLeft,
                child: Text(item.getSuspensionTag()),
              ),
            InkWell(onTap:(){

            },
            child:   Row(
              children: [ Checkbox(
                  value: item.boolSelect?.value,
                  activeColor: Colors.red,
                  checkColor: Colors.yellow,
                  onChanged: (value) {
                    checkChanged(item,value);
                  }
              ),
                Container(
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    decoration: const BoxDecoration(color: ThemeColor.yellow),
                    width: 30,
                    height: 40,
                    child: Image.network(
                        "https://alifei01.cfp.cn/creative/vcg/800/new/VCG211177178924.jpg",
                        fit: BoxFit.cover)),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: needLine
                                    ? ThemeColor.black2
                                    : Colors.transparent))),
                    child: Text(item.title),
                  ),
                )
              ],
            ),)

          ],
        )));
  }

  void checkChanged(ItemTitle item,bool? value) {
    item.boolSelect?.value=value!;

    var list=getC.dateList.value;
    if(list.isEmpty){
      return;
    }
    int num=0;//选中的数量
    //统计点击数量
    for(int i=0;i<list.length;i++){
      if(list[i].boolSelect!.value){
        num++;
      }
    }
    getC.selectedNum.value=num;

  }

}
