import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/controller/search_user_phone_controller.dart';

class SearchUserPhone extends StatefulWidget {
  const SearchUserPhone({super.key});

  @override
  State<SearchUserPhone> createState() => _SearchUserPhoneState();
}

class _SearchUserPhoneState extends State<SearchUserPhone> {
  TextEditingController controller = TextEditingController();
  var getC = Get.find<SearchUserPhoneController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: ThemeColor.appbarBackground,
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 30),
                  height: 50,
                  child: Row(
                    children: [
                      Expanded(
                          child: Container(
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 11, vertical: 11),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              height: 40,
                              width: double.maxFinite,
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  const Icon(Icons.search_sharp),
                                  Expanded(
                                      child: TextField(
                                    controller: controller,
                                    onChanged: (value) {
                                      getC.inputKeyWord.value = value;
                                    },
                                    decoration: const InputDecoration(
                                      hintText: "账号/手机号",
                                      hintStyle: TextStyle(fontSize: 14),
                                      isCollapsed: true,
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide.none),
                                    ),
                                  ))
                                ],
                              ))),
                      InkWell(
                        child: const Text("取消"),
                        onTap: () {
                          Get.back();
                        },
                      ),
                      SizedBox(
                        width: 15,
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                    },
                    child: ListView(
                      padding: const EdgeInsets.only(top: 0),
                      children: [
                        InkWell(
                            onTap: () {},
                            child: Obx(
                              () => getWidget(),
                            ))
                      ],
                    ),
                  ),
                )
              ],
            )));
  }

  getWidget() {
    return getC.inputKeyWord.value.isNotEmpty
        ? Container(
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            decoration: const BoxDecoration(color: Colors.white),
            child: InkWell(
              onTap: () {
                getC.searchUserByPhone();
              },
              child: Row(
                children: [
                  const Icon(
                    MyIcons.addfriend,
                    color: Colors.greenAccent,
                    size: 50,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                      child: Text(
                    getC.inputKeyWord.value,
                    style: const TextStyle(fontSize: 20),
                  ))
                ],
              ),
            ),
          )
        : Container();
  }
}
