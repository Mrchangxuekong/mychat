import 'package:azlistview/azlistview.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/chat/friend_vo06.dart';

import 'select_contacts_logic.dart';

class SelectContactsPage extends StatelessWidget {
  const SelectContactsPage({super.key});

  @override
  Widget build(BuildContext context) {
    final logic = Get.find<SelectContactsLogic>();
    return GetBuilder<SelectContactsLogic>(
      init: SelectContactsLogic(),
        builder: (context)=> Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Get.back();
            logic.onInit();

          },
        ),
        title: const Text("选择联系人"),
        centerTitle: true,
        backgroundColor: ThemeColor.appbarBackground,
      ),
      body:Column(
        children: [
          Container(
            padding: const EdgeInsets.only(left: 16),
            decoration: const BoxDecoration(
                border: Border(bottom: BorderSide(color: ThemeColor.hintText))),
            child: Row(
              children: [
                Container(
                  width: 35,
                  height: 35,
                  child: const Icon(Icons.search_sharp),
                ),
                const Expanded(
                  child: TextField(
                    style: (TextStyle(fontSize: 16)),
                    decoration: InputDecoration(
                        hintText: "搜索",
                        border:
                        OutlineInputBorder(borderSide: BorderSide.none)),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: const Text("选择一个和多个联系人聊天"),
          ),
           Expanded(
              child:  AzListView(
                  data: logic.dateList.value,
                  indexBarData: logic.kIndexBarData.value,
                  itemCount: logic.dateList.value.length,
                  itemBuilder: (context, index) {
                    bool needLine = true;
                    ///需要划线
                    if (index != logic.dateList.value.length - 1) {
                      FriendVo06 nextItem = logic.dateList.value[index + 1];
                      if (nextItem.isShowSuspension) {
                        needLine = false;
                      }
                    }
                    if (index == logic.dateList.value.length - 1) {
                      ///最后一条不需要划线
                      needLine = false;
                    }
                    if (index == 0) {
                      return Column(
                        children: [
                          getDataItem(logic.dateList[index], needLine,index,logic)
                        ],
                      );
                    } else {
                      return getDataItem(logic.dateList[index], needLine,index,logic);
                    }
                  })

          ),
          Container(
            //防止底部太低
            padding: EdgeInsets.only(bottom: MediaQuery.of(Get.context!).padding.bottom),
            decoration: const BoxDecoration(color: ThemeColor.gray25,border: Border(top: BorderSide(color: ThemeColor.gray))),
            child: ButtonBar(
              children: [
                InkWell(
                  onTap: (){
                    logic.addGroupChat();
                  },
                  child:Container(
                    width: 80,
                    height: 30,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(right: 10),
                    color: logic.selectedNum.value==0?ThemeColor.gray50:ThemeColor.green200,
                    child:Text("完成(${logic.selectedNum.value})",style: const TextStyle(color:ThemeColor.white),) ,
                  ),)

              ],
            )),
        ],
      ),
    ));
  }
  getDataItem(FriendVo06 item, bool needLine,int index,SelectContactsLogic logic) {

    return
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            ///是否有
            if (item.isShowSuspension)
              Container(
                color: ThemeColor.gray25,
                padding: const EdgeInsets.only(left: 10),
                height: 32,
                alignment: Alignment.centerLeft,
                child: Text(item.getSuspensionTag()),
              ),
            InkWell(onTap:(){
              int num=item.selectItem!?-1:1;
              logic.selectedNum.value= logic.selectedNum.value+num;
              item.selectItem=!item.selectItem!;
              logic.update();
            },
              child:   Row(
                children: [ Checkbox(
                    value: item.selectItem,
                    activeColor: Colors.green,
                    checkColor: Colors.white,
                    onChanged: (value) {

                    }
                ),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      decoration: const BoxDecoration(color: ThemeColor.yellow),
                      width: 30,
                      height: 40,
                      child: Image.network(
                          item.portrait!,
                          fit: BoxFit.cover)),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: needLine
                                      ? ThemeColor.black2
                                      : Colors.transparent))),
                      child: Text(item.nickName!),
                    ),
                  )
                ],
              ),)
          ],
        ));
  }


}
