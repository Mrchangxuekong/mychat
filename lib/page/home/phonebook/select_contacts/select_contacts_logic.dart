import 'dart:convert';

import 'package:azlistview/azlistview.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/config/api_url.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/chat/chat_group.dart';
import 'package:zhongdingapp/entity/chat/friend_vo06.dart';
import 'package:zhongdingapp/util/cache_util.dart';
import 'package:zhongdingapp/util/http_dio.dart';
import 'package:zhongdingapp/util/mqtt_utils.dart';

class SelectContactsLogic extends GetxController {
  var defaultList =<FriendVo06> [].obs;
  var dateList = <FriendVo06>[].obs;
  var kIndexBarData= <String>[].obs;
  var selectedNum= 0.obs;
  var boolSelect=false.obs;

  @override
  void onInit() {
    selectedNum.value=0;
    defaultList.add(FriendVo06(nickName: "仅聊天的朋友", portrait:MyIcons.networkImg1 , dismissHandle: () {  }));
    Future.wait([fetchData()]).then((List values) {

      dateList.value=values[0];

      // 处理获取的数据
      SuspensionUtil.sortListBySuspensionTag(dateList.value);
      SuspensionUtil.setShowSuspensionStatus(dateList.value);
      for (int i = 0; i < dateList.value.length; i++) {
        String tag=dateList.value[i].getSuspensionTag();
        if(!kIndexBarData.value.contains(tag)){
          kIndexBarData.value.add(tag);
        }
      }
      ///将#号移动到最后
      if(kIndexBarData.contains("#")){
        kIndexBarData.value.remove("#");
        kIndexBarData.value.add("#");
      }
      SuspensionUtil.sortListBySuspensionTag(dateList.value);
      SuspensionUtil.setShowSuspensionStatus(dateList.value);
      print("onitlllllllllllllllllllllllllllllllllll");
      update();
    });

  }


  ///异步获取好友列表
  Future<List<FriendVo06>> fetchData() async {
    var res =await HttpDio.getInstance().post(ApiUrl.getFriendFriendList, {});
    if (res[R.code] == 200) {
      return Future.value(FriendVo06.fromJsonList(res[R.data]));
    }else{
      Get.snackbar("提示",res[R.msg] );
      return  Future.value([]);
    }
  }
  ///点击完成后生成群聊
  void addGroupChat() async {
    List<String> userIds=[];
    ///获取选中参数
    List<FriendVo06> list = dateList.value;
    for(int i=0;i<list.length;i++){
      if(list[i].selectItem!){
        userIds.add(list[i].userId!);
      }
    }
    if(userIds.isEmpty){
      Get.snackbar("提示", "你当前没有选择联系人");
      return;
    }
    String? userId=await CacheUtil.getKey(R.userId);
    userIds.remove(userId);
    if(userIds.isEmpty){
      Get.snackbar("提示", "你不能只选择自己");
      return;
    }
    var res  =await HttpDio.getInstance().post(ApiUrl.groupCreateGroup, userIds);
    if(res [R.code]==200){
     ChatGroup chatGroup=ChatGroup.fromJson(res[R.data]);
    await Mqttutils.sendMessage(jsonEncode(userIds),'${R.chatCreateGroup}${chatGroup.id}');
     ///自己订阅该群
     Mqttutils.addAllSubscribed(param: "${R.chatGroup}${chatGroup.id}");

    }



  }


}
