import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/base/list_title_item.dart';
import 'package:zhongdingapp/routerHelper.dart';

class AddFriend extends StatefulWidget {
  const AddFriend({super.key});

  @override
  State<AddFriend> createState() => _AddFriendState();
}

class _AddFriendState extends State<AddFriend> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ThemeColor.appbarBackground,
        appBar: AppBar(
          backgroundColor: ThemeColor.appbarBackground,
          title: const Text("添加朋友"),
          centerTitle: true,
        ),
        body: ListView(
          children: [
            ///手机号
            Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                height: 30,
                width: double.maxFinite,
                alignment: Alignment.center,
                child: InkWell(
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Icon(Icons.search_sharp), Text("账号/手机号")],
                  ),
                  onTap: () => Get.toNamed(RouterHelper.searchUserPhone),
                )),

            ///我的微信号
            Container(
                margin: const EdgeInsets.only(top: 10, bottom: 35),
                alignment: Alignment.center,
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [Text("我的微信号  cxk1000"), Icon(MyIcons.codepick)],
                )),
            getListTile(ListTitleItem(
                icon: MyIcons.codeHead,
                title: "雷达朋友",
                titleTip: "添加身边的朋友",
                dismissHandle: () {
                  print("点击了雷达");
                })),
            getListTile(ListTitleItem(
                icon: MyIcons.codepick,
                title: "面对面建群",
                titleTip: "与身边的朋友进入同一个群聊",
                dismissHandle: () {
                  print("点击了面对面建群");
                })),
            getListTile(ListTitleItem(
                icon: MyIcons.codeHead,
                title: "扫一扫",
                titleTip: "扫描二维码名片",
                dismissHandle: () {
                  print("点击了扫一扫");
                })),
            getListTile(ListTitleItem(
                icon: MyIcons.codeHead,
                title: "手机联系人",
                titleTip: "添加或邀请通讯录中的朋友",
                dismissHandle: () {
                  print("点击了手机联系人");
                })),
            getListTile(ListTitleItem(
                icon: MyIcons.codeHead,
                title: "公众号",
                titleTip: "获取更多资讯和服务",
                dismissHandle: () {
                  print("点击了公众号");
                })),
            getListTile(ListTitleItem(
                icon: MyIcons.codeHead,
                title: "企业微信联系人",
                titleTip: "通过手机号搜索企业微信用户",
                dismissHandle: () {
                  print("点击了企业微信联系人");
                })),
          ],
        ));
  }

  getListTile(ListTitleItem item) {
    return InkWell(
      child: Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(bottom: BorderSide(color: ThemeColor.grayLine100))),
        child: ListTile(
          title: Text(
            item.title,
            style: const TextStyle(fontSize: 15),
          ),
          leading: Icon(item.icon),
          subtitle: Text(item.titleTip!, style: const TextStyle(fontSize: 10)),
          trailing: const Icon(Icons.chevron_right),
        ),
      ),
      onTap: () {
        item.dismissHandle!();
      },
    );
  }
}
