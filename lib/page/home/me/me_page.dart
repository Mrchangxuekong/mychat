import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/routerHelper.dart';

class MePage extends StatefulWidget {
  const MePage({super.key});

  @override
  State<MePage> createState() => _MePageState();
}

class _MePageState extends State<MePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          color: ThemeColor.appbarBackground,
        ),
        child: ListView(children: [
          InkWell(
            child: getHead(),
            onTap: () {
              Get.toNamed(RouterHelper.meInfoPage);
            },
          ),
          getNeedMargin(
            "服务",
            () {
              Get.toNamed(RouterHelper.personalchat);
            },
            const Icon(
              MyIcons.giveservice,
              color: Colors.blue,
            ),
          ),
          getNeedLine(
            "收藏",
            const Icon(
              MyIcons.collect,
              color: Colors.blue,
            ),
          ),
          getNeedLine(
            "朋友圈",
            const Icon(
              MyIcons.albumclick,
              color: Colors.blue,
            ),
          ),
          getNeedLine(
            "表情",
            const Icon(
              MyIcons.expression,
              color: Colors.blue,
            ),
          ),
          getNeedMargin(
            "设置",
            () {
              Get.toNamed(RouterHelper.setup);
            },
            const Icon(
              MyIcons.settion,
              color: Colors.blue,
            ),
          ),
          getNeedMargin(
            "退出登录",
            () {
              quitLogin();
            },
            const Icon(
              MyIcons.settion,
              color: Colors.blue,
            ),
          ),
        ]),
      ),
    );
  }

  //生成头部方法
  getHead() {
    return Container(
      color: ThemeColor.white,
      padding: const EdgeInsets.only(bottom: 50),
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.only(left: 10),
            alignment: Alignment.centerLeft,
            width: 80,
            height: 80,
            child: CachedNetworkImage(
              imageUrl:
                  "http://192.168.1.13/static/2024/05/21/d46f021c-ee14-4b50-9f29-d0c025a7fd74.jpg",
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          Expanded(
              child: Container(
            margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            height: 80,
            child: const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "以燕立志",
                  style: TextStyle(fontSize: 18),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        "微信号:ic_739jrrrddddsewwwwwwwwwwww",
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Icon(MyIcons.codepick),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(Icons.chevron_right),
                  ],
                )
              ],
            ),
          ))
        ],
      ),
    );
  }

  ///需要margin
  getNeedMargin(String name, VoidCallback fn, Icon icon) {
    return InkWell(
      onTap: fn,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 15),
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 10),
        color: Colors.white,
        child: Row(
          children: [
            icon,
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Text(
                name,
                style: const TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            const Icon(Icons.chevron_right)
          ],
        ),
      ),
    );
  }

  ///需要线的
  getNeedLine(String name, Icon icon) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 10),
      decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(color: ThemeColor.line))),
      child: Row(
        children: [
          icon,
          const SizedBox(
            width: 10,
          ),
          Expanded(
              child: Text(
            name,
            style: const TextStyle(
              fontSize: 17,
            ),
          )),
          const Icon(Icons.chevron_right)
        ],
      ),
    );
  }
}

quitLogin() async {
  var openBox = await Hive.openBox(constHiveBox);
  openBox.delete(R.token);
  openBox.delete(R.userId);
  openBox.delete(R.userId);
  Get.toNamed(RouterHelper.login);
}
