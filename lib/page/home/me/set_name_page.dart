import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/themeColor.dart';

class SetNamePage extends StatefulWidget {
  const SetNamePage({super.key});

  @override
  State<SetNamePage> createState() => _SetNamePageState();
}

class _SetNamePageState extends State<SetNamePage> {
TextEditingController editC=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeColor.appbarBackground,
      appBar: AppBar(
        backgroundColor: ThemeColor.appbarBackground,
        title: const Text("设置名字"),
        centerTitle: true,
        actions: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            padding: const EdgeInsets.symmetric(vertical: 7,horizontal: 12),
            color: ThemeColor.green200,
            child: const Text("完成"),
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 8),
        color: Colors.white,
        child:  Row(
          children: [
            Expanded(
                child: TextField(
                  decoration: const InputDecoration(
                      hintText: "请输入你的名字", border: InputBorder.none),
                controller:editC ,),),
            InkWell(child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 5),
              child: const Icon(Icons.cancel),),onTap: ()=>editC.clear(),)

          ],
        ),
      ),
    );
  }
}
