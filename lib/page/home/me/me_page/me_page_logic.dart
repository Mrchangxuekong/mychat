import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/base/login_user.dart';
import 'package:zhongdingapp/routerHelper.dart';
import 'package:zhongdingapp/util/cache_util.dart';
import 'package:zhongdingapp/util/mqtt_utils.dart';



class MePageLogic extends GetxController {
  var user= LoginUser().obs;

  @override
  void onReady() {
    super.onReady();
    print("onReady===");
    initData();
  }

  @override
  void dispose() {
    print("销毁了");
  }

  ///加载数据
  void initData() async {
    user.value=await CacheUtil.getUser();
    update();

  }

  void quitLogin() async {
    var openBox = await Hive.openBox(constHiveBox);
    openBox.delete(R.token);
    openBox.delete(R.userId);
    openBox.delete(R.userId);
    Get.toNamed(RouterHelper.login);
    //断开mqtt连接
    Mqttutils.close();
  }
}
