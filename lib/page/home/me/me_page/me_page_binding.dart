import 'package:get/get.dart';

import 'me_page_logic.dart';

class MePageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MePageLogic());
  }
}
