import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';
import 'package:zhongdingapp/util/image_util.dart';


class QrMyCodePage extends StatefulWidget {
  const QrMyCodePage({super.key});

  @override
  State<QrMyCodePage> createState() => _QrMyCodePageState();
}

class _QrMyCodePageState extends State<QrMyCodePage> {
  GlobalKey key=GlobalKey();
  var c="".obs;

  @override
  Widget build(BuildContext context)  {
    final List<ItemTitle> list=[
      ItemTitle(title: "扫一扫",dismissHandle: (){print("点击了扫一扫");}),
      ItemTitle(title: "换个样式",dismissHandle: (){print("点击了换个样式");}),
      ItemTitle(title: "保存图片",dismissHandle: (){getcddd();}),
    ];
    return Scaffold(
        appBar: AppBar(
          actions: const [
            InkWell(
              child: Icon(Icons.add),
            )
          ],
        ),
        backgroundColor: ThemeColor.appbarBackground,
        body:RepaintBoundary(
          key: key,
          child:Column(children: [
          Container(padding:const EdgeInsets.only(left: 78),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 100),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(right: 10),
                          child: Image.network(
                            "https://q5.itc.cn/q_70/images03/20240312/dafb237d64634fbabff5c406eea3ff0b.jpeg",
                            width: 48,
                          ),
                        ),
                        const Column(
                          children: [
                            Text("往事如风1"),
                            Text("广西南宁"),
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 32),
                      width: 200,
                      child: Image.network(
                        "https://q5.itc.cn/q_70/images03/20240312/dafb237d64634fbabff5c406eea3ff0b.jpeg",
                        fit: BoxFit.cover,
                      )),
                  const Text("扫一扫，添加我为好友"),
                ]),),
          const Expanded(
              child: Column(
                children: [SizedBox()],
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ...list.map((e) =>  InkWell(onTap: e.dismissHandle,child: Container(
                padding: const EdgeInsets.only(right: 15,left: 15),
                decoration:  BoxDecoration(
                    border: Border(
                        right:e.title=="保存图片"?BorderSide.none:const BorderSide(color: ThemeColor.grayLine100))),
                child:  Text(e.title),
              ),)),
              SizedBox(height: MediaQuery.of(context).padding.bottom + 60)
            ],
          )
        ],
        ) ,)
       );
  }

  void getcddd()async {
    c= await ImageUtil.saveScreenshotImage(key);
  }
}
