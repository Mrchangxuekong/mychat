import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/base/list_title_item.dart';
import 'package:zhongdingapp/routerHelper.dart';

class MeInfoPage extends StatefulWidget {
  const MeInfoPage({super.key});

  @override
  State<MeInfoPage> createState() => _MeInfoPageState();
}

class _MeInfoPageState extends State<MeInfoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("个人信息"),
        centerTitle: true,
      ),
      backgroundColor: ThemeColor.appbarBackground,
      body: ListView(
        children: [
          getListTileImg(ListTitleItem(
              title: "头像",
              url:
                  "https://q5.itc.cn/q_70/images03/20240312/dafb237d64634fbabff5c406eea3ff0b.jpeg",
              dismissHandle: () {
                Get.toNamed(RouterHelper.bigAvatarPage);
              })),
          getListTile(ListTitleItem(
              title: "名字",
              titleTip: "往事随风",
              dismissHandle: () {
                Get.toNamed(RouterHelper.setNamePage);
              })),
          getListTile(ListTitleItem(
              title: "拍一拍",
              icon: MyIcons.message,
              dismissHandle: () {
                print("点击了拍一拍");
              })),
          getListTile(ListTitleItem(
              title: "微信号",
              titleTip: "大幅度飞得更高",
              dismissHandle: () {
                print("点击了微信号");
              })),
          getListTile(ListTitleItem(
              title: "我的二维码",
              icon: MyIcons.codepick,
              dismissHandle: () {
                Get.toNamed(RouterHelper.qrMyCode);
              })),
          getListTile(ListTitleItem(
              title: "更多",
              dismissHandle: () {
                print("点击了更多");
              },
              line: false)),
          const SizedBox(
            height: 10,
          ),
          getListTile(ListTitleItem(
              title: "来电铃声",
              dismissHandle: () {
                print("点击了微信号");
              },
              line: false)),
          const SizedBox(
            height: 10,
          ),
          getListTile(ListTitleItem(
              title: "微信豆",
              dismissHandle: () {
                print("点击了微信号");
              },
              line: false)),
          const SizedBox(
            height: 10,
          ),
          getListTile(ListTitleItem(
              title: "我的地址",
              dismissHandle: () {
                print("点击了微信号");
              },
              line: false)),
        ],
      ),
    );
  }

  getListTile(ListTitleItem item) {
    bool titleTip = item.titleTip != null;
    bool line = item.line == null ? true : item.line!;
    return InkWell(
      child: Container(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          decoration: BoxDecoration(
              color: Colors.white,
              border: line
                  ? const Border(bottom: BorderSide(color: ThemeColor.line))
                  : const Border(bottom: BorderSide.none)),
          child: Row(
            children: [
              const SizedBox(
                width: 10,
              ),
              Expanded(child: Text(item.title)),
              titleTip ? Text(item.titleTip!) : const Text(""),
              const SizedBox(
                width: 3,
              ),
              const Icon(Icons.chevron_right)
            ],
          )),
      onTap: () {
        item.dismissHandle!();
      },
    );
  }

  getListTileImg(ListTitleItem item) {
    return InkWell(
      child: Container(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          decoration: const BoxDecoration(
              color: Colors.white,
              border: Border(bottom: BorderSide(color: ThemeColor.black2))),
          child: Row(
            children: [
              const SizedBox(
                width: 10,
              ),
              Expanded(child: Text(item.title)),
              Image.network(
                item.url!,
                fit: BoxFit.cover,
                width: 50,
                height: 50,
              ),
              const Icon(Icons.chevron_right)
            ],
          )),
      onTap: () {
        item.dismissHandle!();
      },
    );
  }
}
