import 'package:flutter/material.dart';

class AvatarPage extends StatefulWidget {
  const AvatarPage({super.key});

  @override
  State<AvatarPage> createState() => _AvatarPageState();
}

class _AvatarPageState extends State<AvatarPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("个人信息"),
          centerTitle: true, //标题文字居中
        ),
        body: ListView(
          children: [
            Divider(color: Colors.grey.withOpacity(0.7)),
            ListTile(
              trailing: const Icon(Icons.abc_sharp),
              title: const Text("头像"),
              onTap: () {},
            ),
            Container(height: 20, color: Colors.grey.withOpacity(0.5)),
            ListTile(
              trailing: const Icon(Icons.abc_sharp),
              title: const Text("名字"),
              onTap: () {},
            ),
            Divider(height: 8, color: Colors.grey.withOpacity(0.7)),
            ListTile(
              trailing: const Icon(Icons.abc_sharp),
              title: const Text("微信号"),
              onTap: () {},
            ),
            Container(height: 20, color: Colors.grey.withOpacity(0.5)),
            ListTile(
              trailing: const Icon(Icons.abc_sharp),
              title: const Text("我的二维码"),
              onTap: () {},
            ),
            Divider(height: 8, color: Colors.grey.withOpacity(0.7)),
            ListTile(
              trailing: const Icon(Icons.abc_sharp),
              title: const Text("性别"),
              onTap: () {},
            ),
            Container(height: 20, color: Colors.grey.withOpacity(0.5)),
            ListTile(
              trailing: const Icon(Icons.abc_sharp),
              title: const Text("地区"),
              onTap: () {},
            ),
            Divider(height: 8, color: Colors.grey.withOpacity(0.7)),
            ListTile(
              trailing: const Icon(Icons.abc_sharp),
              title: const Text("个性签名"),
              onTap: () {},
            ),
            Divider(height: 8, color: Colors.grey.withOpacity(0.7)),
          ],
        ));
  }
}

Widget profile() {
  return Container(
    child: Text("头像"),
  );
}

Widget name() {
  return Container(
    child: Text("名字"),
  );
}

Widget WeChat() {
  return Container(
    child: Text("微信号"),
  );
}

Widget QRcode() {
  return Container(
    child: Text("我的二维码"),
  );
}

Widget gender() {
  return Container(
    child: Text("性别"),
  );
}

Widget area() {
  return Container(
    child: Text("地区"),
  );
}

Widget Personal() {
  return Container(
    child: Text("个性签名"),
  );
}
