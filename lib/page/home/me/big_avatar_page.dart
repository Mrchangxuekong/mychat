import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';
import 'package:wechat_camera_picker/wechat_camera_picker.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';
import 'package:zhongdingapp/util/image_util.dart';

class BigAvatarPage extends StatefulWidget {
  const BigAvatarPage({super.key});

  @override
  State<BigAvatarPage> createState() => _BigAvatarPageState();
}

class _BigAvatarPageState extends State<BigAvatarPage> {
  List<ItemTitle> list = [];
  var c = "".obs;
  @override
  void initState() {
    list.add(ItemTitle(
        title: "拍照",
        dismissHandle: () {
          photograph();
        }));
    list.add(ItemTitle(
        title: "手机相册选择",
        dismissHandle: () {
          print("点击了手机相册选择");
        }));
    list.add(ItemTitle(
        title: "保存图片",
        dismissHandle: () {
          ImageUtil.saveNetworkImage(
              "http://192.168.1.13/static/2024/05/21/d46f021c-ee14-4b50-9f29-d0c025a7fd74.jpg");
        }));
    list.add(ItemTitle(
        title: "取消",
        dismissHandle: () {
          print("点击了取消");
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //"https://q5.itc.cn/q_70/images03/20240312/dafb237d64634fbabff5c406eea3ff0b.jpeg"
    return Scaffold(
      appBar: AppBar(
        title: Text("点击头像"),
        actions: [
          InkWell(
            child: Container(
              padding: EdgeInsets.only(right: 10),
              child: Icon(Icons.add),
            ),
            onTap: () {
              getBottomSheet();
            },
          )
        ],
      ),
      body: PhotoView(
          imageProvider: const NetworkImage(
              "https://q5.itc.cn/q_70/images03/20240312/dafb237d64634fbabff5c406eea3ff0b.jpeg")),
    );
  }

  void getBottomSheet() {
    Get.bottomSheet(
        Wrap(
          children: [
            ...list.map(
              (e) => Container(
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: ThemeColor.grayLine100))),
                  child: InkWell(
                    child: ListTile(
                      title: Text(
                        e.title,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    onTap: () {
                      e.dismissHandle!();
                    },
                  )),
            )
          ],
        ),
        backgroundColor: Colors.white);
  }

  ///拍照
  void photograph() async {
    AssetEntity? camera = await CameraPicker.pickFromCamera(context);
    if (camera == null) {
      return;
    }
    print((await camera.file)?.path);
  }

  getShow() {
    if (c.value.isNotEmpty) {
      return PhotoView(imageProvider: NetworkImage(c.value));
    }
  }
}
