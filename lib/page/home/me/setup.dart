import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:zhongdingapp/controller/registController.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SetupPage extends StatelessWidget {
  final RegistController contrller = Get.find<RegistController>(); //注册控制器
  SetupPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("设置"),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          ListTile(
            trailing: const Icon(Icons.chevron_right),
            title: const Text("建议与反馈"),
            onTap: () {
              Get.to(() => services());
            },
          ),
          Divider(height: 8, color: Colors.grey.withOpacity(0.7)),
          ListTile(
            trailing: const Icon(Icons.chevron_right),
            title: const Text("隐私与服务协议"),
            onTap: () {
              contrller.goToAgreement();
            },
          ),
          Divider(height: 15, color: Colors.grey.withOpacity(0.7)),
          const ListTile(
            /*  dense: true, */
            title: Text("关于"),
            subtitle: Text("众鼎App v1.38", textAlign: TextAlign.end),
          ),
          Divider(height: 8, color: Colors.grey.withOpacity(0.7)),
          ListTile(
            trailing: const Icon(Icons.chevron_right),
            title: const Text("修改密码"),
            onTap: () {
              Get.to(() => modifyWidget());
              print(123);
            },
          ),
        ],
      ),
    );
  }
}

Widget modifyWidget() {
  //文本监视器
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  RxString errorMessage = ''.obs;
  //三秒后清除错误信息
  void cleanErro() {
    Timer(Duration(seconds: 3), () {
      errorMessage.value = '';
    });
  }

  return Scaffold(
    appBar: AppBar(title: const Text("设置密码"), centerTitle: true),
    body: Column(
      children: [
        Container(
          alignment: Alignment.center,
          height: 80,
          margin: const EdgeInsets.symmetric(horizontal: 15),
          child: const TextField(
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                /*边角*/
                borderRadius: BorderRadius.all(
                  Radius.circular(20), //边角为20
                ),
                borderSide: BorderSide(
                  color: Colors.blue, //边线颜色为黄色
                  width: 2, //边线宽度为2
                ),
              ),
              focusedBorder: OutlineInputBorder(
                  /*边角*/
                  borderRadius: BorderRadius.all(
                    Radius.circular(20), //边角为20
                  ),
                  borderSide: BorderSide(
                    color: Colors.blue, //边框颜色为蓝色
                    width: 2, //宽度为5
                  )),
              labelText: "旧密码",
              hintText: "请输入旧密码...",
              // prefixIcon: Icon(Icons.perm_identity),
            ),
          ),
        ),
        const SizedBox(height: 20),
        Container(
          alignment: Alignment.center,
          height: 80,
          margin: const EdgeInsets.symmetric(horizontal: 15),
          child: const TextField(
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                /*边角*/
                borderRadius: BorderRadius.all(
                  Radius.circular(20), //边角为20
                ),
                borderSide: BorderSide(
                  color: Colors.blue, //边线颜色为蓝色
                  width: 2, //边线宽度为2
                ),
              ),
              focusedBorder: OutlineInputBorder(
                  /*边角*/
                  borderRadius: BorderRadius.all(
                    Radius.circular(20), //边角为20
                  ),
                  borderSide: BorderSide(
                    color: Colors.blue, //边框颜色为绿色
                    width: 2, //宽度为5
                  )),
              labelText: "新密码",
              hintText: "请输入新密码...",
              // prefixIcon: Icon(Icons.perm_identity),
            ),
          ),
        ),
        const SizedBox(height: 20),
        Container(
          alignment: Alignment.center,
          height: 80,
          margin: const EdgeInsets.symmetric(horizontal: 15),
          child: const TextField(
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                /*边角*/
                borderRadius: BorderRadius.all(
                  Radius.circular(20), //边角为20
                ),
                borderSide: BorderSide(
                  color: Colors.blue, //边线颜色为黄色
                  width: 2, //边线宽度为2
                ),
              ),
              focusedBorder: OutlineInputBorder(
                  /*边角*/
                  borderRadius: BorderRadius.all(
                    Radius.circular(20), //边角为20
                  ),
                  borderSide: BorderSide(
                    color: Colors.blue, //边框颜色为绿色
                    width: 2, //宽度为5
                  )),
              labelText: "新密码",
              hintText: "请确定新密码...",
              // prefixIcon: Icon(Icons.perm_identity),
            ),
          ),
        ),
        Container(height: 20),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround, //按钮平分父容器的空间

            children: [
              ElevatedButton(
                onPressed: () {
                  String oldPassword = oldPasswordController.text;
                  String newPassword = newPasswordController.text;
                  String confirmPassword = confirmPasswordController.text;
                  // 添加修改密码的逻辑
                  String storedPassword = "123456"; // 假设正确的旧密码
                  if (oldPassword != storedPassword) {
                    errorMessage.value = '旧密码输入错误，请重新输入';
                  } else if (newPassword.length < 8) {
                    errorMessage.value = '新密码长度不能少于八位,请重新输入';
                  }

                  // 验证新密码和确认密码是否一致
                  else if (newPassword != confirmPassword) {
                    errorMessage.value = '新密码和确认密码不一致，请重新输入';
                  } else
                    // 假设密码修改成功
                    errorMessage.value = "密码修改成功"; // 更新错误消息
                  cleanErro();
                },
                child: const Text('提交'),
              ),
              ElevatedButton(
                  onPressed: () {
                    cleanErro();
                    Get.back();
                  },
                  child: const Text("取消")),
            ],
          ),
        ),
        Obx(() => Text(
              errorMessage.value, //实时更新错误信息
              style: const TextStyle(color: Colors.red),
            ))
      ],
    ),
  );
}

Widget services() {
  return Scaffold(
      appBar: AppBar(title: const Text("建议与反馈"), centerTitle: true),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: double.infinity,
            height: 200,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10)),
            child: const TextField(
              decoration:
                  InputDecoration(border: InputBorder.none, hintText: "请输入内容"),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                    onPressed: () {
                      Get.back();
                    },
                    child: Text("提交")),
                ElevatedButton(
                    onPressed: () {
                      Get.back();
                    },
                    child: Text("取消"))
              ],
            ),
          )
        ],
      ));
}
