import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/controller/search_user_phone_controller.dart';
import 'package:zhongdingapp/routerHelper.dart';

class AddFriendBySearch extends StatefulWidget {
  const AddFriendBySearch({super.key});

  @override
  State<AddFriendBySearch> createState() => _AddFriendBySearchState();
}

class _AddFriendBySearchState extends State<AddFriendBySearch> {
  var getC = Get.find<SearchUserPhoneController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeColor.appbarBackground,
      appBar: AppBar(
        backgroundColor: ThemeColor.white,
        actions: const [
          Icon(
            MyIcons.more,
            size: 30,
          ),
          SizedBox(
            width: 16,
          )
        ],
      ),
      body: Container(
        child: Column(
          children: [
            getHead(),
            const SizedBox(
              height: 12,
            ),

            ///根据是否是好友显示不同的item
            ...getIsFriends(getC.friendVo07.value.isFriend != "N"),
          ],
        ),
      ),
    );
  }

  //生成头部方法
  Widget getHead() {
    return Container(
      decoration: const BoxDecoration(
          color: ThemeColor.white,
          border: Border(bottom: BorderSide(color: ThemeColor.line))),
      padding: const EdgeInsets.only(bottom: 50, left: 40, top: 20),
      child: Row(
        children: [
          Container(
            alignment: Alignment.center,
            width: 70,
            height: 70,
            child: CachedNetworkImage(
              fit: BoxFit.fitWidth,
              imageUrl: getC.friendVo07.value.portrait!,
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          Expanded(
              child: Container(
            margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            height: 80,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(getC.friendVo07.value.nickName!),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        style: TextStyle(letterSpacing: 1),
                        "微信号: ${getC.friendVo07.value.chatNo}",
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ))
        ],
      ),
    );
  }

  getIsFriends(bool addFriend) {
    List<Widget> list = [];
    if (addFriend) {
      return getFriends(list);
    }

    ///设置备注和标签
    list.add(Container(
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
      color: Colors.white,
      child: const Row(
        children: [
          Expanded(child: Text("设置备注和标签")),
          Icon(Icons.chevron_right),
          SizedBox(
            width: 10,
          )
        ],
      ),
    ));

    ///来源
    list.add(Container(
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
      margin: const EdgeInsets.symmetric(vertical: 12),
      color: Colors.white,
      child: const Row(
        children: [
          Text("来源"),
          SizedBox(
            width: 40,
          ),
          Expanded(child: Text("来自手机通讯录")),
        ],
      ),
    ));

    ///添加到通讯录
    list.add(InkWell(
      onTap: () {
        Get.toNamed(RouterHelper.applyAddFriendPage, arguments: {
          R.userId: getC.friendVo07.value.userId,
          R.source: Get.arguments[R.source]
        });
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
        margin: const EdgeInsets.symmetric(vertical: 12),
        alignment: Alignment.center,
        color: Colors.white,
        child: Text(
          "添加到通讯录",
          style:
              TextStyle(fontSize: 17, color: ThemeColor.blue.withOpacity(0.6)),
        ),
      ),
    ));
    return list;
  }

  getFriends(List<Widget> list) {
    list.add(Container(
        color: ThemeColor.white,
        child: Row(
          children: [
            SizedBox(
              width: 10,
            ),
            const Text("朋友圈"),
            const SizedBox(
              width: 25,
            ),
            Expanded(
                child: Container(
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                ...getListImg(),
              ]),
              padding: EdgeInsets.symmetric(vertical: 9),
            )),
            Icon(Icons.chevron_right)
          ],
        )));

    list.add(Container(
      margin: const EdgeInsets.only(top: 1, bottom: 10),
      color: ThemeColor.white,
      padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 10),
      child: const Row(
        children: [
          Expanded(
              child: Text(
            "更多信息",
            style: TextStyle(fontSize: 15),
          )),
          Icon(Icons.chevron_right)
        ],
      ),
    ));
    list.add(Container(
      margin: const EdgeInsets.only(top: 1, bottom: 10),
      color: ThemeColor.white,
      padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 10),
      child: const Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            MyIcons.message,
            color: ThemeColor.blue,
          ),
          Text(
            "发消息",
            style: TextStyle(fontSize: 18, color: ThemeColor.blue),
          ),
        ],
      ),
    ));

    return list;
  }

  getListImg() {
    List<Widget> list = [];
    List<String> imgs = [
      MyIcons.networkImg1,
      MyIcons.networkImg2,
      MyIcons.networkImg3,
      MyIcons.networkImg4
    ];
    for (int i = 0; i < imgs.length; i++) {
      list.add(CachedNetworkImage(
        fit: BoxFit.cover,
        height: 50,
        width: 50,
        imageUrl: imgs[i],
        placeholder: (context, url) => const CircularProgressIndicator(),
        errorWidget: (context, url, error) => const Icon(Icons.error),
      ));
      list.add(const SizedBox(
        width: 7,
      ));
    }
    return list;
  }
}
