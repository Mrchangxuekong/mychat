import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/controller/homeController.dart';
import 'package:zhongdingapp/widget/avatar_item02.dart';
import 'package:zhongdingapp/widget/list_title_item_01.dart';
import 'package:zhongdingapp/widget/search_item.dart';
import 'package:zhongdingapp/widget/title_item.dart';

class AddFriendListPage extends StatelessWidget {
  const AddFriendListPage({super.key});

  @override
  Widget build(BuildContext context) {
    var getC = Get.find<HomeController>();
    Future<void> _refreshList() async {
      getC.initCommunication(1);
    }

    //TODO  后续在区分3天前的数据和3天后的数据分层展示标题
    //getC.applyVo02List.value.sort((a,b)=>DateTime.parse(a.createTime!).compareTo(DateTime.parse(b.createTime!)));
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.grey[200],
          title: const Text("新的朋友"),
          centerTitle: true,
          actions: const [
            Text("添加朋友"),
            SizedBox(
              width: 20,
            )
          ],
        ),
        body: Obx(() => ListView.builder(
            itemCount: getC.applyVo02List.value.length,
            itemBuilder: (context, index) {
              var e = getC.applyVo02List.value[index];
              if (index == 0) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SearchItem(onTap: () {
                      print("点击了手机号");
                    }),
                    const ListTitleItem01(
                      title: "手机",
                      left: Icons.phone,
                      right: Icons.chevron_right,
                    ),
                    const TitleItem(title: "近三1天"),
                    AvatarItem02(
                      needLine: index != getC.applyVo02List.value.length - 1,
                      fn: () {
                        getC.agreeAddFriend(e);
                      },
                      bean: e,
                    )
                  ],
                );
              } else {
                return AvatarItem02(
                  needLine: index != getC.applyVo02List.value.length - 1,
                  fn: () {
                    getC.agreeAddFriend(e);
                  },
                  bean: e,
                );
              }
            })));
  }
}
