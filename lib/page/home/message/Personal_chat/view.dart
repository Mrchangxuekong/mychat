import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'index.dart';
import 'widgets/bar_item.dart';

class PersonalChatPage extends StatefulWidget {
  const PersonalChatPage({Key? key}) : super(key: key);

  @override
  State<PersonalChatPage> createState() => _PersonalChatPageState();
}

class _PersonalChatPageState extends State<PersonalChatPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return const _PersonalChatViewGetX();
  }
}

class _PersonalChatViewGetX extends GetView<PersonalChatController> {
  const _PersonalChatViewGetX({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView() {
    return const WidgetBar();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PersonalChatController>(
      init: PersonalChatController(),
      id: "聊天详情",
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.grey[300],
              centerTitle: true,
              title: const Text("聊天详情"),
            ),
            body: SafeArea(
              child: _buildView(),
            ));
      },
    );
  }
}
