import 'package:get/get.dart';

class PersonalChatController extends GetxController {
  PersonalChatController();

  _initData() {
    update(["personal_chat"]);
  }

  void onTap() {}

  // @override
  // void onInit() {
  //   super.onInit();

  // }

  @override
  void onReady() {
    super.onReady();
    _initData();
  }

  // @override
  // void onClose() {
  //   super.onClose();
  // }
}
