import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/my_icons.dart';

class WidgetBar extends StatefulWidget {
  const WidgetBar({super.key});

  @override
  State<WidgetBar> createState() => _WidgetBarState();
}

class _WidgetBarState extends State<WidgetBar> {
  bool _switch = false; //是否选中
  bool _chat = false;
  bool _voice = false;
  List<String> list = [
    MyIcons.networkImg1,
  ];

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                Image.asset(
                  "assets/splash/aaa.jpg",
                  height: 50,
                  width: 50,
                ),
              ]),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                child: Image.asset(
                  "assets/splash/aaa.jpg",
                  height: 50,
                  width: 50,
                ),
              ),
            ),
          ],
        ),
        const Padding(
          padding: EdgeInsets.only(left: 20, bottom: 10, top: 0),
          child: Text("柒玖"),
        ),
        Container(height: 10, color: Colors.grey[300]),
        ListTile(
          title: const Text("聊天记录"),
          trailing: const Icon(Icons.chevron_right_outlined),
          onTap: () {
            print("点击了聊天记录");
          },
        ),
        Container(height: 10, color: Colors.grey[300]),
        Column(
          children: <Widget>[
            Container(
              child: ListTile(
                title: const Text('消息免打扰'),
                onTap: () {},
                trailing: Switch(
                  value: _switch, //当前状态
                  onChanged: (value) {
                    //重新构建页面
                    setState(() {
                      _switch = value;
                    });
                  },
                  trackOutlineColor: MaterialStateProperty.all(Colors.white),
                  thumbColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.white; //开启状态的颜色
                    }
                    return Colors.white; //关闭状态的颜色
                  }),
                  trackColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.blue; //开启状态的轨迹颜色
                    }
                    return Colors.grey.withOpacity(0.3); //关闭状态的轨迹颜色
                  }),
                ),
              ),
            ),
            Divider(color: Colors.grey[300], height: 1),
            Container(
              child: ListTile(
                onTap: () {},
                title: const Text('置顶聊天'),
                trailing: Switch(
                  value: _chat, //当前状态
                  onChanged: (value) {
                    //重新构建页面
                    setState(() {
                      _chat = value;
                    });
                  },
                  thumbColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.white; //开启状态的颜色
                    }
                    return Colors.white; //关闭状态的颜色
                  }),
                  trackOutlineColor: MaterialStateProperty.all(Colors.white),
                  trackColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.blue; //开启状态的轨迹颜色
                    }
                    return Colors.grey.withOpacity(0.3); //关闭状态的轨迹颜色
                  }),
                ),
              ),
            ),
            Divider(color: Colors.grey[300], height: 1),
            Container(
              child: ListTile(
                onTap: () {},
                title: const Text('提醒'),
                trailing: Switch(
                  value: _voice, //当前状态
                  onChanged: (value) {
                    //重新构建页面
                    setState(() {
                      _voice = value;
                    });
                  },
                  trackOutlineColor: MaterialStateProperty.all(Colors.white),
                  thumbColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.white; //开启状态的颜色
                    }
                    return Colors.white; //关闭状态的颜色
                  }),
                  trackColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.blue; //开启状态的轨迹颜色
                    }
                    return Colors.grey.withOpacity(0.3); //关闭状态的轨迹颜色
                  }),
                ),
              ),
            ),
            Container(height: 10, color: Colors.grey[300]),
            ListTile(
              onTap: () {
                print("点击了聊天背景");
              },
              title: const Text("设置当前聊天背景"),
              trailing: const Icon(Icons.chevron_right_outlined),
            ),
            Container(height: 10, color: Colors.grey[300]),
            ListTile(
              onTap: () {
                print("点击了清空聊天记录");
              },
              title: const Text("清空聊天记录"),
              trailing: const Icon(Icons.chevron_right_outlined),
            ),
            Container(height: 10, color: Colors.grey[300]),
            ListTile(
              onTap: () {},
              title: const Text("投诉"),
              trailing: const Icon(Icons.chevron_right_outlined),
            ),
          ],
        )
      ],
    );
  }
}
