import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/controller/apply_add_friend_controller.dart';
import 'package:zhongdingapp/entity/chat/friend_vo02.dart';

///申请添加朋友
class ApplyAddFriendPage extends StatefulWidget {
  const ApplyAddFriendPage({super.key});

  @override
  State<ApplyAddFriendPage> createState() => _ApplyAddFriendPageState();
}

class _ApplyAddFriendPageState extends State<ApplyAddFriendPage> {
  var getC=Get.find<ApplyAddFriendController>();
  TextEditingController inputReason=TextEditingController();


  @override
  Widget build(BuildContext context) {
    inputReason.text="添加好友";
    return Scaffold(
      backgroundColor: ThemeColor.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ThemeColor.white,
        title: const Text("申请添加朋友"),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: ListView(
          children: [
            getTitle("发送添加朋友申请"),
            Container(
              decoration: BoxDecoration(color: ThemeColor.gray20,
                  borderRadius: BorderRadius.circular(5)),

              child:  TextField(
                controller: inputReason,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  filled: true, // 设置为filled
                  fillColor: ThemeColor.gray20, // 设置背景颜色
                ),
                maxLines: 3,
                minLines: 3,
              ),
            ),
            Container(
                child: const Row(
                  children: [
                    Expanded(
                      child: Text("常用申请语：不填，默认(添加好友)",
                          style: TextStyle(fontSize: 10)),
                    ),
                    InkWell(
                      child: Text(
                        "填入",
                        style: TextStyle(color: ThemeColor.blue),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    )
                  ],
                )),

            ///设置备注
            getTitle("设置备注"),
            Container(
              decoration: BoxDecoration(color: ThemeColor.gray20,
                  borderRadius: BorderRadius.circular(5)),

              child: const TextField(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  filled: true, // 设置为filled
                  fillColor: ThemeColor.gray20, // 设置背景颜色
                ),
                maxLines: 2,
                minLines: 1,
              ),
            ),

            ///设置备注的下方文字
            Container(
                child: const Row(
                  children: [
                    Expanded(
                      child: Text("对方在手机通讯录中为“邓丽君”",
                          style: TextStyle(fontSize: 10)),
                    ),
                    InkWell(
                      child: Text(
                        "填入",
                        style: TextStyle(color: ThemeColor.blue),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    )
                  ],
                )),

            getTitle("添加标签和描述"),
            getItemTagAndDesc("标签", () {
              print("点击了标签");
            }),
            Container(color: ThemeColor.gray, height: 1,),
            getItemTagAndDesc("描述", () {
              print("点击了描述");
            }),
            getTitle("设置朋友权限"),
            getItemAuth("聊天、朋友、微信运动等"),
            Container(color: ThemeColor.gray, height: 1,),
            getItemAuth("仅聊天"),
            Center(
              child:Container(
                margin: const EdgeInsets.only(top: 15),
                child:  ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(ThemeColor.green300),),
                  onPressed: () {

                    FriendVo02 vo02=FriendVo02(userId: Get.arguments[R.userId],source:Get.arguments[R.source],reason: inputReason.text);
                    getC.applyAdd(vo02);
                  },
                  child: const Text("发送",style: TextStyle(fontSize: 20,letterSpacing:15,color: Colors.white),),),
              )
              ,)

          ],
        ),
      ),
    );
  }

  getItemTagAndDesc(String title, VoidCallback fn) {
    return InkWell(
        onTap: fn,
        child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),

            decoration: BoxDecoration(color: ThemeColor.gray20,
                borderRadius: BorderRadius.circular(5)),
            child: Row(
                children: [
                  Expanded(child: Text(title)),
                  const Icon(Icons.chevron_right)
                ])));
  }

  getTitle(String title) {
    if (title == "发送添加朋友申请") {
      return Container(
        padding: const EdgeInsets.only(left: 15, bottom: 10),
        child: Text(title, style: TextStyle(fontSize: 13),),
      );
    }
    return Container(
      margin: const EdgeInsets.only(top: 14),
      padding: const EdgeInsets.only(left: 15, bottom: 10),
      child: Text(title, style: TextStyle(fontSize: 13),),
    );
  }

  getItemAuth(String title) {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
        decoration: BoxDecoration(
            color: ThemeColor.gray20, borderRadius: BorderRadius.circular(5)),
        child: Row(
            children: [
              Expanded(child: Text(title)),
              Checkbox(value: getC.check.value, onChanged: (v) {
                getC.check.value = v!;
              })
            ]));
  }
}
