import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/chat/friend_vo06.dart';
import 'package:zhongdingapp/page/home/message/message/message_logic.dart';
import 'package:zhongdingapp/sqllite/bean/message_dao.dart';
import 'package:zhongdingapp/routerHelper.dart';
import 'package:zhongdingapp/sqllite/bean/new_message_record.dart';

class MessagePage extends StatelessWidget {
  const MessagePage({super.key});

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(MessageLogic());
    logic.onInit();
    return  Scaffold(
        body:  GetBuilder<MessageLogic>(
            init: logic,//近在第一次初始化
            builder: (c) =>  Column(
              children: [
                Container(
                  color: ThemeColor.black2,
                  width: double.maxFinite,
                  height: 66,
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: ThemeColor.white),
                      margin: const EdgeInsets.all(10),
                      child: const Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.search_sharp),
                              SizedBox(
                                width: 7,
                              ),
                              Text("搜索"),
                            ],
                          ),
                        ],
                      )),
                ),
                Expanded(
                    child: ListView.builder(
                      itemBuilder: (context, item) {
                        return listviewItem(context, logic.messageDaoList.value[item]);
                      },
                      itemCount: logic.messageDaoList.value.length,
                    )),
              ],
            )

        ));
  }
  Widget listviewItem(BuildContext context, NewMessageRecord item) {

    FriendVo06 friendVo06=FriendVo06(userId:item.userId,dismissHandle:(){},portrait: item.portrait,nickName: item.nickName,userType: "");
    return  InkWell(onTap: (){
      if(item.isGroupMessage==0){
        Get.toNamed(RouterHelper.chatSinglePage,arguments: {"friendVo6":friendVo06});
      }else{
        Get.toNamed(RouterHelper.chatGroupPage,arguments: {"friendVo6":friendVo06});
      }
    },child: Row(
      children: [
        Stack(children: [
          Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
            ),
            margin: const EdgeInsets.all(12),
            height: 66,
            width: 66,
            child:
            CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl:item.portrait!,
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          item.sqlNoIsReadNum==0?const Text(""):Positioned(
              right: 13/2,
              top: 13/2,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(18)),
                child: Text(
                  item.sqlNoIsReadNum!.toString() ,
                  style: const TextStyle(color: Colors.white, fontSize: 8),
                ),
              ))
        ],),
        Expanded(
            child: Container(
              height: 66,
              decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(color: ThemeColor.black2))),
              child:  Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(item.nickName!),
                      ),
                      Text(
                        DateFormat('MM-dd HH:mm:ss').format(item.createTime!),
                        style: const TextStyle(fontSize: 10,color: ThemeColor.hintText20),
                      ),
                      const SizedBox(width: 10,)
                    ],
                  ),
                  const SizedBox(height: 5,),
                  Expanded(child: Text(
                  item.content! ,
    style: const TextStyle(fontSize: 10,color: ThemeColor.hintText20),))

                ],
              ),
            )),
      ],
    ),)  ;
  }

  showContext(MessageDao item) {
    switch(item.type){
      case 0:
        return Text(
          item.content! ,
          style: const TextStyle(fontSize: 10,color: ThemeColor.hintText20),
        );
      case 1:
        return const Text(
          "[图片]" ,
          style: TextStyle(fontSize: 10,color: ThemeColor.hintText20),
        );
      case 2:
        return const Text(
          "[视频]" ,
          style: TextStyle(fontSize: 10,color: ThemeColor.hintText20),
        );
      case 3:
        return const Text(
          "[文件]" ,
          style: TextStyle(fontSize: 10,color: ThemeColor.hintText20),
        );
    }
  }


}
