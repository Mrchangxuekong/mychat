import 'package:flutter/material.dart';

class Friendpermissions extends StatefulWidget {
  const Friendpermissions({super.key});

  @override
  State<Friendpermissions> createState() => _FriendpermissionsState();
}

class _FriendpermissionsState extends State<Friendpermissions> {
  bool _isChecked = true;
  bool _isChecked1 = false;
  bool _isSwitch = false;
  bool _isSwitch1 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("朋友权限"),
      ),
      body: ListView(
        children: [
          const Padding(
              padding: EdgeInsets.only(left: 15),
              child: Text(
                "设置朋友权限",
              )),
          ListTile(
            title: const Text("聊天、朋友圈、微信运动等"),
            trailing: Checkbox(
                value: _isChecked,
                side: const BorderSide(color: Colors.white),
                activeColor: Colors.white, //选中时框的颜色
                checkColor: Colors.blue, //选中时勾的颜色
                onChanged: (value) {
                  setState(() {
                    _isChecked = value!;
                  });
                }),
          ),
          Divider(height: 1, color: Colors.grey[300]),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                title: const Text("仅聊天"),
                trailing: Checkbox(
                    side: const BorderSide(color: Colors.white),
                    activeColor: Colors.white, //选中时框的颜色
                    checkColor: Colors.blue, //选中时勾的颜色
                    value: _isChecked1,
                    onChanged: (value) {
                      setState(() {
                        _isChecked1 = value!;
                      });
                    }),
              ),
              Container(height: 10, color: Colors.grey[300]),
              Container(
                height: 20,
                child: const Padding(
                    padding: EdgeInsets.only(left: 15),
                    child: Text(
                      "朋友圈和状态",
                      textAlign: TextAlign.left,
                    )),
              ),
              ListTile(
                title: const Text("不让他(她)看"),
                trailing: Switch(
                  value: _isSwitch,
                  onChanged: (value) {
                    setState(() {
                      _isSwitch = value;
                    });
                  },
                  thumbColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.white; //开启状态的颜色
                    }
                    return Colors.white; //关闭状态的颜色
                  }),
                  trackOutlineColor: MaterialStateProperty.all(Colors.white),
                  trackColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.blue; //开启状态的轨迹颜色
                    }
                    return Colors.grey.withOpacity(0.3); //关闭状态的轨迹颜色
                  }),
                ),
              ),
              Divider(height: 1, color: Colors.grey[300]),
              ListTile(
                title: const Text("不让他(她)看"),
                trailing: Switch(
                  value: _isSwitch1,
                  onChanged: (value) {
                    setState(() {
                      _isSwitch1 = value;
                    });
                  },
                  thumbColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.white; //开启状态的颜色
                    }
                    return Colors.white; //关闭状态的颜色
                  }),
                  trackOutlineColor: MaterialStateProperty.all(Colors.white),
                  trackColor:
                      MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.selected)) {
                      return Colors.blue; //开启状态的轨迹颜色
                    }
                    return Colors.grey.withOpacity(0.3); //关闭状态的轨迹颜色
                  }),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
