import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';

// ignore: camel_case_types
class Click_on_avatarWidget extends StatefulWidget {
  const Click_on_avatarWidget({super.key});

  @override
  State<Click_on_avatarWidget> createState() => _Click_on_avatarWidgetState();
}

// ignore: camel_case_types
class _Click_on_avatarWidgetState extends State<Click_on_avatarWidget> {
  _mainView() {
    return Container(
      color: Colors.grey[200],
      child: ListView(
        children: [
          Container(height: 20, color: Colors.white),
          Container(
            padding: const EdgeInsets.only(left: 0),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  color: Colors.white,
                  width: 80,
                  height: 80,
                  child: Image.network(
                      "https://q5.itc.cn/q_70/images03/20240312/dafb237d64634fbabff5c406eea3ff0b.jpeg"),
                ),
                Container(width: 16, color: Colors.white),
                Container(
                  width: 160,
                  color: Colors.white,
                  child: const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("柒玖"),
                      SizedBox(height: 5),
                      Text("微信号:ic_739jrrrddddse"),
                      SizedBox(height: 5),
                      Text("地区:北京"),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(height: 30, color: Colors.white),
          Container(height: 10, color: Colors.grey[300]),
          Container(
            color: Colors.white,
            padding: const EdgeInsets.only(left: 15),
            child: const ListTile(
              title: Text("朋友圈"),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
          Divider(height: 1, color: Colors.grey[200]),
          Container(
            color: Colors.white,
            padding: const EdgeInsets.only(left: 15),
            child: const ListTile(
              title: Text("视频号"),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
          Container(height: 10, color: Colors.grey[200]),
          Container(
            height: 50,
            color: Colors.white,
            padding: const EdgeInsets.all(8),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Icon(MyIcons.message), Text("发消息")],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: const Icon(Icons.chevron_left_outlined)),
        automaticallyImplyLeading: false,
      ),
      body: _mainView(),
    );
  }
}
