import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/page/home/message/information/widgets/Friendpermissions.dart';
import 'package:zhongdingapp/page/home/message/information/widgets/notes.dart';

class InformationWidget extends StatefulWidget {
  const InformationWidget({super.key});

  @override
  State<InformationWidget> createState() => _InformationWidgetState();
}

class _InformationWidgetState extends State<InformationWidget> {
  bool _switch = false;
  bool _chat = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[100],
      child: ListView(
        children: [
          ListTile(
            title: const Text("设置备注和标签"),
            trailing: const Icon(Icons.chevron_right_outlined),
            onTap: () {
              Get.to(const NotesWidget());
            },
          ),
          Divider(height: 1, color: Colors.grey[300]),
          ListTile(
            title: const Text("朋友权限"),
            trailing: const Icon(Icons.chevron_right_outlined),
            onTap: () {
              Get.to(const Friendpermissions());
            },
          ),
          Container(height: 10, color: Colors.grey[300]),
          ListTile(
            title: const Text("把他推给朋友"),
            trailing: const Icon(Icons.chevron_right_outlined),
            onTap: () {},
          ),
          Container(height: 10, color: Colors.grey[300]),
          Container(
            child: ListTile(
              onTap: () {},
              title: const Text('设置成星标朋友'),
              trailing: Switch(
                value: _switch, //当前状态
                onChanged: (value) {
                  //重新构建页面
                  setState(() {
                    _switch = value;
                  });
                },
                thumbColor: MaterialStateProperty.resolveWith<Color>((states) {
                  if (states.contains(MaterialState.selected)) {
                    return Colors.white; //开启状态的颜色
                  }
                  return Colors.white; //关闭状态的颜色
                }),
                trackOutlineColor: MaterialStateProperty.all(Colors.white),
                trackColor: MaterialStateProperty.resolveWith<Color>((states) {
                  if (states.contains(MaterialState.selected)) {
                    return Colors.blue; //开启状态的轨迹颜色
                  }
                  return Colors.grey.withOpacity(0.3); //关闭状态的轨迹颜色
                }),
              ),
            ),
          ),
          Container(height: 10, color: Colors.grey[300]),
          Container(
            child: ListTile(
              onTap: () {},
              title: const Text('加入黑名单'),
              trailing: Switch(
                value: _chat, //当前状态
                onChanged: (value) {
                  //重新构建页面
                  setState(() {
                    _chat = value;
                  });
                },
                thumbColor: MaterialStateProperty.resolveWith<Color>((states) {
                  if (states.contains(MaterialState.selected)) {
                    return Colors.white; //开启状态的颜色
                  }
                  return Colors.white; //关闭状态的颜色
                }),
                trackOutlineColor: MaterialStateProperty.all(Colors.white),
                trackColor: MaterialStateProperty.resolveWith<Color>((states) {
                  if (states.contains(MaterialState.selected)) {
                    return Colors.blue; //开启状态的轨迹颜色
                  }
                  return Colors.grey.withOpacity(0.3); //关闭状态的轨迹颜色
                }),
              ),
            ),
          ),
          Divider(height: 1, color: Colors.grey[300]),
          ListTile(
            title: const Text("投诉"),
            trailing: const Icon(Icons.chevron_right_outlined),
            onTap: () {},
          ),
          Container(height: 10, color: Colors.grey[300]),
          Container(
            height: 50,
            color: Colors.white,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return Container(
                        width: double.infinity,
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(10),
                              height: 50,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8)),
                              child: const Text("将联系人“柒玖”删除，同时删除与该联系人的聊天记录"),
                            ),
                            Divider(height: 1, color: Colors.grey[300]),
                            Container(
                              padding: const EdgeInsets.all(10),
                              height: 50,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8)),
                              child: GestureDetector(
                                onTap: () {
                                  Get.back();
                                },
                                child: const Text(
                                  "删除联系人",
                                  style: TextStyle(color: Colors.red),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Container(height: 5, color: Colors.grey[300]),
                            Container(
                              padding: const EdgeInsets.all(10),
                              height: 50,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8)),
                              child: GestureDetector(
                                onTap: () {
                                  Get.back();
                                },
                                child: const Text(
                                  "取消",
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                  print("111");
                },
                child: const Text(
                  "删除联系人",
                  style: TextStyle(color: Colors.red),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
