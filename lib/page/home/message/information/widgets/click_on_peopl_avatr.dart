import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/page/home/message/information/widgets/Friendpermissions.dart';
import 'package:zhongdingapp/page/home/message/information/widgets/notes.dart';

// ignore: camel_case_types
class Click_on_peopl_avatrWidget extends StatefulWidget {
  const Click_on_peopl_avatrWidget({super.key});

  @override
  State<Click_on_peopl_avatrWidget> createState() =>
      _Click_on_peopl_avatrWidgetState();
}

// ignore: camel_case_types
class _Click_on_peopl_avatrWidgetState
    extends State<Click_on_peopl_avatrWidget> {
  _mainView() {
    return Container(
      color: Colors.grey[200],
      child: ListView(
        children: [
          Container(height: 20, color: Colors.white),
          Container(
            padding: const EdgeInsets.only(left: 0),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  color: Colors.white,
                  width: 80,
                  height: 80,
                  child: Image.network(
                      "https://q5.itc.cn/q_70/images03/20240312/dafb237d64634fbabff5c406eea3ff0b.jpeg"),
                ),
                Container(width: 16, color: Colors.white),
                Container(
                  width: 160,
                  color: Colors.white,
                  child: const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("柒玖"),
                      SizedBox(height: 5),
                      Text("微信号:ic_739jrrrddddse"),
                      SizedBox(height: 5),
                      Text("地区:北京"),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(height: 30, color: Colors.white),
          Container(height: 10, color: Colors.grey[300]),
          Container(
            color: Colors.white,
            padding: const EdgeInsets.only(left: 15),
            child: ListTile(
              onTap: () {
                Get.to(const NotesWidget());
              },
              title: const Text("标签"),
              trailing: const Icon(Icons.chevron_right),
            ),
          ),
          Divider(height: 1, color: Colors.grey[200]),
          Container(
            color: Colors.white,
            padding: const EdgeInsets.only(left: 15),
            child: ListTile(
              onTap: () {
                Get.to(const Friendpermissions());
              },
              title: const Text("朋友权限"),
              trailing: const Icon(Icons.chevron_right),
            ),
          ),
          Container(height: 10, color: Colors.grey[200]),
          Container(
            color: Colors.white,
            padding: const EdgeInsets.only(left: 15),
            child: ListTile(
              onTap: () {},
              title: const Text("朋友圈"),
              trailing: const Icon(Icons.chevron_right),
            ),
          ),
          Divider(height: 1, color: Colors.grey[200]),
          Container(
            color: Colors.white,
            padding: const EdgeInsets.only(left: 15),
            child: ListTile(
              onTap: () {
                Get.to(const SocialmedaWidget());
              },
              title: const Text("更多信息"),
              trailing: const Icon(Icons.chevron_right),
            ),
          ),
          Container(height: 10, color: Colors.grey[200]),
          Container(
            height: 50,
            color: Colors.white,
            padding: const EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(MyIcons.message, color: Colors.blue[200]),
                Text("发消息", style: TextStyle(color: Colors.blue[200]))
              ],
            ),
          ),
          Divider(height: 1, color: Colors.grey[200]),
          Container(
            height: 50,
            color: Colors.white,
            padding: const EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(MyIcons.video, color: Colors.blue[200]),
                Text(
                  "音视频通话",
                  style: TextStyle(color: Colors.blue[200]),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: GestureDetector(
              onTap: () {
                Get.to(const NotesWidget());
              },
              child: const Icon(Icons.more_horiz_outlined),
            ),
          )
        ],
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: const Icon(Icons.chevron_left_outlined)),
        automaticallyImplyLeading: false,
      ),
      body: _mainView(),
    );
  }
}

class SocialmedaWidget extends StatefulWidget {
  const SocialmedaWidget({super.key});

  @override
  State<SocialmedaWidget> createState() => _SocialmedaWidgetState();
}

class _SocialmedaWidgetState extends State<SocialmedaWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("社交资料"),
        backgroundColor: Colors.grey[200],
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: const Icon(Icons.chevron_left_outlined)),
        automaticallyImplyLeading: false,
      ),
      body: Container(
        color: Colors.grey[200],
        child: ListView(
          children: [
            Container(
              color: Colors.white,
              padding: const EdgeInsets.all(3),
              child: const ListTile(
                title: Text("我和他的共同群聊"),
                trailing: Text("0个"),
              ),
            ),
            Container(height: 10, color: Colors.grey[200]),
            Container(
              color: Colors.white,
              padding: const EdgeInsets.all(3),
              child: const ListTile(
                title: Text("我和他的共同群聊"),
                trailing: Text("*******"),
              ),
            ),
            Divider(height: 1, color: Colors.grey[200]),
            Container(
              color: Colors.white,
              padding: const EdgeInsets.all(3),
              child: const ListTile(
                title: Text("我和他的共同群聊"),
                trailing: Text("对方通过扫一扫添加"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
