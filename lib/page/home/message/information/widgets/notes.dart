import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/util/config.dart';
import 'package:zhongdingapp/widget/appbar.dart';

class NotesWidget extends StatefulWidget {
  const NotesWidget({super.key});

  @override
  State<NotesWidget> createState() => _NotesWidgetState();
}

class _NotesWidgetState extends State<NotesWidget> {
  // 用户名
  final TextEditingController inNikeName = TextEditingController();
  _mainView() {
    return ListView(
      children: [
        Container(
          height: 100,
          width: double.infinity,
          padding: const EdgeInsets.all(30),
          child: const Text(
            "设置备注和标签",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 15),
          child: const Text("备注"),
        ),
        Container(height: 5),
        Container(
          color: Colors.grey[200],
          child: TextField(
            onTap: () {
              inNikeName.text = "";
            },
            controller: inNikeName,
            decoration: (const InputDecoration(
              hintText: "添加备注名",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                borderSide: BorderSide(color: Colors.grey),
              ),
            )),
          ),
        ),
        Container(height: 5),
        Container(
          padding: const EdgeInsets.only(left: 15),
          child: const Text("标签"),
        ),
        Container(height: 5),
        Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(Radius.circular(5)),
            ),
            padding: const EdgeInsets.all(8),
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("添加标签", style: TextStyle(color: Colors.blue[200])),
                const Icon(Icons.chevron_right_outlined),
              ],
            )),
        Container(height: 5),
        Container(
          padding: const EdgeInsets.only(left: 15),
          child: const Text("电话"),
        ),
        Container(height: 5),
        Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(Radius.circular(5)),
            ),
            /*  decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: const BorderRadius.all(Radius.circular(5)),
            ), */
            padding: const EdgeInsets.all(8),
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.add_circle_outline_sharp,
                  color: Colors.blue[200],
                ),
                const SizedBox(width: 8),
                Text("添加电话", style: TextStyle(color: Colors.blue[200])),
              ],
            )),
        Container(height: 5),
        Container(
          padding: const EdgeInsets.only(left: 15),
          child: const Text("描述"),
        ),
        Container(height: 5),
        Container(
          color: Colors.grey[200],
          child: TextField(
              controller: inNikeName,
              decoration: (const InputDecoration(
                hintText: "添加文字",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  borderSide: BorderSide(color: Colors.grey),
                ),
              ))),
        ),
        Container(height: 10),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Padding(padding: EdgeInsets.only(left: 10)),
            Container(
              height: 80,
              width: 80,
              color: Colors.grey[300],
              child: GestureDetector(
                onTap: () {},
                child: const Icon(Icons.add),
              ),
            )
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        leading: GestureDetector(
            onTap: () {
              Get.back();
            },
            child: const Icon(
              Icons.chevron_left,
              size: 30,
              color: Colors.black,
            )),
        actions: [
          GestureDetector(
            child: Padding(
              padding: const EdgeInsets.only(right: spacing),
              child: ElevatedButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: const Text("完成")),
            ),
          ),
        ],
      ),
      body: _mainView(),
    );
  }
}
