import 'package:get/get.dart';

class PersonalController extends GetxController {
  PersonalController();

  _initData() {
    update(["personal"]);
  }

  void onTap() {}

  // @override
  // void onInit() {
  //   super.onInit();
  // }

  @override
  void onReady() {
    super.onReady();
    _initData();
  }

  // @override
  // void onClose() {
  //   super.onClose();
  // }
}
