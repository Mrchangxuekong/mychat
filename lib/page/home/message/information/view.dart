import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/page/home/message/information/widgets/information.dart';

import 'index.dart';

class InformationPage extends StatefulWidget {
  const InformationPage({Key? key}) : super(key: key);

  @override
  State<InformationPage> createState() => _InformationPageState();
}

class _InformationPageState extends State<InformationPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return const _PersonalViewGetX();
  }
}

class _PersonalViewGetX extends GetView<PersonalController> {
  const _PersonalViewGetX({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView() {
    return InformationWidget();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PersonalController>(
      init: PersonalController(),
      id: "个人资料设置",
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.grey[300],
              centerTitle: true,
              title: const Text("资料设置")),
          body: SafeArea(
            child: _buildView(),
          ),
        );
      },
    );
  }
}
