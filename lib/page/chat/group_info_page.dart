import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';

class GroupInfoPage extends StatefulWidget {
  const GroupInfoPage({super.key});

  @override
  State<GroupInfoPage> createState() => _GroupInfoPageState();
}

class _GroupInfoPageState extends State<GroupInfoPage> {
  @override
  Widget build(BuildContext context) {

    ///消息免打扰
    bool disturb=false;
    ///置顶
    bool topping=false;
    ///保存通讯录
    bool saveBook=true;
    List<ItemTitle> list = [];
    list.add(ItemTitle(title: '蔡旭', url: MyIcons.networkImg1, dismissHandle: () {  }));
    list.add(ItemTitle(title: '蔡旭1', url: MyIcons.networkImg2, dismissHandle: () {  }));
    list.add(ItemTitle(title: '蔡旭2', url: MyIcons.networkImg3, dismissHandle: () {  }));
    list.add(ItemTitle(title: '蔡旭34444', url: MyIcons.networkImg4, dismissHandle: () {  }));
    list.add(ItemTitle(title: '蔡旭', url: MyIcons.networkImg1, dismissHandle: () {  }));
    list.add(ItemTitle(title: '蔡旭1', url: MyIcons.networkImg2, dismissHandle: () {  }));
    list.add(ItemTitle(title: '蔡旭2', url: MyIcons.networkImg3, dismissHandle: () {  }));
    list.add(ItemTitle(title: '蔡旭34444', url: MyIcons.networkImg4, dismissHandle: () {  }));
    list.add(ItemTitle(title: '蔡旭34444', url: MyIcons.networkImg4, dismissHandle: () {  }));

    return Scaffold(
      backgroundColor: ThemeColor.gray,
      appBar: getAppbar(),
      body: ListView(
        children: [
          Container(
            color: Colors.white,
            padding: const EdgeInsets.only(left: 25, top: 20, bottom: 10),
            margin: const EdgeInsets.only(bottom: 12),
            child: Wrap(
              runSpacing: 11,
              spacing: 8,
              children: [
                ...list.map((e) => getItemAvatarByUrl(e)),
                const Icon(
                  MyIcons.addRound,
                  size: 45,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Text("查看更多群成员"), Icon(Icons.chevron_right)],
                  ),
                ),
              ],
            ),
          ),
          getItemName("群聊名称", "群聊名称", () {}),
          getQrCode("二维码",(){}),
          getItemKey("群公告",(){}),
          getItemKey("备注",(){},10),
          getItemKey("查找聊天记录",(){},10),
          getOffNo("消息免打扰",disturb,(v){print("消息免打扰$v");}),
          getOffNo("置顶聊天",disturb,(v){print("置顶聊天$v");}),
          getOffNo("保存通讯录",disturb,(v){print("保存通讯录$v");}),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 13),
            margin: const EdgeInsets.only(top: 10),
            decoration: const BoxDecoration(color: Colors.white),
            child:
            const Text("退出群聊",textAlign: TextAlign.center,style: TextStyle(color: Colors.red,fontSize: 18),),)
        ],
      ),
    );
  }

  getItemAvatarByUrl(ItemTitle item) {
    return Container(
      margin: const EdgeInsets.only(right: 12),
      width: 45,
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: CachedNetworkImage(
                height: 45, width: 45, fit: BoxFit.cover, imageUrl: item.url!),
          ),
          Text(
            item.title,
            style: const TextStyle(
              fontSize: 10,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }

  getAppbar() {
    return AppBar(
      backgroundColor: ThemeColor.appbarBackground,
      title: const Row(
        children: [Text("群聊信息(8)"), Icon(MyIcons.bell)],
      ),
      centerTitle: true,
      leading: InkWell(
        child: const Icon(
          MyIcons.leftMain,
          size: 30,
        ),
        onTap: () => Get.back(),
      ),
    );
  }

  getItemKey(String key, VoidCallback fn,[double line=1]) {
    return InkWell(
      onTap: fn,
      child: Container(
        margin: EdgeInsets.only(bottom: line),
        color: Colors.white,
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
        child:  Row(
          children: [
            Expanded(child:  Text(key,),),

            const SizedBox(
              width: 40,
            ),
            const Icon(Icons.chevron_right)
          ],
        ),

      ),
    );
  }

  ///添加群聊
  getItemName(String key, String name, VoidCallback fn) {
    return InkWell(
      onTap: fn,
      child: Container(
        color: Colors.white,
        margin: const EdgeInsets.only(bottom: 1),
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
        child: Row(
          children: [
            Text(key),
            const SizedBox(
              width: 40,
            ),
            Expanded(
                child: Text(name,
                    textAlign: TextAlign.right,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis)),
            const SizedBox(width: 10,),
            const Icon(Icons.chevron_right)
          ],
        ),
      ),
    );
  }
  ///添加二维码
  getQrCode(String key, VoidCallback fn) {
    return InkWell(
      onTap: fn,
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.only(bottom: 1),
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
        child: Row(
          children: [
            Expanded(
              child: Text(key),
            ),
            const Icon(MyIcons.codepick),
            const SizedBox(width: 10,),
            const Icon(Icons.chevron_right)
          ],
        ),
      ),
    );
  }

  ///开关按钮
  getOffNo(String key,bool flag, Function(int?)callback) {
    return  Container(
        color: Colors.white,
        margin: const EdgeInsets.only(bottom: 1),
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
        child: Row(
          children: [
            Expanded(
              child: Text(key),),
            const SizedBox(width: 10,),
            ToggleSwitch(
              minWidth: 65.0,
              minHeight: 35,
              cornerRadius: 10.0,
              activeBgColors: const [[Colors.red], [Colors.green]],
              activeFgColor:ThemeColor.gray20,
              inactiveBgColor: ThemeColor.gray20,
              inactiveFgColor: ThemeColor.gray20,
              labels: ['关', '开'],
              initialLabelIndex: 1,
              onToggle:callback,
            ),
          ],
        ),
    );

    ///开关按钮
    getOffNo1(String s, bool flag, Function(bool) callback) {
      return Switch(value: flag, onChanged: callback);
    }
  }

}
