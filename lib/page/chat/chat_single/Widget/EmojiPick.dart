import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';

class EmojiPickerPage extends StatefulWidget {
  @override
  _EmojiPickerPageState createState() => _EmojiPickerPageState();
}

class _EmojiPickerPageState extends State<EmojiPickerPage> {
  void _toggleEmojiPicker() {
    setState(() {
      _showEmojiPicker = !_showEmojiPicker;
    });
  }

  bool _showEmojiPicker = false;
  final _textController = TextEditingController();
  void _showemojiPicker() {
    showModalBottomSheet(
        context: context,
        constraints: BoxConstraints(
          maxHeight:
              MediaQuery.of(context).size.height * 0.3, // 设置最大高度为屏幕高度的 70%
        ),
        builder: (BuildContext context) {
          return EmojiPicker(
            onEmojiSelected: (category, emoji) {
              _textController.text += emoji.emoji;
              setState(() {
                _showEmojiPicker = false;
              });
            },
            onBackspacePressed: () {
              _textController.text = _textController.text
                  .substring(0, _textController.text.length - 1);
            },
            // ignore: prefer_const_constructors

            config: const Config(
              columns: 7,
              verticalSpacing: 0,
              horizontalSpacing: 0,
              initCategory: Category.RECENT,
              bgColor: Color(0xFFF2F2F2),
              indicatorColor: Colors.blue,
              iconColor: Colors.grey,
              iconColorSelected: Colors.blue,
              backspaceColor: Colors.blue,
              skinToneDialogBgColor: Colors.white,
              skinToneIndicatorColor: Colors.grey,
              enableSkinTones: true,
              recentsLimit: 28,
              tabIndicatorAnimDuration: kTabScrollDuration,
              categoryIcons: const CategoryIcons(),
              buttonMode: ButtonMode.MATERIAL,
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: EmojiPicker(
        onEmojiSelected: (category, emoji) {
          _textController.text += emoji.emoji;
          setState(() {
            _showEmojiPicker = false;
          });
        },
        onBackspacePressed: () {
          _textController.text = _textController.text
              .substring(0, _textController.text.length - 1);
        },
        // ignore: prefer_const_constructors

        config: const Config(
          columns: 7,
          verticalSpacing: 0,
          horizontalSpacing: 0,
          initCategory: Category.RECENT,
          bgColor: Color(0xFFF2F2F2),
          indicatorColor: Colors.blue,
          iconColor: Colors.grey,
          iconColorSelected: Colors.blue,
          backspaceColor: Colors.blue,
          skinToneDialogBgColor: Colors.white,
          skinToneIndicatorColor: Colors.grey,
          enableSkinTones: true,
          recentsLimit: 28,
          tabIndicatorAnimDuration: kTabScrollDuration,
          categoryIcons: const CategoryIcons(),
          buttonMode: ButtonMode.MATERIAL,
        ),
      ),
    );
  }
}
