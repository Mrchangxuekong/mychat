import 'package:flutter/material.dart';

// 上传文件成功后获取的文件信息
class UploadedFile {
  final String url;
  final String fileName;
  final String fileType;
  final int fileSize;

  UploadedFile({
    required this.url,
    required this.fileName,
    required this.fileType,
    required this.fileSize,
  });
}

// 在页面中显示上传的文件
Widget File_uploadWidget(BuildContext context) {
  final List<UploadedFile> uploadedFiles = [
    /*  UploadedFile(
      url: 'https://example.com/file1.pdf',
      fileName: 'file1.pdf',
      fileType: 'application/pdf',
      fileSize: 1024,
    ),
    UploadedFile(
      url: 'https://example.com/file2.docx',
      fileName: 'file2.docx',
      fileType:
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      fileSize: 2048,
    ),
    UploadedFile(
      url: 'https://example.com/file3.xlsx',
      fileName: 'file3.xlsx',
      fileType:
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      fileSize: 4096,
    ),
    UploadedFile(
      url: 'https://example.com/file3.xlsx',
      fileName: 'file3.tet',
      fileType:
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      fileSize: 4096,
    ), */
  ];

  return ListView.builder(
    itemCount: uploadedFiles.length,
    itemBuilder: (context, index) {
      final file = uploadedFiles[index];
      return Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: [
              // 根据文件类型显示不同的缩略图
              if (file.fileType.startsWith('image/'))
                Image.network(
                  file.url,
                  width: 80,
                  height: 80,
                  fit: BoxFit.cover,
                )
              else
                Icon(
                  _getIconForFileType(file.fileType),
                  size: 40,
                ),
              const SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      file.fileName,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    Text(
                      '${file.fileSize.toStringAsFixed(2)} KB',
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}

// 根据文件类型获取对应的图标
IconData _getIconForFileType(String fileType) {
  // 根据文件类型返回合适的图标
  switch (fileType) {
    case 'application/pdf':
      return Icons.picture_as_pdf;
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
    case 'application/msword':
      return Icons.insert_drive_file;
    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
    case 'application/vnd.ms-excel':
      return Icons.insert_chart;
    case "application/msword.tet":
      return Icons.text_snippet_rounded;
    default:
      return Icons.attachment;
  }
}
