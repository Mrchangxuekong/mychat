import 'dart:convert';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:wechat_camera_picker/wechat_camera_picker.dart' as wcp;
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/config/api_url.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';
import 'package:zhongdingapp/entity/base/login_user.dart';
import 'package:zhongdingapp/entity/chat/friend_vo06.dart';
import 'package:zhongdingapp/sqllite/bean/message_dao.dart';
import 'package:zhongdingapp/sqllite/bean/new_message_record.dart';
import 'package:zhongdingapp/sqllite/db_helper.dart';
import 'package:zhongdingapp/sqllite/table_name.dart';
import 'package:zhongdingapp/util/cache_util.dart';
import 'package:zhongdingapp/util/http_dio.dart';
import 'package:zhongdingapp/util/mqtt_utils.dart';
import 'package:zhongdingapp/routerHelper.dart';

class ChatSingleLogic extends GetxController {
  var title = "".obs;
  var inputSend = TextEditingController().obs;
  var marginBottomListview = 0.0.obs;


  ///底部键盘的高度
  ///加载更多
  var addMore = false.obs;
  ///显示键盘
  var showKeyboard = false.obs;
  var isGroupChat = 0.obs;
  ///是否显示底部拍照分页
  var showBottomPage=false.obs;

  ///0单聊还是1群聊
  ///聊天集合
  var msgList = <MessageDao>[].obs;
  Rx<FriendVo06> friendVo06 = FriendVo06.Init(() {}).obs;

  @override
  void onInit() {
    if (Get.arguments != null) {
      friendVo06.value = Get.arguments["friendVo6"];

      Future.wait([fetchData()]).then((List values) {
        print("初始化数据");
        msgList.value = values[0]; //
        print(msgList.value.length);

        ///更新本地为已读    接收人的id, 0单聊
        updateMessage(friendVo06.value.userId!, 0);
        super.onInit();
      });
    }

    ///从本地数据库获取聊天内容
  }

  @override
  void onReady() {
    update();
  }

  ///pageView
  var pageViewIndex = 0.obs;
  List<ItemTitle> addMoreList = [
    ItemTitle(title: R.systemAlbum, icon: MyIcons.album, dismissHandle: () {}),
    ItemTitle(
        title: R.systemShot,
        icon: MyIcons.shot,
        dismissHandle: () {
          print("点击了拍摄");
        }),
    ItemTitle(
        title: R.systemVideoCall,
        icon: MyIcons.video,
        dismissHandle: () {
          print("点击了视频通话");
        }),
    ItemTitle(
        title: R.systemLocation,
        icon: MyIcons.position,
        dismissHandle: () {
          print("点击了位置");
        }),
    ItemTitle(
        title: R.systemInfoCard, //名片
        icon: MyIcons.envelope,
        dismissHandle: () {
          print("点击了红包");
        }),
    ItemTitle(
        title: R.systemFile,// 文件
        icon: MyIcons.transfer,
        dismissHandle: () {
          print("点击了转账");
        }),
    ItemTitle(
        title: "语音输入",
        icon: MyIcons.speech1,
        dismissHandle: () {
          print("点击了语音输入");
        }),
    ItemTitle(
        title: "我的收藏",
        icon: MyIcons.pentagram,
        dismissHandle: () {
          print("点击了我的收藏");
        }),
    ItemTitle(
        title: "转账",
        icon: MyIcons.transfer,
        dismissHandle: () {
          print("点击了转账");
        }),
    ItemTitle(
        title: "语音输入",
        icon: MyIcons.speech1,
        dismissHandle: () {
          print("点击了语音输入");
        }),
    ItemTitle(
        title: "我的收藏",
        icon: MyIcons.pentagram,
        dismissHandle: () {
          print("点击了我的收藏");
        }),
  ];

  ///监听是否加载右方按钮
  void changAddMore() {
    showBottomPage.value = !showBottomPage.value;
    update();
  }

  void changInputText() {
    if (inputSend.value.text.isEmpty) {
      addMore.value = true;
    }else {
      addMore.value = false;
    }
    update();
  }

  //mqtt 发送消息
  void sendMessage() async {
    if(inputSend.value.text.isEmpty){
      Get.snackbar("提示", "不能发送空消息!");
      return;
    }
   await send(inputSend.value.text, 0);
    inputSend.value.text = "";
    update();
  }

  ///发送消息方法 content(内容) type（0，文字，1（图片） coverUrl 封面地址）
    Future send(String content,int type,{String? coverUrl}) async{
    if(content.isEmpty){
      return;
    }
    var e = friendVo06.value;
    LoginUser user = await CacheUtil.getUser();
    MessageDao messageDao = MessageDao(
        senderId: user.userId,
        sendNickName: user.nickName,
        sendPortrait: user.portrait,
        receiverId: e.userId,
        nickName: e.nickName,
        portrait: e.portrait,
        content: content,
        coverUrl: coverUrl,
        createTime: DateTime.now(),
        type: type,
        self: 1,
        isRead: 1,
        isGroupMessage: 0);
    await Mqttutils.sendMessage(
        jsonEncode(messageDao.toJson()), "${R.mqttSingle}${e.userId}");
    ///插入聊天记录
    await DBHelper().insert(TableName.message,messageDao.toJson());
    ///插入消息表数据
    insertNewMessageRecord(messageDao);
    ///本地保存发送信息
    msgList.value = await fetchData();
  }

  void updateMsgList() async {
    if (friendVo06.value.userId != null) {
      msgList.value = await fetchData();
      update();
    }
  }

  ///跳转单聊页面
  void toNameChatSinglePage(FriendVo06 item) async {
    /* var res = await DBHelper().queryByCurrUserId(item.userId!);
    List<MessageDao> listr= MessageDao.fromJsonList(res);
    print(listr);*/
    friendVo06.value = item;
    Get.toNamed(RouterHelper.chatSinglePage);
  }

  ///接收消息回调
  Future<List<MessageDao>> fetchData() async {
    LoginUser user = await CacheUtil.getUser();
    String sql = "SELECT * FROM messages t WHERE "
        " (t.receiverId=${friendVo06.value.userId!} and t.senderId=${user.userId} and t.isGroupMessage=${isGroupChat.value} and self=1) "

        ///自己发的
        " or (t.senderId=${friendVo06.value.userId!} and t.receiverId=${user.userId} and t.isGroupMessage=${isGroupChat.value} and self=0)"

        ///对方发的
        " ORDER BY t.createTime";
    var querySql = await DBHelper().querySql(sql);
    List<MessageDao> fromJsonList = MessageDao.fromJsonList(querySql);

    ///因为插件问题，需要列表反转才符合需求，后期在改
    /// //列表反转
    fromJsonList = fromJsonList.reversed.toList();
    return fromJsonList;
  }

  @override
  void onClose() {
    print("页面关闭了");
    super.onClose();
  }

  ///更新本地数据标记为已读
  void updateMessage(String userId, int isGroup) async {

    await DBHelper().updateMessageRead(TableName.newMessageRecord,userId,isGroup);
  }

  ///打开相册
  void openPicture() async {
    List<AssetEntity>? pickAssets = await AssetPicker.pickAssets(Get.context!,
        pickerConfig:
            const AssetPickerConfig(textDelegate: AssetPickerTextDelegate()));

    ///textDelegate 中文配置
    if (pickAssets == null || pickAssets.isEmpty) {
      return;
    }
    for (int i = 0; i < pickAssets.length; i++) {
      AssetEntity c = pickAssets[i];

      ///图片
      if (c.type == AssetType.image) {
        dealImage(c);
      }

      ///视频
      if (c.type == AssetType.video) {
        dealVideo(c);
      }
    }
  }

  ///发送图片
  void sendImage(String imageUrl) async {
    await send(imageUrl, 1);
    update();
  }

  ///点击了哪个item
  clickButtonItem(ItemTitle e) {
    switch (e.title) {
      case R.systemAlbum:
        openPicture();
        break;
      case R.systemShot:

        ///打开拍摄
        openShot();
        break;
      case R.systemFile:///打开文件
        openFile();
        break;

      default:
    }
  }

  ///选择的是本地图片
  void dealImage(AssetEntity c) async {
    File? file = await c.file;
    //发送到后台保存
    var imageUrl = await HttpDio.getInstance().upload(ApiUrl.fileUpLoad, file!);
    if (imageUrl == null) {
      Get.snackbar("提示", "上传图片失败");
      return;
    }
    sendImage(imageUrl);
  }

  ///本地视频
  void dealVideo(AssetEntity c) async {
    ///生成视频封面
    File videoCover = await getVideoThumbnail(c);

    ///保存封面
    var coverUrl =
        await HttpDio.getInstance().upload(ApiUrl.fileUpLoad, videoCover);
    if (coverUrl == null) {
      Get.snackbar("提示", "保存图片失败");
      return;
    }

    ///获得视频文件
    File? file = await c.file;

    ///保存视频
    var videoUrl =
        await HttpDio.getInstance().upload(ApiUrl.fileUploadVideo, file!);
    await send(videoUrl, 2, coverUrl: coverUrl);
    update();
  }

  //生成视频封面
  Future<File> getVideoThumbnail(AssetEntity videoAsset) async {
    File? file1 = await videoAsset.file;
    final fileName = await VideoThumbnail.thumbnailFile(
      video: file1!.path,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG,
      maxHeight:
          64, // specify the height of the thumbnail, let the width auto-scaled to keep the source aspect ratio
      quality: 75,
    );

    File file = File(fileName!);
    return file;
  }

  ///点击拍摄
  void openShot() async {
    final AssetEntity? c = await wcp.CameraPicker.pickFromCamera(Get.context!,
        pickerConfig: const wcp.CameraPickerConfig(enableRecording: true));
    if (c == null) {
      return;
    }

    ///图片
    if (c.type == AssetType.image) {
      dealImage(c);
    }

    ///视频
    if (c.type == AssetType.video) {
      dealVideo(c);
    }
  }
  ///打开文件以及处理文件逻辑
  void openFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if(result==null){
      return;
    }
    List<PlatformFile> files=result.files;
    for(int i=0;i<files.length;i++){
      PlatformFile platformFile= files[i];
      File myFile = File(platformFile.path!);
      String fileUrl= await sendFile(myFile);
      print(fileUrl);
      await send(fileUrl,3);
      update();
    }




  }
  ///保存图片 返回图片url
  Future<String> sendFile(File myFile) async {
    //发送到后台保存
    var fileUrl =
    await HttpDio.getInstance().upload(ApiUrl.fileUpLoad, myFile);
    if (fileUrl == null) {
      Get.snackbar("提示", "上传图片失败");
      return"";
    }

    return fileUrl;
  }
  ///插入消息表记录
  Future<void> insertNewMessageRecord(MessageDao e) async{
    FriendVo06 rec=friendVo06.value;
    /// 组装id
    NewMessageRecord bean=NewMessageRecord(
        userId: rec.userId,
        portrait:rec.portrait,
        nickName: rec.nickName,
        content: e.content,
        createTime: e.createTime,
        isGroupMessage: e.isGroupMessage,
        isRead: 1
    );
    await DBHelper().insertNewMessageRecord(bean);
  }
}
