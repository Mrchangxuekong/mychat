import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_list_view/flutter_list_view.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';
import 'package:zhongdingapp/entity/chat/friend_vo06.dart';
import 'package:zhongdingapp/iconfont/icon_font.dart';
import 'package:zhongdingapp/page/chat/chat_single/chat_single_logic.dart';
import 'package:zhongdingapp/page/home/message/message/message_logic.dart';
import 'package:zhongdingapp/sqllite/bean/message_dao.dart';
import 'package:zhongdingapp/routerHelper.dart';
import 'package:zhongdingapp/widget/msg/msg_text.dart';
import 'package:zhongdingapp/widget/msg/msg_time.dart';

class ChatSinglePage extends StatelessWidget {
  const ChatSinglePage({super.key});

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ChatSingleLogic());
    logic.onInit();
    return GetBuilder<ChatSingleLogic>(
      init: logic,
      builder: (c) => Scaffold(
        backgroundColor: ThemeColor.gray20,
        appBar: getAppbar(logic.friendVo06.value),
        body: Column(
          children: [
            Expanded(
              child: InkWell(
                onTap: () {
                  //取消键盘
                  FocusScope.of(context).requestFocus(FocusNode());
                  logic.addMore.value = false;
                  logic.showBottomPage.value = false;
                  logic.showKeyboard.value = false;
                  logic.update();
                },
                child: Container(
                  color: ThemeColor.appbarBackground,
                  child: FlutterListView(
                    reverse: true,
                    delegate: FlutterListViewDelegate((build, index) {
                      return getMsgListItem(build, index, c);
                    },
                        firstItemAlign: FirstItemAlign.end,
                        childCount: c.msgList.length),
                  ),
                ),
              ),
            ),
            getBottom(c, context),
            /*  getItem(, () {
              showEmojiPicker(context);
            }) */
            //showEmojiPicker(context),
          ],
        ),
      ),
      assignId: true,
    );
  }

  getAppbar(FriendVo06 item) {
    return AppBar(
      backgroundColor: ThemeColor.appbarBackground,
      title: Text(item.nickName!),
      centerTitle: true,
      leading: InkWell(
          child: const Icon(
            MyIcons.leftMain,
            size: 30,
          ),
          onTap: () {
            MessageLogic logic = Get.find<MessageLogic>();
            logic.onInit();
            Get.back();
          }),
      actions: [
        InkWell(
          child: const Padding(
            padding: EdgeInsets.only(right: 15),
            child: Icon(
              MyIcons.more,
              size: 30,
            ),
          ),
          onTap: () => Get.toNamed(RouterHelper.personalchat),
        ),
      ],
    );
  }

  getBottom(ChatSingleLogic chatC, BuildContext context) {
    print("dddddddddddddddddddddd");
    print("${ chatC.inputSend.value.text.isEmpty}");
    return SafeArea(
        child: Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 10),
          child: Row(
            children: [
              getItem(MyIcons.keyboard, () {}),
              Expanded(
                  child: Container(
                color: Colors.white,
                child: TextField(
                  autofocus: false,
                  controller: chatC.inputSend.value,
                  onChanged: (v) {
                    chatC.inputSend.value.text = v;
                    chatC.update();
                  },
                  onTap: () {
                    chatC.showBottomPage.value = false;
                    chatC.showKeyboard.value = true;
                    chatC.update();
                  },
                  maxLines: 8,
                  minLines: 1,
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 6),
                    border: OutlineInputBorder(borderSide: BorderSide.none),
                    isCollapsed: true,
                  ),
                ),
              )),

              ///微笑按钮
              getItem(MyIcons.smilingFace, () {
                showEmojiPicker(context, chatC);
              }),

              ///第一种情况，输入框为空，键盘没弹出来默认的时候
              (chatC.inputSend.value.text.isEmpty)
                  ? getItem(Icons.add_circle_outline, () {
                    if(chatC.inputSend.value.text.isEmpty){
                      FocusScope.of(context).requestFocus(FocusNode());
                      chatC.showBottomPage.value=!chatC.showBottomPage.value;
                      chatC.update();
                     return;
                    }
                      chatC.changAddMore();
                    })
                  : InkWell(
                      onTap: () {
                        /// 发送消息
                        chatC.sendMessage();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: IconFont(
                          IconNames.fujin_1,
                          size: 25,
                        ),
                      ),
                    ),
            ],
          ),
        ),
        if (chatC.showBottomPage.value) getMore(chatC),
      ],
    ));
  }

  getMore(ChatSingleLogic chatC) {
    return SizedBox(
      height: 200,
      child: Stack(
        children: [
          Container(
              padding: const EdgeInsets.only(bottom: 20, left: 20),
              child: PageView(
                onPageChanged: (currIndex) {
                  chatC.pageViewIndex.value = currIndex;
                },
                children: [...getPageViewItem(chatC)],
              )),
          //底部滑动栏
          Positioned(
              bottom: 15,
              left: 0,
              right: 0,
              child: Wrap(
                alignment: WrapAlignment.center,
                spacing: 8,
                children: List.generate(
                    2,
                    (index) => Container(
                          width: 8,
                          height: 8,
                          decoration: BoxDecoration(
                              color: index == chatC.pageViewIndex.value
                                  ? ThemeColor.gray100
                                  : ThemeColor.gray25,
                              shape: BoxShape.circle),
                        )),
              ))
        ],
      ),
    );
  }

  getWarp(List<ItemTitle> list, ChatSingleLogic chatC) {
    return Wrap(
      spacing: 15,
      runSpacing: 15,
      children: [
        ...list.map((e) => InkWell(
              onTap: () {
                chatC.clickButtonItem(e);
              },
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Column(
                    children: [
                      Icon(
                        e.icon,
                        size: 40,
                        color: Colors.black.withOpacity(0.6),
                      ),
                      Text(
                        e.title,
                        style: const TextStyle(fontSize: 8),
                      )
                    ],
                  )),
            )),
      ],
    );
  }

  getPageViewItem(ChatSingleLogic chatC) {
    List<Wrap> list = [];
    for (int i = 0; i < chatC.addMoreList.length; i += 8) {
      List<ItemTitle> sublist = chatC.addMoreList.sublist(i,
          i + 8 > chatC.addMoreList.length ? chatC.addMoreList.length : i + 8);
      list.add(getWarp(sublist, chatC));
    }
    return list;
  }

  Widget getMsgListItem(
      BuildContext context, int index, ChatSingleLogic chatC) {
    // ignore: invalid_use_of_protected_member
    MessageDao item = chatC.msgList.value[index];
    // ignore: invalid_use_of_protected_member
    if (chatC.msgList.value.isEmpty) {
      return const Text("");
    } else if (index == 0) {
      return Column(
        children: [
          MsgTime(
            msgTime: item.createTime!,
          ),
          MsgText(
              type: item.type!,
              self: item.self == 1,
              nikeName: item.sendNickName!,
              portrait: item.sendPortrait!,
              coverUrl: item.coverUrl,
              content: item.content!)
        ],
      );
      // ignore: invalid_use_of_protected_member
    } else if (index != chatC.msgList.value.length - 1) {
      ///如果不是最后一条数据
      DateTime time1 = item.createTime!;
      // ignore: invalid_use_of_protected_member
      DateTime time2 = chatC.msgList.value[index + 1].createTime!;
      var inHours = (time2.difference(time1).inHours).abs();
      if (inHours >= 1) {
        return Column(
          children: [
            MsgTime(
              msgTime: item.createTime!,
            ),
            MsgText(
                type: item.type!,
                self: item.self == 1,
                portrait: item.sendPortrait!,
                nikeName: item.sendNickName!,
                coverUrl: item.coverUrl,
                content: item.content!)
          ],
        );
      } else {
        return MsgText(
            type: item.type!,
            self: item.self == 1,
            nikeName: item.sendNickName!,
            coverUrl: item.coverUrl,
            portrait: item.sendPortrait!,
            content: item.content!);
      }
    } else {
      return MsgText(
          type: item.type!,
          self: item.self == 1,
          nikeName: item.sendNickName!,
          coverUrl: item.coverUrl,
          portrait: item.sendPortrait!,
          content: item.content!);
    }
  }

  getItem(IconData item, VoidCallback fn) {
    return InkWell(
      key: ValueKey(item),
      onTap: fn,
      child: Container(
        padding: const EdgeInsets.all(6),
        child: Icon(item, size: 27),
      ),
    );
  }

  ///是发送按钮还是下拉按钮
  getSendOrAdd(ChatSingleLogic chatC) {
    if (!chatC.addMore.value && !chatC.showKeyboard.value) {
      ///默认显示add按钮
      return getItem(Icons.add_circle_outline, () {
        chatC.showBottomPage.value = !chatC.showBottomPage.value;
        chatC.update();
      });
    }
    if (chatC.showKeyboard.value) {
      return InkWell(
        onTap: () {
          ///发送消息
          chatC.sendMessage();
        },
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: IconFont(
            IconNames.fujin_1,
            size: 25,
          ),
        ),
      );
    }
  }
}

showEmojiPicker(BuildContext context, ChatSingleLogic chatC) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (BuildContext context) {
      return GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        behavior: HitTestBehavior.opaque, //确保触发点击事件
        child: DraggableScrollableSheet(
          initialChildSize: 0.4,
          maxChildSize: 0.6,
          minChildSize: 0.3,
          builder: (
            BuildContext context,
            ScrollController scrollController,
          ) {
            return ConstrainedBox(
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * 0.4),
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16.0),
                    topRight: Radius.circular(16.0),
                  ),
                ),
                child: EmojiPicker(
                  textEditingController: chatC.inputSend.value,
                  onEmojiSelected: (category, emoji) {
                    // 处理选择的表情
                  },
                  config: const Config(
                    columns: 7,
                    verticalSpacing: 0,
                    horizontalSpacing: 0,
                    initCategory: Category.RECENT,
                    bgColor: Color(0xFFF2F2F2),
                    indicatorColor: Colors.blue,
                    iconColor: Colors.grey,
                    iconColorSelected: Colors.blue,
                    backspaceColor: Colors.blue,
                    skinToneDialogBgColor: Colors.white,
                    skinToneIndicatorColor: Colors.grey,
                    enableSkinTones: true,
                    recentsLimit: 28,
                    tabIndicatorAnimDuration: kTabScrollDuration,
                    categoryIcons: CategoryIcons(),
                    buttonMode: ButtonMode.MATERIAL,
                  ),
                ),
              ),
            );
          },
        ),
      );
    },
  );
}
