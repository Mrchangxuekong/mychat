import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:zhongdingapp/color/local_img_path.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/routerHelper.dart';
import 'package:zhongdingapp/util/mqtt_utils.dart';

class InitialImg extends StatefulWidget {
  const InitialImg({Key? key}) : super(key: key);

  @override
  _InitialImgState createState() => _InitialImgState();
}

class _InitialImgState extends State<InitialImg> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width, // 屏幕宽度
      height: MediaQuery.of(context).size.height, // 屏幕高度
      child: Image.asset(
        LocalImgPath.initBackground,
        fit: BoxFit.cover,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    Duration duration = const Duration(seconds: 3);
    Timer(duration, () async {
       var cc= await Hive.openBox(constHiveBox);
       print(R.token);
        var userId= await cc.get(R.userId);
        if (userId==null){
          Get.toNamed(RouterHelper.initIndex);
        }else{
          Get.toNamed(RouterHelper.home);
        }

    });
  }

}
