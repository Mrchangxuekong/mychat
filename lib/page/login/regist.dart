// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/color/themeText.dart';
import 'package:zhongdingapp/controller/registController.dart';
import 'package:zhongdingapp/entity/vo/auth_vo01.dart';
import 'package:zhongdingapp/routerHelper.dart';

class Regist extends StatefulWidget {
  const Regist({Key? key}) : super(key: key);

  @override
  _RegistState createState() => _RegistState();
}

class _RegistState extends State<Regist> {
  TextEditingController inCode = TextEditingController();
  TextEditingController inPhone = TextEditingController();
  TextEditingController inPassword = TextEditingController();
  TextEditingController inNikeName = TextEditingController();
  RegistController gexReg = Get.find<RegistController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeColor.gray,
        centerTitle: true,
        title: const Text("注册"),
      ),
      body: Container(
          margin: const EdgeInsets.all(ThemeText.double20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _getListWidget(),
            ),
          )),
    );
  }

  List<Widget> _getListWidget() {
    List<Widget> list = [];
    Center title = const Center(
      child: Text(
        "手机号注册",
        style: TextStyle(
          fontSize: ThemeText.double15,
          fontWeight: FontWeight.bold,
          letterSpacing: ThemeText.double2,
        ),
      ),
    );
    list.add(title);
    _addphone(list);
    _addCode(list);
    _addPassword(list);
    _addNickName(list);
    OutlinedButton goLogin = OutlinedButton(
      onPressed: () {
        Get.toNamed(RouterHelper.login);
      },
      style: ButtonStyle(
        side: MaterialStateProperty.all(BorderSide.none),
      ),
      child: const Text("已注册,去登录"),
    );
    list.add(goLogin);
    _addRegistButton(list);
    _addAgreement(list);
    return list;
  }

  var c1 = RegistController();

  void _addAgreement(List<Widget> list) {
    Row item = Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      GetX<RegistController>(
          init: c1,
          builder: (c) {
            return Transform.scale(
              scale: ThemeText.double1,
              child: Checkbox(
                  value: c.getCheckBtn(),
                  onChanged: (value) {
                    c.setCheckBtn(value!);
                  }),
            );
          }),
      const Text(
        "已阅读并同意",
        style: TextStyle(fontSize: ThemeText.double8),
      ),
      InkWell(
        child: const Text("<<隐私及服务协议>>",
            style:
                TextStyle(fontSize: ThemeText.double8, color: ThemeColor.blue)),
        onTap: () {
          goToAgreement();
        },
      )
    ]);
    list.add(item);
  }

  void _addRegistButton(List<Widget> list) {
    Container center = Container(
      margin: const EdgeInsets.only(top: 50),
      child: Center(
        child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(ThemeColor.mainGreen)),
          onPressed: () {
            registerBtn();
          },
          child: const Text(
            "注册",
            style: TextStyle(
                fontSize: 15, letterSpacing: 20, color: ThemeColor.whiteText),
          ),
        ),
      ),
    );
    list.add(center);
  }

  List<Widget> _addNickName(List<Widget> list) {
    Container phone = Container(
      margin: const EdgeInsets.only(top: 20),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: Container(
                margin: const EdgeInsets.only(left: 20),
                height: ThemeText.double25,
                child: const Text("昵称"),
              )),
          Expanded(
              flex: 6,
              child: SizedBox(
                height: 40,
                child: TextField(
                    controller: inNikeName,
                    decoration: (const InputDecoration(
                      hintStyle: TextStyle(height: 1),
                      hintText: "请输入昵称",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    ))),
              )),
        ],
      ),
    );
    list.add(phone);
    return list;
  }

  List<Widget> _addPassword(List<Widget> list) {
    Container phone = Container(
      margin: const EdgeInsets.only(top: ThemeText.double30),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
              flex: ThemeText.int2,
              child: Container(
                margin: const EdgeInsets.only(left: ThemeText.double20),
                height: ThemeText.double25,
                child: const Text("密码"),
              )),
          Expanded(
              flex: ThemeText.int6,
              child: SizedBox(
                height: ThemeText.double40,
                child: TextField(
                    controller: inPassword,
                    obscureText: true,
                    enableInteractiveSelection: false,
                    decoration: (const InputDecoration(
                      hintStyle: TextStyle(height: 1),
                      hintText: "请输入密码",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    ))),
              )),
        ],
      ),
    );
    list.add(phone);
    return list;
  }

  List<Widget> _addCode(List<Widget> list) {
    Container phone = Container(
      margin: const EdgeInsets.only(top: ThemeText.double30),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
              flex: ThemeText.int2,
              child: Container(
                margin: const EdgeInsets.only(left: ThemeText.double20),
                height: ThemeText.double25,
                child: const Text("验证码"),
              )),
          Expanded(
              flex: 4,
              child: SizedBox(
                height: ThemeText.double40,
                child: TextField(
                  controller: inCode,
                  decoration: (const InputDecoration(
                    hintStyle: TextStyle(height: 1),
                    hintText: "请输入验证码",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                  )),
                ),
              )),
          Expanded(
              flex: 2,
              child: SizedBox(
                height: ThemeText.double21,
                child: ElevatedButton(
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all(
                          const EdgeInsets.symmetric(
                              horizontal: 1, vertical: 1)),
                      backgroundColor:
                          MaterialStateProperty.all(ThemeColor.mainGreen)),
                  onPressed: () {
                    getCode();
                  },
                  child: const Text(
                    maxLines: 1,
                    // textAlign: TextAlign.center,
                    "获取验证码",
                    style: TextStyle(fontSize: 14, color: ThemeColor.whiteText),
                  ),
                ),
              )),
        ],
      ),
    );
    list.add(phone);
    return list;
    //后台接口获取验证码
  }

  List<Widget> _addphone(List<Widget> list) {
    Container phone = Container(
      margin: const EdgeInsets.only(
        top: ThemeText.double30,
      ),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
              flex: ThemeText.int2,
              child: Container(
                margin: const EdgeInsets.only(
                  left: ThemeText.double20,
                ),
                height: ThemeText.double25,
                child: const Text("手机号"),
              )),
          Expanded(
              flex: ThemeText.int6,
              child: SizedBox(
                height: ThemeText.double40,
                child: TextField(
                    controller: inPhone,
                    keyboardType: TextInputType.number,
                    decoration: (const InputDecoration(
                      hintStyle: TextStyle(height: 1),
                      hintText: "请输入手机号",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    ))),
              )),
        ],
      ),
    );
    list.add(phone);
    return list;
  }

  @override
  void dispose() {
    inCode.dispose();
    super.dispose();
  }

  //---------------------------上方写视图组件，以下写事件方法----------------------------------------
  //获取验证码
  void getCode() {
    String phone = inPhone.text;
    if (phone.isEmpty) {
      Get.snackbar("提示", "手机号输入有误");
      return;
    }
    gexReg.getCode(phone);
  }

  ///注册
  void registerBtn() {
    AuthVo01 reg = AuthVo01()
      ..phone = inPhone.text
      ..password = inPassword.text
      ..nickName = inNikeName.text
      ..code = inCode.text;
    gexReg.registerBtn(reg);
  }

  ///用户隐私协议
  void goToAgreement() {
    gexReg.goToAgreement();
  }
}
