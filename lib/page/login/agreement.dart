import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/controller/registController.dart';

class Agreement extends StatefulWidget {
  Agreement({super.key});

  RegistController gexReg = Get.find<RegistController>();

  @override
  _State createState() => _State();
}

class _State extends State<Agreement> {
  WebViewController? webViewController;

  @override
  void initState() {
    initWebViewWidget();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("用户隐私和协议"),
      ),
      body: WebViewWidget(controller: webViewController!),
    );
  }

  void initWebViewWidget() {
    webViewController = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith("https://www.youtube.com/")) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(Get.arguments[R.keyAgreement]));
  }
}
