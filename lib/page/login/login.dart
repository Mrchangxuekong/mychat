import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/color/themeText.dart';
import 'package:zhongdingapp/controller/loginController.dart';
import 'package:zhongdingapp/controller/registController.dart';
import 'package:zhongdingapp/entity/vo/auth_vo02.dart';
import 'package:zhongdingapp/entity/vo/auth_vo03.dart';
import 'package:zhongdingapp/routerHelper.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  LoginController gexC = Get.find<LoginController>();
  RegistController registController = Get.find<RegistController>();
  TextEditingController inPhone = TextEditingController();
  TextEditingController inPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeColor.gray,
        leading: InkWell(
          onTap: () {
            Get.toNamed(RouterHelper.registe);
          },
          child: const Icon(MyIcons.left),
        ),
        centerTitle: true,
        title: const Text("手机号登录"),
      ),
      body: Container(
          margin: const EdgeInsets.all(ThemeText.double20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _getListWidget(),
            ),
          )),
    );
  }

  List<Widget> _getListWidget() {
    List<Widget> list = [];
    Center title = const Center(
      child: Text(
        "手机号登录",
        style: TextStyle(
          fontSize: ThemeText.double15,
          fontWeight: FontWeight.bold,
          letterSpacing: ThemeText.double2,
        ),
      ),
    );
    list.add(title);
    _addPhone(list);
    _addPassword(list);
    _addOutButton(list);
    OutlinedButton forgot = OutlinedButton(
      onPressed: () {},
      style: ButtonStyle(
        side: MaterialStateProperty.all(BorderSide.none),
      ),
      child: const Text("忘记密码"),
    );
    list.add(forgot);
    _addLoginButton(list);
    _addAgreement(list);
    return list;
  }

  ///密码输入框
  List<Widget> _addPassword(List<Widget> list) {
    Container phone = Container(
      margin: const EdgeInsets.only(top: ThemeText.double30),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
              flex: ThemeText.int2,
              child: Container(
                margin: const EdgeInsets.only(left: ThemeText.double20),
                height: ThemeText.double25,
                child: Obx(() => Text(gexC.phoneType.value)),
              )),
          Expanded(
            flex: ThemeText.int6,
            child: SizedBox(
              height: ThemeText.double40,
              child: Obx(
                () => TextField(
                  controller: inPassword,
                  obscureText: gexC.loginType.value,
                  enableInteractiveSelection: false,
                  decoration: (InputDecoration(
                    hintStyle: const TextStyle(height: 1),
                    hintText: gexC.hintType.value,
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                  )),
                ),
              ),
            ),
          ),
        ],
      ),
    );
    list.add(phone);
    return list;
  }

  var c1 = LoginController();

  //登录操作
  void _addLoginButton(List<Widget> list) {
    Container center = Container(
      margin: const EdgeInsets.only(top: 50),
      child: Center(
        child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(ThemeColor.mainGreen)),
          onPressed: getCodeFn,
          child: const Text(
            "登录",
            style: TextStyle(
                fontSize: 15, letterSpacing: 20, color: ThemeColor.whiteText),
          ),
        ),
      ),
    );
    list.add(center);
  }

  void _addAgreement(List<Widget> list) {
    Row item = Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      GetX<LoginController>(
          init: c1,
          builder: (c) {
            return Transform.scale(
              scale: ThemeText.double1,
              child: Checkbox(
                  value: c.getCheckBtn(),
                  onChanged: (value) {
                    c.setCheckBtn(value!);
                  }),
            );
          }),
      const Text(
        "已阅读并同意",
        style: TextStyle(fontSize: ThemeText.double8),
      ),
      InkWell(
        child: const Text("<<隐私及服务协议>>",
            style:
                TextStyle(fontSize: ThemeText.double8, color: ThemeColor.blue)),
        onTap: () {
          registController.goToAgreement();
        },
      )
    ]);
    list.add(item);
  }

  List<Widget> _addPhone(List<Widget> list) {
    Container phone = Container(
      margin: const EdgeInsets.only(top: ThemeText.double30),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
              flex: ThemeText.int2,
              child: Container(
                margin: const EdgeInsets.only(left: ThemeText.double20),
                height: ThemeText.double25,
                child: const Text("手机号"),
              )),
          Expanded(
              flex: ThemeText.int6,
              child: SizedBox(
                height: ThemeText.double40,
                child: TextField(
                  autofocus: false, //控制是加载时自动获取焦点
                  showCursor: true, //控制显示光标
                  controller: inPhone,
                  keyboardType: TextInputType.number,
                  decoration: (const InputDecoration(
                    hintStyle: TextStyle(height: 1),
                    hintText: "请输入手机号",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                  )),
                ),
              )),
        ],
      ),
    );
    list.add(phone);
    return list;
  }

  List<Widget> _addOutButton(List<Widget> list) {
    OutlinedButton goLogin = OutlinedButton(
      onPressed: () {
        gexC.updateLoginType();
      },
      style: ButtonStyle(
        side: MaterialStateProperty.all(BorderSide.none),
      ),
      child: Obx(() => Text(gexC.longinTip.value)),
    );
    list.add(goLogin);
    return list;
  }

  //-------------------以下写后台接口--------------------------------
  ///登录操作
  void getCodeFn() {
    if (gexC.loginType.value) {
      //密码登录
      AuthVo02 authVo02 =
          AuthVo02(phone: inPhone.text, password: inPassword.text, cid: "");
      gexC.login(authVo02);
    } else {
      //验证码登录
      AuthVo03 authVo03 = AuthVo03();
      authVo03.phone = inPhone.text;
      authVo03.code = inPassword.text;
      gexC.loginByCode(authVo03);
    }
  }
}
