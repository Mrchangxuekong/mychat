import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:zhongdingapp/util/curr_device.dart';
import 'package:zhongdingapp/util/middle_ware.dart';

import '../my_app_state.dart';
import 'r.dart';

///页面启动前的初始化配置
class AppContext {
  static Future init() async {
    Hive.initFlutter();
    // 初始化永久对象
    MyAppState.initializePermanentObject();
    await dotenv.load();
    GetStorage.init();
    Get.put(MiddleWare());
    Hive.openBox(constHiveBox);
    CurrDevice.getDevice();//获取设备信息
    return Future.value();
  }
}
