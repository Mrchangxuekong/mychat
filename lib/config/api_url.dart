class ApiUrl{
  ///密码登录
  static const String login="auth/login";
  ///验证码登录
  static const String loginByCode="auth/loginByCode";
  ///注册
  static const String register="auth/register";
  ///获取验证码
  static const String sendCode="auth/sendCode";
  ///协议
  static const String getAgreement="common/getAgreement";
  ///搜索好友
  static const String postFriendFindFriend="friend/findFriend";
  ///申请添加好友
  static const String applyAdd="apply/add";
  ///申请添加好友记录
  static const String getApplyList="apply/list";
  ///同意申请
  static const String agreeAddFriend="apply/agree";
  ///好友列表
  static const String getFriendFriendList="friend/friendList";
  ///文件上传
  static const String fileUpLoad="file/upload";

  ///生成视频封面
  static const String fileUploadVideo="file/uploadVideo";
  ///上传视频
  static const String fileUploadAudio="file/uploadAudio";
  ///创建群聊
  static const String groupCreateGroup="group/createGroup";






}