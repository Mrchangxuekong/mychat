import 'package:flutter_dotenv/flutter_dotenv.dart';

String constApiBaseUrl = dotenv.get('API_BASE_URL');
String mqttAddress = dotenv.get('MQTT_ADDRESS');
String constHiveBox = dotenv.get('HIVE_BOX');

class R {
  static const String token = "token";
//参数key
  static const String keyAgreement = "paramAgreement";
  static const String apiVersion = "1.0.0";
//设备建
  static const String device = "device";

  ///userid
  static const String userId = "userId";

  ///来源类型
  static const String source = "source";
  static const String code = "code";
  static const String msg = "msg";
  static const String data = "data";
  static const String rows = "rows";
/* static const String mqttUrl="192.168.1.51"; */
  static const String mqttPort = "1883";

  ///系统字符串
  ///"相册"
  static const String systemAlbum = "相册";

  ///"拍摄"
  static const String systemShot = "拍摄";

  ///"视频通话"
  static const String systemVideoCall = "视频通话";

  ///"相册"
  static const String systemVoiceCall = "相册";

  ///"位置"
  static const String systemLocation = "位置";

  ///"名片"

  static const String systemInfoCard = "名片";

  ///"文件"
static const String systemFile="文件";
  ///"mqtt 单聊前缀 "
static const String mqttSingle="home/mqttSingle/";
///群聊
static const String mqttGroup="home/mqttGroup/";
///系统
static const String mqttSystem="home/mqttSystem/";
  ///"mqtt 单聊后缀 "
static const String mqttSurSingle="/single";
static const String chatCreateGroup="chat/createGroup/";
///群聊前缀
static const String chatGroup="chat/group/";


}

