import 'package:get/get_navigation/get_navigation.dart';
import 'package:zhongdingapp/page/chat/chat_single/chat_single_binding.dart';
import 'package:zhongdingapp/page/chat/chat_single/chat_single_view.dart';
import 'package:zhongdingapp/page/chat/group_info_page.dart';
import 'package:zhongdingapp/page/home/find/find_page.dart';
import 'package:zhongdingapp/page/home/find/timeline.dart';
import 'package:zhongdingapp/page/home/home.dart';
import 'package:zhongdingapp/page/home/me/big_avatar_page.dart';
import 'package:zhongdingapp/page/home/me/me_info_page.dart';
import 'package:zhongdingapp/page/home/me/me_page.dart';
import 'package:zhongdingapp/page/home/me/me_page/me_page_view.dart';
import 'package:zhongdingapp/page/home/me/qr_my_code_page.dart';
import 'package:zhongdingapp/page/home/me/set_name_page.dart';
import 'package:zhongdingapp/page/home/message/Personal_chat/view.dart';
import 'package:zhongdingapp/page/home/message/add_friend_by_search.dart';
import 'package:zhongdingapp/page/home/message/add_friend_list_page.dart';
import 'package:zhongdingapp/page/home/message/apply_add_friends_page.dart';
import 'package:zhongdingapp/page/home/message/information/view.dart';
import 'package:zhongdingapp/page/home/message/message/message_binding.dart';
import 'package:zhongdingapp/page/home/message/message/message_view.dart';
import 'package:zhongdingapp/page/home/phonebook/add_friend.dart';
import 'package:zhongdingapp/page/home/phonebook/communication_page.dart';
import 'package:zhongdingapp/page/home/phonebook/search_user_phone.dart';
import 'package:zhongdingapp/page/home/phonebook/select_contacts.dart';
import 'package:zhongdingapp/page/home/phonebook/select_contacts/select_contacts_binding.dart';
import 'package:zhongdingapp/page/home/phonebook/select_contacts/select_contacts_view.dart';
import 'package:zhongdingapp/page/home/searchpage.dart';
import 'package:zhongdingapp/page/index.dart';
import 'package:zhongdingapp/page/login/agreement.dart';
import 'package:zhongdingapp/page/login/initial_img.dart';
import 'package:zhongdingapp/page/login/login.dart';
import 'package:zhongdingapp/page/login/regist.dart';
import 'package:zhongdingapp/util/middle_ware.dart';
import 'package:zhongdingapp/page/home/me/setup.dart';
import 'package:zhongdingapp/widget/check_image/check_image_binding.dart';
import 'package:zhongdingapp/widget/check_image/check_image_view.dart';

import 'page/home/message/message/message_logic.dart';

class RouterHelper {
  static const String login = '/login';
  static const String initial = '/';
  static const String initIndex = '/index';
  static const String registe = '/registe';
  static const String agreement = '/agreement';
  static const String home = '/home';
  static const String search = '/search';
  static String index = "/index";
  static String message = "/message";
  static String communication = "/communication";
  static String find = "/find";
  static String mePage = "/mePage";
  static String addFriend = "/addFriend";
  static String selectContacts = "/selectContacts";
  static String searchUserPhone = "/searchUserPhone";
  static String qrMyCode = "/qrMyCode";
  static String setNamePage = "/SetNamePage";
  static String bigAvatarPage = "/bigAvatarPage";
  static String addFriendBySearch = "/addFriendBySearch";
  ///查看图片
  static String checkImage = "/checkImage";

  ///添加朋友，已是好友和不是好友的页面
  static String applyAddFriendPage = "/applyAddFriendPage";

  ///申请添加朋友
  static String chatSinglePage = "/chatSinglePage";

  ///单聊
  static String chatGroupPage = "/chatGroupPage";

  ///群聊
  static String groupInfoPage = "/groupInfoPage";

  ///群聊详细信息
  static String meInfoPage = "/meInfoPage";

  ///群聊详细信息
  static String addFriendListPage = "/addFriendListPage";

  ///审核添加朋友好友列表
  ///  ///我的设置
  static String setup = "/setup";
  static String circleoffriends = "/circleoffriends";

  ///个人聊天详情信息
  static String personalchat = "/personalchat";

  ///个人资料设置
  static String information = "/information";
  ///添加群聊
  static String selectContactsPage = "/selectContactsPage";

  static String getInitial() => initial;
  static String getLogin() => login;
  static String getRegiste() => registe;

  static final List<GetPage> routes = [
    GetPage(name: registe, page: () => const Regist()),
    GetPage(name: agreement, page: () => Agreement()),
    GetPage(name: home, page: () => const Home(),bindings: [MessageBinding()]),
    GetPage(name: search, page: () => const Searchpage()),
    GetPage(name: login, page: () => const Login()),
    GetPage(
        name: initial,
        page: () => const InitialImg(),
        middlewares: [MiddleWare()]),
    GetPage(name: initIndex, page: () => const Index()),
    GetPage(name: message, page: () => const MessagePage()),
    GetPage(name: communication, page: () => const CommunicationPage()),
    GetPage(name: find, page: () => const FindPage()),
    GetPage(name: mePage, page: () => const MePagePage()),
    GetPage(name: addFriend, page: () => const AddFriend()),
    GetPage(name: selectContacts, page: () => const SelectContacts()),
    GetPage(name: searchUserPhone, page: () => const SearchUserPhone()),
    GetPage(name: qrMyCode, page: () => const QrMyCodePage()),
    GetPage(name: setNamePage, page: () => const SetNamePage()),
    GetPage(name: bigAvatarPage, page: () => const BigAvatarPage()),
    GetPage(name: addFriendBySearch, page: () => const AddFriendBySearch()),
    GetPage(name: applyAddFriendPage, page: () => const ApplyAddFriendPage()),
    GetPage(
        name: chatSinglePage,
        page: () => const ChatSinglePage(),
        binding: ChatSingleBinding()),
    GetPage(name: groupInfoPage, page: () => const GroupInfoPage()),
    GetPage(name: meInfoPage, page: () => const MeInfoPage()),
    GetPage(name: addFriendListPage, page: () => const AddFriendListPage()),
    GetPage(name: setup, page: () => SetupPage()),
    GetPage(name: circleoffriends, page: () => const TimeLinePage()),
    GetPage(name: personalchat, page: () => const PersonalChatPage()),
    GetPage(name: information, page: () => const InformationPage()),
    GetPage(name: checkImage, page: () => const CheckImagePage(),binding: CheckImageBinding()),
    GetPage(name: selectContactsPage, page: () => const SelectContactsPage(),binding: SelectContactsBinding()),
  ];
}
