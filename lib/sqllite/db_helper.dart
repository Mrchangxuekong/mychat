import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:zhongdingapp/sqllite/bean/new_message_record.dart';
import 'package:zhongdingapp/sqllite/table_name.dart';

class DBHelper {
  static Database? _database;
  static  DBHelper? _dbHelper;
  static const  String tableMessages="messages"; //表名
  DBHelper._createInstance();

  factory DBHelper() {
    _dbHelper ??= DBHelper._createInstance();
    return _dbHelper!;
  }

  Future<Database> get database async {
    if (_database != null) {
      return _database!;
    }
    _database = await _initDB();
    return _database!;
  }

  Future<Database> _initDB() async {
    String path = join(await getDatabasesPath(), 'zDing.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _createDB,
    );
  }

  void _createDB(Database db, int version) async {
    ///聊天记录表
    await db.execute(
        '''CREATE TABLE messages (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        senderId TEXT NOT NULL,
        sendNickName TEXT NOT NULL,
        sendPortrait TEXT NOT NULL,
        receiverId TEXT NOT NULL,
        content TEXT,
        coverUrl TEXT,
        type INTEGER,
        portrait TEXT,
        nickName TEXT,
        createTime INTEGER,
        isRead INTEGER,
        self INTEGER,
        isGroupMessage INTEGER
    )''');
    ///最新记录表，主要是记录消息页面展示数据的列表
    await db.execute(
        '''CREATE TABLE new_message_record (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        userId TEXT NOT NULL,
        portrait TEXT NOT NULL,
        nickName TEXT NOT NULL,
        content TEXT,
        isRead INTEGER,
        createTime INTEGER,
        isGroupMessage INTEGER
    )''');
  }

  Future<int> insert(TableName table,Map<String, dynamic> row) async {
    Database db = await database;
    row.removeWhere((key, value) => key.contains("sql"));
    return await db.insert(table.name, row);
  }


  Future<List<Map<String, dynamic>>> queryAll(TableName table) async {
    Database db = await database;
    return await db.query(table.name);
  }

  Future<List<Map<String, dynamic>>> queryByCurrUserId(TableName table,String userId) async {
    Database db = await database;
    return await db.query(table.name,where: 'senderId=?',whereArgs:[userId],orderBy:'createTime desc');
  }

  Future<List<Map<String, Object?>>> queryObjectById(TableName table,dynamic id) async {
    Database db = await database;
    return await db.query(table.name,where: 'id=?',whereArgs:[id],orderBy:'createTime desc');
  }
/// 查询消息列表
  Future<List<Map<String, dynamic>>> queryAllContextRecord(TableName table) async {
    Database db = await database;
    return await db.query(table.name,  groupBy: 'receiverId', having: "timestamp=max(timestamp)", orderBy:'timestamp desc');
  }
  /// 查询消息列表
  Future<List<Map<String, dynamic>>> querySql(String sql) async {
    Database db = await database;
    return await db.rawQuery(sql);
  }
  Future<int> update(TableName table,Map<String, dynamic> row) async {
    Database db = await database;
    dynamic id = row['id'];
    return await db.update(
      table.name,
      row,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<int> delete(TableName table,int id) async {
    Database db = await database;
    return await db.delete(
      table.name,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  ///更新已读  ///userId：发过来的用户id   isGroup单聊信息还是群聊信息，0，代表单聊，1群聊
  updateMessageRead(TableName table,String id,int isGroup) async {
    Database db = await database;
    String sql= "UPDATE ${table.name} SET isRead =1 WHERE userId = ? and isGroupMessage=?";
    db.rawUpdate(sql,[id,isGroup]);
  }

   deleteNewMessageRecordById(TableName table, String id) async {
    Database db = await database;
    return await db.delete(
      table.name,
      where: 'id = ?',
      whereArgs: [id],
    );
  }
  ///保存消息记录
  Future<void> insertNewMessageRecord(NewMessageRecord bean)async {
    await DBHelper().insert(TableName.newMessageRecord,bean.toJson());
  }



}
