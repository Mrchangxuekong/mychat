import 'package:json_annotation/json_annotation.dart';

part 'message_dao.g.dart';

@JsonSerializable()
class MessageDao {
  int? id;
  String? senderId;

  /// 发送人
  String? sendPortrait;

  /// 发送人
  String? sendNickName;

  /// 发送人
  String? receiverId;

  /// 接收人
  String? content;

  /// 发送信息
  String? portrait;

  /// 接收人图片
  String? nickName;

  /// 视频封面，当且仅当该消息为视频时有值
  String? coverUrl;

  /// 接收人微信号
  DateTime? createTime;

  ///发送时间
  int? self;

  ///是否是自己发的信息 0不是，1，是
  int? isRead;

  ///消息类型（0，文字，1，图片）
  int? type;

  ///0，未读，1，已读
  int? isGroupMessage;

  ///单聊还是群聊，0，单聊，1，群聊

  ///sql 开头的字段表示不插入数据库 ///未读数量
  int? sqlNoIsReadNum;

  ///最大时间  sql开头的字段表明该字段不插入数据库
  DateTime? sqlMaxTimes;

  MessageDao(
      {this.id,
      this.senderId,
      this.type,
      this.sendNickName,
      this.sendPortrait,
      this.receiverId,
      this.coverUrl,
      this.content,
      this.portrait,
      this.nickName,
      this.createTime,
      this.isRead,
      this.self,
      this.isGroupMessage});

  ///是单聊还是群聊，0，单聊，1，群聊

  factory MessageDao.fromJson(Map<String, dynamic> json) =>
      _$MessageDaoFromJson(json);

  Map<String, dynamic> toJson() => _$MessageDaoToJson(this);

  static List<MessageDao> fromJsonList(List<dynamic> jsonArray) {
    return jsonArray.map((json) => MessageDao.fromJson(json)).toList();
  }
}
