// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_message_record.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewMessageRecord _$NewMessageRecordFromJson(Map<String, dynamic> json) =>
    NewMessageRecord(
      userId: json['userId'] as String?,
      portrait: json['portrait'] as String?,
      nickName: json['nickName'] as String?,
      content: json['content'] as String?,
      createTime: json['createTime'] == null
          ? null
          : DateTime.parse(json['createTime'] as String),
      isRead: (json['isRead'] as num?)?.toInt(),
          id: (json['id'] as num?)?.toInt(),
      sqlNoIsReadNum: (json['sqlNoIsReadNum'] as num?)?.toInt(),
          isGroupMessage: (json['isGroupMessage'] as num?)?.toInt(),
    );

Map<String, dynamic> _$NewMessageRecordToJson(NewMessageRecord instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'portrait': instance.portrait,
      'nickName': instance.nickName,
      'content': instance.content,
      'createTime': instance.createTime?.toIso8601String(),
      'isRead': instance.isRead,
      'isGroupMessage': instance.isGroupMessage,
      'sqlNoIsReadNum': instance.sqlNoIsReadNum,
    };
