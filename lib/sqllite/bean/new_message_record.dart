
import 'package:json_annotation/json_annotation.dart';

part 'new_message_record.g.dart';

@JsonSerializable()
class NewMessageRecord{

  ///组合id 该userid和isGroupMessage是否是群聊，组成
  int? id;

  ///对方id ，
  String? userId;
  ///对方图片
  String? portrait;

  /// 发送人
  String? nickName;
  /// 内容
  String? content;
  ///是否是群聊
  int? isGroupMessage;

  /// 发送时间
  DateTime? createTime;

  ///未读数量
  int? isRead;

  ///sql 开头的字段表示不插入数据库 ///未读数量
  int? sqlNoIsReadNum;

  NewMessageRecord({this.id, this.userId, this.portrait, this.nickName,
      this.content, this.createTime, this.isRead,this.isGroupMessage,this.sqlNoIsReadNum});

  factory NewMessageRecord.fromJson(Map<String, dynamic> json) =>
      _$NewMessageRecordFromJson(json);

  Map<String, dynamic> toJson() => _$NewMessageRecordToJson(this);

  static List<NewMessageRecord> fromJsonList(List<dynamic> jsonArray) {
    return jsonArray.map((json) => NewMessageRecord.fromJson(json)).toList();
  }
}