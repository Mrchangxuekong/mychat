// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_dao.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageDao _$MessageDaoFromJson(Map<String, dynamic> json) => MessageDao(
      id: (json['id'] as num?)?.toInt(),
      senderId: json['senderId'] as String?,
      type: (json['type'] as num?)?.toInt(),
      sendNickName: json['sendNickName'] as String?,
      sendPortrait: json['sendPortrait'] as String?,
      receiverId: json['receiverId'] as String?,
      coverUrl: json['coverUrl'] as String?,
      content: json['content'] as String?,
      portrait: json['portrait'] as String?,
      nickName: json['nickName'] as String?,
      createTime: json['createTime'] == null
          ? null
          : DateTime.parse(json['createTime'] as String),
      isRead: (json['isRead'] as num?)?.toInt(),
      self: (json['self'] as num?)?.toInt(),
      isGroupMessage: (json['isGroupMessage'] as num?)?.toInt(),
    )
      ..sqlNoIsReadNum = (json['sqlNoIsReadNum'] as num?)?.toInt()
      ..sqlMaxTimes = json['sqlMaxTimes'] == null
          ? null
          : DateTime.parse(json['sqlMaxTimes'] as String);

Map<String, dynamic> _$MessageDaoToJson(MessageDao instance) =>
    <String, dynamic>{
      'id': instance.id,
      'senderId': instance.senderId,
      'sendPortrait': instance.sendPortrait,
      'sendNickName': instance.sendNickName,
      'receiverId': instance.receiverId,
      'content': instance.content,
      'portrait': instance.portrait,
      'nickName': instance.nickName,
      'coverUrl': instance.coverUrl,
      'createTime': instance.createTime?.toIso8601String(),
      'self': instance.self,
      'isRead': instance.isRead,
      'type': instance.type,
      'isGroupMessage': instance.isGroupMessage,
      'sqlNoIsReadNum': instance.sqlNoIsReadNum,
      'sqlMaxTimes': instance.sqlMaxTimes?.toIso8601String(),
    };
