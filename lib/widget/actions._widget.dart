import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';

class ActionsWidget extends StatefulWidget {
  final double itemHeight = 53; //组件的高度
  List<ItemTitle> listItem = [];
  ItemTitle tip;

  ActionsWidget({required this.tip, required this.listItem, super.key});

  @override
  State<ActionsWidget> createState() => _ActionsWidgetState();
}

class _ActionsWidgetState extends State<ActionsWidget> {
  ///点击的y坐标位置

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 5),
      child: PopupMenuButton(
          color: ThemeColor.gray200,
          offset: const Offset(10, 30),
          child: const Icon(Icons.add_circle_outline),
          itemBuilder: (BuildContext context) {
            return getPopupMenuItems();
          }),
    );
  }

  getPopupMenuItems() {
    List<PopupMenuItem> list = [];
    for (int i = 0; i < widget.listItem.length; i++) {
      list.add(_popupMenuItem(widget.listItem[i], i));
    }
    return list;
  }

  _popupMenuItem(ItemTitle item, index) {
    return PopupMenuItem(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 0),
        onTap: item.dismissHandle,
        child: Row(
          children: [
            Container(
              child: Icon(
                item.icon,
                color: Colors.white,
              ),
            ),
            Expanded(
                child: Container(
              decoration: widget.listItem.length - 1 != index
                  ? const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                      color: Colors.grey,
                    )))
                  : null,
              padding: const EdgeInsets.only(left: 20, top: 10, bottom: 10),
              child: Text(
                textAlign: TextAlign.start,
                item.title,
                style: const TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ))
          ],
        ));
  }
}
