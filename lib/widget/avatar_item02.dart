import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/chat/apply_vo02.dart';

class AvatarItem02 extends StatelessWidget {
  const AvatarItem02({required this.fn,required this.bean,this.needLine=false,super.key});
  final ApplyVo02 bean;
  final bool needLine;
  final VoidCallback fn;
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child:
        Row(children: [
          Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            margin: const EdgeInsets.all(12),
            height: 66,
            width: 66,
            child:
            CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl:bean.portrait!,
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),

          Expanded(
            child:Container(
              decoration: BoxDecoration(color: Colors.white,
                  border: Border(bottom:needLine?const BorderSide(color: ThemeColor.gray50,width: 1):BorderSide.none)
              ),child:
            Row(children: [
              Expanded(child:
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    Text(bean.nickName!),
                    const SizedBox(height: 20,),
                    Text(bean.reason!),
                    const SizedBox(height: 2,)
                  ]),)
              ,
              const SizedBox(width: 10,),
              getWidget(bean.applyStatus!),
              const SizedBox(width: 20,)
            ],),),),
        ],)
        ,);
  }

  getWidget(String applyStatus) {
    Widget res= ElevatedButton(onPressed:fn, child: const Text("同意"));
    switch(applyStatus){
      case"1":
        res=const Text("已添加",style: TextStyle(color: Colors.green),) ;
        break;
      case"2":
        res=const Text("已拒绝",style: TextStyle(color: Colors.red)) ;
        break;
      case"3":
        res=const Text("已忽略") ;
        break;
      default:
    }
    return res;
  }
}
