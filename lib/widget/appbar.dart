import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

//顶部导航栏
class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  const AppBarWidget({
    super.key,
    this.backgroundColor,
    this.elevation,
    this.leading,
    this.actions,
    this.isAnimated,
    this.isShow,
    this.title,
  });
  //是否动画
  final bool? isAnimated;

  //是否显示
  final bool? isShow;

  //标题
  final Widget? title;

  //用于应用栏的[材质]的填充颜色。
  final Color? backgroundColor;

  //该属性控制应用栏下方阴影的大小
  final double? elevation;

  //在工具栏的 [标题] 之前显示的小部件。
  final Widget? leading;

  //在 [title] 小部件之后连续显示的小部件列表
  final List<Widget>? actions;

  @override
  Size get preferredSize => const Size.fromHeight(30);

  Widget _mainView() {
    var appBar = AppBar(
      backgroundColor: backgroundColor ?? Colors.transparent,
      elevation: elevation ?? 0,
      leading: leading,
      actions: actions,
      title: title,
    );

    //动画的开与关，如果不用就直接返回一个appbar
    return isAnimated == true
        ? isShow == true
            ? FadeInDown(
                duration: const Duration(milliseconds: 100),
                child: appBar,
              )
            : FadeInUp(
                duration: const Duration(
                  milliseconds: 100,
                ),
                child: appBar,
              )
        : appBar;
  }

  @override
  Widget build(BuildContext context) {
    return _mainView();
  }
}
