import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/persons/friend_vo07.dart';
import 'package:zhongdingapp/entity/persons/message_vo.dart';

class ViewListItem extends StatelessWidget {
   const
   ViewListItem(this.item,{super.key});
   final FriendVo07 item;

  @override
  Widget build(BuildContext context) {

    return  InkWell(onTap: (){
       item.onTap!();
    },child: Row(
      children: [
        Stack(children: [
          Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
            ),
            margin: const EdgeInsets.all(12),
            height: 66,
            width: 66,
            child:
            CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl:item.portrait!,
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),

        item.chatNo==null?Container():Positioned(
              right: 13/2,
              top: 13/2,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(18)),
                child: Text(
                  item.chatNo!.toString(),
                  style: const TextStyle(color: Colors.white, fontSize: 8),
                ),
              )),
        ],),
        Expanded(
            child: Container(
              height: 66,

              decoration: const BoxDecoration(

                  border: Border(bottom: BorderSide(color: ThemeColor.black2))),
              child:  Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(item.nickName!),
                      ),
                       Text(
                        item.chatNo!,
                        style: const TextStyle(fontSize: 10,color: ThemeColor.hintText20),
                      ),
                      const SizedBox(width: 10,)
                    ],
                  ),
                  const SizedBox(height: 5,),
                  Expanded(child: Text(
                    item.chatNo!
                    ,
                    style: const TextStyle(fontSize: 10,color: ThemeColor.hintText20),
                  ),)

                ],
              ),
            )),
      ],
    ),)  ;
  }
}
