import 'dart:ffi';

import 'package:flutter/material.dart';

class PositionedRed extends StatelessWidget {
  const PositionedRed({super.key, this.top,this.right,this.bottom,this.left,required this.num});
  final int num;
  final double? top;
  final double? right;
  final double? bottom;
  final double? left;

  @override
  Widget build(BuildContext context) {
    return num!=0?Positioned(
        right: right,
        top: top,
        left: left,
        bottom: bottom,
        child: Container(
          alignment: Alignment.center,
          width: 19,
          height: 19,
          decoration: BoxDecoration(
              color: Colors.redAccent,
              borderRadius: BorderRadius.circular(16)),
          child:  Text(
            num.toString(),
            style: const TextStyle(color: Colors.white, fontSize: 9),
          ),
        )):Container();
  }
}
