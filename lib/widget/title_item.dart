import 'package:flutter/material.dart';

class TitleItem extends StatelessWidget {
  const TitleItem({super.key,this.textColor, this.size, this.color, required this.title});

  final String title;
  final Color? color;
  final double? size;
  final Color? textColor;


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      decoration: BoxDecoration(color: color),
      child: Text(
        title,textAlign: TextAlign.start,
        style: TextStyle(fontSize: size,color: textColor,),
      ),
    );
  }
}
