import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/themeColor.dart';

class ListTitleItem01 extends StatelessWidget {
  const ListTitleItem01({this.left,required this.title,this.right,this.size,this.needLine=false,super.key});
  final IconData? left;
  final String title;
  final IconData? right;
  final bool needLine;
  final double? size;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(color: Colors.white,
      border: Border(bottom:needLine?const BorderSide(color: ThemeColor.red500,width: 4):BorderSide.none)
      ),
      child:
        Row(
          children: [
            const SizedBox(width: 10,),
            Icon(left,size: size,),
            const SizedBox(width: 10,),
            Expanded(child: Text(title)),
            Icon(right,size: size),
            const SizedBox(width: 10,),
          ],
        )
      ,);
  }
}
