import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/iconfont/icon_font.dart';

class ShowVideo extends StatelessWidget {
  const ShowVideo({super.key, required this.url,this.width=80,this.height=80});
  final String url;
  final double width;
  final double height;
  @override
  Widget build(BuildContext context) {
    return
        Stack(
          alignment: Alignment.center,
          children: [
          CachedNetworkImage(
            width: 120,
            height: 150,
            imageUrl:url,
            fit: BoxFit.cover,
            placeholder: (context, url) => const CircularProgressIndicator(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
            const Positioned(
                child: Icon(Icons.slow_motion_video_sharp,color: Colors.white,size: 40,))

        ],

    );
  }
}
