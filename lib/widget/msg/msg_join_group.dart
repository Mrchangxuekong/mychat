import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/my_icons.dart';

class MsgJoinGroup extends StatelessWidget {
  const MsgJoinGroup({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(child: Padding(
      padding: EdgeInsets.symmetric(vertical: 7),
      child: Text(
        "你邀请了xxx、abgflutter加入了群聊",
        style: MyIcons.msgTime,
      ),
    ),);
  }
}
