import 'package:fijkplayer/fijkplayer.dart';
import 'package:flutter/material.dart';
///视频播放器页面
class MsgVideoPlay extends StatefulWidget {
  const MsgVideoPlay(this.url,{super.key});
  final String url;
  @override
  State<MsgVideoPlay> createState() => _MsgVideoPlayState();
}

class _MsgVideoPlayState extends State<MsgVideoPlay> {
  FijkPlayer player=FijkPlayer();
  @override
  void initState() {
    player.setDataSource(widget.url,autoPlay: true);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.white, // 设置返回箭头颜色为白色
        ),
        backgroundColor: Colors.black,),
      body: FijkView(player: player,),
    );
  }
}
