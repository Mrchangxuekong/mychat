
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/routerHelper.dart';
import 'package:zhongdingapp/widget/msg/msg_video_play.dart';
import 'package:zhongdingapp/widget/msg/show_image.dart';
import 'package:zhongdingapp/widget/msg/show_video.dart';

class MsgText extends StatelessWidget {
  const MsgText({this.self = false,this.coverUrl, required this.portrait,this.content = "",required this.nikeName, super.key, required this.type});

  final bool self;
  final String content;
  final String portrait;
  final String nikeName;
  final String? coverUrl;
  final int  type;


  @override
  Widget build(BuildContext context) {
    const double trianglessSize = 6;
    const double imgHeight = 41;
    final List<Widget> bodyList = [
      const SizedBox(
        width: 12,
      ),
      ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: CachedNetworkImage(
            height: imgHeight,
            width: 41,
            fit: BoxFit.cover,
            imageUrl:portrait),
      ),
      const SizedBox(
        width: 12,
      ),
      Expanded(
          child: Column(

        crossAxisAlignment:
            self ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          if (!self)  Text(nikeName),
          Container(
            margin: const EdgeInsets.only(top: 6,bottom: 6,left: 5,right: 5),
            child:  showMsg(context, trianglessSize, imgHeight),)

        ],
      )),
    ];
  return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: self ? bodyList.reversed.toList() : bodyList,
    );
  }

  showMsg(BuildContext context,double trianglessSize,double imgHeight) {
    switch(type){
      case 0: ///显示文字
       return addMsgText(context,trianglessSize,imgHeight);
      case 1: ///显示图片
        return addMsgImage();
      case 2: ///显示视频
        return addMsgVideo();
      default:
       return addMsgText(context,trianglessSize,imgHeight);
    }

  }

  addMsgText(BuildContext context,double trianglessSize,double imgHeight) {
    return
      Stack(
        children: [
          Container(
            constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.7),
            //限制宽度
            margin:  EdgeInsets.symmetric(
                vertical: trianglessSize, horizontal: trianglessSize),
            padding:  EdgeInsets.symmetric(
                vertical: trianglessSize, horizontal: trianglessSize),
            decoration: BoxDecoration(
                color: !self ? Colors.white : ThemeColor.mainGreen,
                borderRadius:  BorderRadius.circular(5)),
            child: Text(
              content,
              style: self ? MyIcons.chatNoSelf : MyIcons.chatSelf,
            ),
          ),
          Positioned(
              right: self ? 0 : null,
              top: () {
                TextPainter t = TextPainter(
                    text: TextSpan(
                      text: content,
                    ),
                    textDirection: TextDirection.ltr)
                  ..layout(maxWidth: 220);
                return (t.size.height + trianglessSize) / 2 > imgHeight
                    ? trianglessSize * 2
                    : (t.size.height + trianglessSize) / 2;
              }(),
              child: Container(
                width: trianglessSize,
                height: trianglessSize + 3,
                child: CustomPaint(
                  painter: TrianglePainter(self),
                ),
              )),
        ],
      )
   ;
  }

  addMsgImage() {
    //return  ShowImage(url:content ,width: 100,height: 250,);
    //158B1822
    return InkWell(
      onTap: ()=>Get.toNamed(RouterHelper.checkImage,arguments: {"url":content,"title":"查看"}),
      child:  ShowImage(url:content,width: 100,height: 250,) ,);
    return const ShowImage(url: MyIcons.networkImg4,width: 100,height: 250,);
  }
  ///显示视频
  addMsgVideo() {
    return InkWell(
      onTap: ()=>Get.to( MsgVideoPlay(content)),
      child: ShowVideo(url:coverUrl!,width: 100,height: 250,) ,);

  }
}

class TrianglePainter extends CustomPainter {
  TrianglePainter(this.self);

  bool self;

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = self ? ThemeColor.mainGreen : Colors.white
      ..style = PaintingStyle.fill;
    var path = Path()
      ..moveTo(0, size.height / 2)
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height)
      ..close();
    if (self) {
      path = Path()
        ..moveTo(0, size.height)
        ..lineTo(size.width, size.height / 2)
        ..lineTo(0, 0)
        ..close();
    }
    canvas.drawPath(path, paint);

    // TODO: implement paint
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
