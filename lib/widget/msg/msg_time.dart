import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:zhongdingapp/color/my_icons.dart';

class MsgTime extends StatelessWidget {
  const MsgTime({super.key,required this.msgTime});
  final DateTime msgTime;

  @override
  Widget build(BuildContext context) {

    return Center(child:Container(
      padding: const EdgeInsets.only(top: 8),
      child: Text( DateFormat('MM-dd HH:mm:ss').format(msgTime),style:MyIcons.msgTime,),) ,) ;
  }
}
