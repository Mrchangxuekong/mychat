import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/my_icons.dart';

class MsgText1 extends StatelessWidget {
  const MsgText1( {this.self=false,this.content="", super.key});
  final bool self;
  final String content;


  @override
  Widget build(BuildContext context) {
   final List<Widget> bodyList= [
      Column(
        crossAxisAlignment:self? CrossAxisAlignment.start:CrossAxisAlignment.end,
        children: [
          self?const Text(""):const Text("1212"),
          Text("你好"),
        ],),
     const SizedBox(width: 10,),
     ClipRRect(
       borderRadius: BorderRadius.circular(4),
       child: CachedNetworkImage(
           height: 41,
           width: 41,
           fit: BoxFit.cover,
           imageUrl: MyIcons.networkImg1) ,),
     const SizedBox(width: 12,),

    ];
    return Padding(
      padding:const EdgeInsets.symmetric(vertical: 9,horizontal: 9),
      child:
    Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:self?bodyList.reversed.toList():bodyList,),);

  }
}
