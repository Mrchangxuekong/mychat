import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/themeColor.dart';

class AvatarItem01 extends StatelessWidget {
  const AvatarItem01({required this.url,required this.fn,required this.title,this.subTitle="我是：",this.buttonText="接受",this.needLine=false,super.key});
  final String url;
  final bool needLine;
  final String title;
  final String subTitle;
  final String buttonText;
  final VoidCallback fn;
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child:
        Row(children: [
          Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            margin: const EdgeInsets.all(12),
            height: 66,
            width: 66,
            child:
            CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl:url,
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),

          Expanded(
            child:Container(
              decoration: BoxDecoration(color: Colors.white,
                  border: Border(bottom:needLine?const BorderSide(color: ThemeColor.gray50,width: 1):BorderSide.none)
              ),child:
            Row(children: [
              Expanded(child:
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    Text(title),
                    const SizedBox(height: 20,),
                    Text(subTitle),
                    const SizedBox(height: 2,)
                  ]),)
              ,
              ElevatedButton(onPressed:fn, child: Text(buttonText)),
              const SizedBox(width: 20,)
            ],),),),
        ],)
        ,);
  }
}
