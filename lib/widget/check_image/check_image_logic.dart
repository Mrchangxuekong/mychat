import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';

class CheckImageLogic extends GetxController {
  var url=MyIcons.networkImg3.obs;
  var title="查看图片".obs;
  @override
  void onInit() {
    print("dk");
    if(Get.arguments["url"]!=null){
      url.value=Get.arguments["url"];
      update();
    }
    if(Get.arguments["title"]!=null){
      title.value=Get.arguments["title"];
    }
    update();
    super.onInit();
  }

  @override
  void onReady() {
    update();
  }
}
