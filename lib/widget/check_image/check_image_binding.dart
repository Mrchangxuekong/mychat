import 'package:get/get.dart';

import 'check_image_logic.dart';

class CheckImageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CheckImageLogic());
  }
}
