import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';

import 'check_image_logic.dart';

class CheckImagePage extends StatelessWidget {
  const CheckImagePage({super.key,});


  @override
  Widget build(BuildContext context) {
    final logic = Get.put(CheckImageLogic());
    print("zou");
    print(logic.url.value);
    return GetBuilder<CheckImageLogic>(builder: (C)=>Scaffold(
        appBar: AppBar(
          title: Text("查看图片"),
        ),
        body:PhotoView(
                imageProvider:  NetworkImage(logic.url.value))));
   /* return Scaffold(
      appBar: AppBar(
        title: Text("查看图片"),
      ),
      body: GetBuilder<CheckImageLogic>(
          init: logic,
          builder:(c)=>PhotoView(
          imageProvider:  NetworkImage(logic.url.value))));*/
  }
}
