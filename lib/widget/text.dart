import 'package:flutter/material.dart';

class TextMaxWidget extends StatefulWidget {
  const TextMaxWidget({
    super.key,
    required this.content,
    this.maxLines,
  });

  final String content;
  final int? maxLines;

  @override
  State<TextMaxWidget> createState() => _TextMaxWidgetState();
}

class _TextMaxWidgetState extends State<TextMaxWidget> {
  //内容
  late final String _content;

  //最大行数
  late final int _maxLines;

  //是否展开
  bool _isExpansion = false;

  @override
  void initState() {
    super.initState();
    _content = widget.content;
    _maxLines = widget.maxLines ?? 3;
  }

  //主视图
  Widget _mainView() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        //将TextSpan树绘制到Canvas中的对象
        final TextPainter textPainter = TextPainter(
          text: TextSpan(
            text: _content,
            style: const TextStyle(
              fontSize: 15,
              color: Colors.blue,
            ),
          ),
          maxLines: _maxLines,
          textDirection: TextDirection.ltr,
        )..layout(
            maxWidth: constraints.maxWidth,
          );

        //1、不展开
        if (_isExpansion == false) {
          List<Widget> ws = [];

          //检查是否超出高度
          if (textPainter.didExceedMaxLines && _isExpansion == false) {
            ws.add(
              Text(
                _content,
                maxLines: _maxLines,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontSize: 15,
                  color: Colors.black87,
                ),
              ),
            );
            ws.add(
              GestureDetector(
                onTap: () {
                  _doExpansion();
                },
                child: const Text(
                  "全文",
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.blue,
                  ),
                ),
              ),
            );
          }
          //1.2不超出显示全部
          else {
            ws.add(
              Text(
                _content,
                style: const TextStyle(
                  fontSize: 15,
                  color: Colors.black87,
                ),
              ),
            );
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: ws,
          );
        }
        //展示全部
        else {
          return Text(
            _content,
            style: const TextStyle(
              fontSize: 15,
              color: Colors.black87,
            ),
          );
        }
      },
    );
  }

  void _doExpansion() {
    setState(() {
      _isExpansion = !_isExpansion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _mainView();
  }
}
