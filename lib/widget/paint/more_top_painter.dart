import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/util/colorutils.dart';

class MoreTopPainter extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
        Paint paint= Paint()..color=Color(ColorUtils.intByColor(ThemeColor.grayTopBackground))
        ..style=PaintingStyle.fill;
        Path path=Path();
        path.moveTo(0, size.height);
        path.lineTo(size.width/2+10, 0);
        path.lineTo(size.width/2+10, 0);
        path.lineTo(size.width, size.height);
        path.close();
        canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    ///是否重新绘制  false 代表否
    return false;
  }


}
