import 'package:flutter/material.dart';
import 'package:zhongdingapp/color/themeColor.dart';
import 'package:zhongdingapp/entity/base/item_title.dart';
import 'package:zhongdingapp/widget/paint/more_top_painter.dart';


void moreDialog(BuildContext context, TapUpDetails d, List<ItemTitle> list) {
  OverlayEntry? entry;
  entry = OverlayEntry(builder: (context) {
    return Material(
        type: MaterialType.transparency,
        child: MoreDialog(
          clickDy: d.globalPosition.dy,
          listItem: list,
          dismissHandle: () {
            entry!.remove();
          },
        ));
  });
  Overlay.of(context).insert(entry);
}

class MoreDialog extends StatefulWidget {
  final VoidCallback dismissHandle;
  final double clickDy;

  ///点击的y坐标位置
  final double itemHeight = 53; //组件的高度
  List<ItemTitle> listItem = [];

  MoreDialog(
      {required this.dismissHandle,
      required this.clickDy,
      required this.listItem,
      super.key});

  @override
  State<MoreDialog> createState() => _MoreDialogState();
}

class _MoreDialogState extends State<MoreDialog> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Align(
          alignment: Alignment.topRight,
          child: Container(
            margin: EdgeInsets.only(top: widget.clickDy + 22, right: 9),
            width: 130,
            height: widget.itemHeight * widget.listItem.length+53,
            child: getWidget(),
          ),
        ),
        onTap: () {
          widget.dismissHandle();
        });
  }

  getWidget() {
    return Column(
      children: [
        Align(
          alignment: Alignment.topRight,
          child: Container(
          height: 15,
          width: 40,
          child: CustomPaint(painter: MoreTopPainter()),) ,),
        Expanded(
            child: ListView.builder(
          itemBuilder: (context, index) {
            var row = Container(
              color: ThemeColor.grayTopBackground,
              child: InkWell(
                child: Row(
                  children: [
                    SizedBox(
                      height: 53,
                      width: 40,
                      child: Icon(widget.listItem[index].icon),
                    ),
                    Text(widget.listItem[index].title),
                  ],
                ),
                onTap: () {widget.listItem[index].dismissHandle!();
                  widget.dismissHandle();} ,
              ),
            );
            return row;
          },
          itemCount: widget.listItem.length,
          padding: const EdgeInsets.all(0),
        ))
      ],
    );
  }
}
