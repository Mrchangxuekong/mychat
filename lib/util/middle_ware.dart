import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/util/cache_util.dart';
import 'package:zhongdingapp/util/mqtt_utils.dart';
import 'package:zhongdingapp/routerHelper.dart';

import '../config/r.dart';

class MiddleWare extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {

   // print(CacheUtil.getToken().runtimeType);
    ///判断是否登录
    ///CacheUtil.getToken()
    ///
    String token="";
    token="";
    String userId="";
    Future.wait([getToken(),getUserId()]).then((List values) async {
      token=values[0] ?? "";//
      userId=values[1]??"";
      if(token.isEmpty){///还未登录
        return const RouteSettings(name: RouterHelper.login);
      }
      await Mqttutils.getInstance(userId);
      Mqttutils.addAllSubscribed();

    });

  }

  Future<String?> getToken() async{
   return  await CacheUtil.getToken();
  }

  Future<String?> getUserId() async{
    return await CacheUtil.getKey(R.userId);
  }




}

