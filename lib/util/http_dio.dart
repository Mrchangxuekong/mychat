import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/get.dart' as getAs;
import 'package:zhongdingapp/entity/persons/message_vo.dart';
import 'package:zhongdingapp/util/cache_util.dart';
import 'package:zhongdingapp/util/curr_device.dart';
import 'package:zhongdingapp/routerHelper.dart';
import '../config/r.dart';
import '../entity/base/base_common_response.dart';

class HttpDio {
  // default options
  static const int _connectTimeout = 15000; //15s
  static const int _receiveTimeout = 15000;
  static const int _sendTimeout = 10000;

  static const String GET = 'get';
  static const String POST = 'post';
  static const String PUT = 'put';
  static const String PATCH = 'patch';
  static const String DELETE = 'delete';
  //static String basicUrl = "http://$constApiBaseUrl:8080/";
  static const bool noUpload = true;

  ///默认不是上传
  ///static String basicUrl ="http://$constApiBaseUrl:8080/" ;
  static String basicUrl = "$constApiBaseUrl/";

  var _dio;
  static HttpDio? _httpUtil;

  static HttpDio getInstance() => _httpUtil ??= HttpDio();
  static Map<String, String> header = {
    "version": "1.0.0",
    HttpHeaders.contentTypeHeader:Headers.jsonContentType
  };
  // 创建 dio 实例对象
  Dio? _createInstance() {
    ///头部认证信息dependencies:
    //   device_info_plus: ^5.0.0
    /*print("fdskfhdskfhdskf${CacheUtil.getToken()}");
     if (CacheUtil.isToken()) {///存在token ，已登录过
       header["Authorization"]=CacheUtil.getToken();
       header["version"]=ConstA.apiVersion;
       header["device"]=CacheUtil.getKey(ConstA.device);
    }*/
    if (_dio == null) {
      /// 全局属性：请求前缀、连接超时时间、响应超时时间
      var options = BaseOptions(
        headers: header,
        // responseType: ResponseType.json,
        baseUrl: basicUrl,
        connectTimeout: const Duration(milliseconds: _connectTimeout),
        receiveTimeout: const Duration(milliseconds: _receiveTimeout),
        sendTimeout: const Duration(milliseconds: _sendTimeout),
      );

      //拦截器
      var interceptor =
          InterceptorsWrapper(onRequest: (options, handler) async {
        bool cc = await CacheUtil.isToken();
        if (cc) {
          String authorization = (await CacheUtil.getToken()) as String;
          String? device = await CacheUtil.getKey(R.device);
          header["Authorization"] = authorization;
          header["version"] = R.apiVersion;
          header["device"] = device!;
          options.headers.addAll(header);
        }
        return handler.next(options);
      }, onResponse: (response, handler) {
        if (response.data['code'] == 401) {
          ///用户token过期
          getAs.Get.snackbar("提示", response.data['msg']);
          getAs.Get.toNamed(RouterHelper.login);
          return handler.resolve(response);
        }
        return handler.next(response);
      }, onError: (e, handler) {
        return handler.next(e);
      });

      _dio = Dio(options);
      _dio.interceptors.add(interceptor);
      _dio.interceptors.add(JsonStringTransformer());
    }
    return _dio;
  }

  Future<T> get<T>(String url, dynamic param) async {
    return _requestHttp<T>(
      url,
      param: param,
      method: GET,
    );
  }

  Future<T> getObj<T>(String url, T? obj) async {
    return _requestHttp<T>(
      url,
      param: obj,
      method: GET,
    );
  }

  Future<T> post<T>(String url, dynamic param) async {
    return _requestHttp<T>(
      url,
      param: param,
      method: POST,
    );
  }

  /// T is Map<String,dynamic> response?.data,  or List<dynamic>
  _requestHttp<T>(String url, {param, method}) async {
    _dio = _createInstance()!;
    try {
      var response = await _dio?.request(url,
          data: param, options: Options(method: method));
      if (response?.statusCode == 200) {
        return response.data;
      }
    } on DioException catch (e) {
      /// 打印请求失败相关信息
      print("【请求出错】${e.toString()}");
      rethrow;
    }
  }

  // 清空 dio 对象
  clear() {
    _dio = null;
  }

  ///该上传方法，有强制要求，api接口要包含(flie/)字符串
  Future upload<T>(String url, File file) async {
    MultipartFile item = await MultipartFile.fromFile(file.path,
        filename: file.path.split('/').last);
    final formData = FormData.fromMap({
      'name': 'dio',
      'date': DateTime.now().toIso8601String(),
      'file': item,
    });
    var resp = await post<dynamic>(url, formData);
    if (resp[R.code] == 200) {
      return resp[R.data]["fullPath"];
    }
    return null;
  }
}

class JsonStringTransformer extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (options.data != null && !options.path.contains("file/")) {
      Map<String, String> header = {
        Headers.contentTypeHeader: Headers.jsonContentType,
      };
      Map<String, dynamic> head = options.headers;
      head.addAll(header);
      options.headers = head;
      options.data = jsonEncode(options.data);
    }
    if (options.path.contains("file/")) {
      ///默认是json 如果是文件上传，则需要转multipart/form-data
      Map<String, String> header = {
        Headers.contentTypeHeader: Headers.multipartFormDataContentType,
      };
      Map<String, dynamic> head = options.headers;
      head.addAll(header);
      options.headers = head;
    }
    return super.onRequest(options, handler);
  }
}
