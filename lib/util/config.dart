import 'package:flutter/material.dart';

//间距
const double spacing = 10.0;

//图片选取数量
const int maxAssets = 9;

//强调色
const Color accentColor = Colors.blue;

//图片padding
const double imagePadding = 3;

// 视频录制最大时间 秒
const double maxVideoDuration = 30;

//次文字颜色
const Color secondaryTextColor = Colors.black45;

//页面padding
const double pagePadding = 12;

//api url
const String apiUrl = "https://mock.apifox.cn/m1/2249037-0-default";

//申请好友
String friends_addurl = "http://192.168.1.51:8080/";
