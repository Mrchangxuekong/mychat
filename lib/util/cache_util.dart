import 'dart:convert';

import 'package:get/get.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/base/login_user.dart';
import 'package:zhongdingapp/routerHelper.dart';

class CacheUtil{
  ///删除token
  static  deleteToken() async{
    var openBox = await Hive.openBox(constHiveBox);
    openBox.delete(R.token);
    Get.toNamed(RouterHelper.login);
  }
  ///是否存在token
  static  Future<bool> isToken()async{
    var openBox = await Hive.openBox(constHiveBox);

    var token=  openBox.get(R.token);
    return token!=null;
  }



  ///是否存在token
  static   Future<String?>   getToken()async{
    var openBox = await Hive.openBox(constHiveBox);
  var token= await openBox.get(R.token);
    return token;
  }


  ///是否存在token
  static  Future<String?>  getKey(String key) async{
    var openBox = await Hive.openBox(constHiveBox);
    return openBox.get(key);
  }

  ///保存一个key
  static  addKey(String key,String json)async {
    var openBox = await Hive.openBox(constHiveBox);
    return openBox.put(key,json);
  }

  ///获取登录用户
  static Future<LoginUser>    getUser()async {
    var openBox = await Hive.openBox(constHiveBox);
         String jsonUser=await openBox.get("user");
    return LoginUser.fromJson(json.decode(jsonUser));
  }
  ///保存登录用户
  static Future<void>  putUser(String jsonLoginUser)async {
    var openBox = await Hive.openBox(constHiveBox);
    return openBox.put("user",jsonLoginUser);
  }

}