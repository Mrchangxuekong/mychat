import 'dart:ui';

class ColorUtils {
  static String colorToHex(Color color) {
    return  '#${color.value.toRadixString(16).padLeft(8, '0').toUpperCase()}';
  }

  static int getColorFromHex(String hexColor) {
    if (hexColor.startsWith("#")) {
      hexColor = hexColor.toUpperCase().replaceAll("#", "");
    } else {
      hexColor = hexColor.toUpperCase();
    }
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
  static int intByColor(Color color) {
    return getColorFromHex(colorToHex(color));
  }


}