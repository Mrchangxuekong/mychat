import 'package:get/get.dart';
import 'package:zhongdingapp/controller/apply_add_friend_controller.dart';
import 'package:zhongdingapp/controller/chat_group_controller.dart';
import 'package:zhongdingapp/controller/communication_controller.dart';
import 'package:zhongdingapp/controller/homeController.dart';
import 'package:zhongdingapp/controller/loginController.dart';
import 'package:zhongdingapp/controller/registController.dart';
import 'package:zhongdingapp/controller/search_user_phone_controller.dart';
import 'package:zhongdingapp/controller/select_contacts_controller.dart';

class PutBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController(),fenix: true);
    Get.lazyPut(() => RegistController(),fenix: true);
    Get.lazyPut(() => LoginController(),fenix: true);
    Get.lazyPut(() => SelectContactsController(),fenix: true);
    Get.lazyPut(() => ChatGroupController(),fenix: true);
    Get.lazyPut(() => SearchUserPhoneController(),fenix: true);
    Get.lazyPut(() => ApplyAddFriendController(),fenix: true);
    Get.lazyPut(() => CommunicationController(),fenix: true);
  }
}
