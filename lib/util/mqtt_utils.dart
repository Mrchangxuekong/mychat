import 'dart:convert';

import 'package:get/get.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:zhongdingapp/page/chat/chat_single/chat_single_logic.dart';
import 'package:zhongdingapp/page/home/message/message/message_logic.dart';
import 'package:zhongdingapp/sqllite/bean/message_dao.dart';
import 'package:zhongdingapp/sqllite/bean/new_message_record.dart';
import 'package:zhongdingapp/sqllite/db_helper.dart';
import 'package:zhongdingapp/sqllite/table_name.dart';
import 'package:zhongdingapp/util/cache_util.dart';

import '../config/r.dart';

class Mqttutils extends GetxController {
  static MqttServerClient? _instance;
  static bool isConnect = true;

  static Future<MqttServerClient> getInstance(String clientId) async {
    if (_instance == null) {
      _instance = MqttServerClient(mqttAddress, clientId);
      // Add necessary configuration like port, username, password, etc.
      _instance!.port = 1883;
      _instance!.keepAlivePeriod = 60000;
      // Add event handlers
      _instance!.onConnected = onConnected;
      _instance!.onDisconnected = onDisconnected;
      _instance!.onSubscribed = onSubscribed;
      _instance!.onUnsubscribed = onUnsubscribed;
      _instance!.pongCallback = pong;

      // Connect to the server
      await connect();
      isConnect = true;
    }
    return _instance!;
  }

  ///该方法直接获取，因为登录的时候已经初始化过了
  static MqttServerClient getClient() {
    return _instance!;
  }

  static Future<void> connect() async {
    try {
      await _instance!.connect();
    } catch (e) {
      print('Exception: $e');
      // Handle connection errors
    }
  }

  static void onConnected() {
    print('Connected');
    //订阅已失效，需要重新订阅
    addAllSubscribed();
  }

  ///断开自动重连
  static void onDisconnected() {
    if (isConnect) {
      _instance!.connect().then((value) => onConnected());
    }
  }

  static void onSubscribed(String topic) {
    print('Subscribed to $topic');
    _instance!.updates?.listen(_onData);
  }

  static void onUnsubscribed(String? topic) {
    print('Unsubscribed from $topic');
    // Do something when unsubscribed
  }

  static void close() {
    ///断开连接
    isConnect = false;
    _instance?.disconnect();
    _instance = null;
  }

  static void pong() {}

  static Future<void> sendMessage(String message, String topic) async {
    print("-----------------_instance-------------topic--------------$topic");
    List<int> utf8Bytes = Utf8Codec().encode(message);
    var decode = utf8.decode(utf8Bytes);
    MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addUTF8String(decode);
    _instance!.publishMessage(topic, MqttQos.atMostOnce, builder.payload!);
  }

  ///消息监听
  static void _onData(List<MqttReceivedMessage<MqttMessage>> data) async {
    ChatSingleLogic baseC = Get.put(ChatSingleLogic()); // 遍历data中的每个消息
    MessageLogic messageC = Get.put(MessageLogic()); // 遍历data中的每个消息
    for (var message in data) {
      // 获取消息体
      MqttMessage mqttMessage = message.payload;
      // 处理消息
      if (mqttMessage is MqttPublishMessage) {
        // 如果是发布消息
        String topic = mqttMessage.variableHeader!.topicName;

        ///如果是创建群聊主题，则只添加订阅
        if (topic.contains(R.chatCreateGroup)) {
          String groupId = topic.replaceAll(R.chatCreateGroup, "");

          ///订阅某个群聊
          String param = "${R.chatGroup}$groupId";
          addAllSubscribed(param: param);
          return;
        }
        String payload =const Utf8Decoder().convert(mqttMessage.payload.message);
        MessageDao messageDao = MessageDao.fromJson(jsonDecode(payload));

        ///保存到本地
        messageDao.self = 0;

        ///不是自己
        messageDao.isRead = 0;

        ///标记为未读
        await DBHelper().insert(TableName.message, messageDao.toJson());
        await saveNewMessage(messageDao);

        ///本地保存发送信息*/
        baseC.updateMsgList();
        messageC.initMessage(0);
      } else if (mqttMessage is MqttSubscribeAckMessage) {
        // 如果是订阅确认消息

        print('Subscribe acknowledged');
      }
      // 添加其他消息类型的处理逻辑
    }
  }

  ///添加订阅
  static void addAllSubscribed({String? param}) async {
    if (param == null) {
      String? userId = await CacheUtil.getKey(R.userId);

      ///前期只订阅自己，后期订阅数据需要重后台获取，比如添加的群组
      var topic = "${R.mqttSingle}$userId";

      ///单聊格式,后面群聊在添加其它的操作
      print("订阅了$topic");
      topic = Uri.decodeComponent(topic);
      _instance!.subscribe(topic, MqttQos.exactlyOnce);
    } else {
      param = Uri.decodeComponent(param);
      _instance!.subscribe(param, MqttQos.exactlyOnce);
    }
  }

  static Future<void>  saveNewMessage(MessageDao e) async {
    NewMessageRecord bean = NewMessageRecord(
        userId: e.senderId,
        portrait: e.sendPortrait,
        nickName: e.sendNickName,
        content: e.content,
        createTime: e.createTime,
        isGroupMessage: e.isGroupMessage,
        isRead: 0);
    await DBHelper().insertNewMessageRecord(bean);
  }
}
