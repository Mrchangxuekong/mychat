import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zhongdingapp/util/picker.dart';
import 'package:zhongdingapp/widget/divider.dart';

enum PickType { camera, asset }

//微信底部弹出
class DuBottomSheet {
  DuBottomSheet({this.selectedAssets});

  final List<AssetEntity>? selectedAssets;

  //选择拍摄，相机资源
  Future<T?> wxPicker<T>(BuildContext context) {
    return DuPicker.showModeSheet<T>(
      context,
      child: _builAssetCamera(context),
    );
  }

  //相册、相机
  Widget _builAssetCamera(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        //拍摄
        _buildBtn(
          Text("拍摄"),
          onTap: () {
            DuPicker.showModeSheet(
              context,
              child: _builMediaImageVideo(context, pickType: PickType.camera),
            );
          },
        ),
        const DividerWidget(),

        //相册
        _buildBtn(
          Text("相册"),
          onTap: () {
            DuPicker.showModeSheet(
              context,
              child: _builMediaImageVideo(context, pickType: PickType.asset),
            );
          },
        ),
        const DividerWidget(height: 6),

        //取消
        _buildBtn(
          Text("取消"),
          onTap: () {
            Get.back();
          },
        ),
      ],
    );
  }

  //图片，视频
  Widget _builMediaImageVideo(BuildContext context,
      {required PickType pickType}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        //图片
        _buildBtn(
          const Text("图片"),
          onTap: () async {
            List<AssetEntity>? result;

            if (pickType == PickType.asset) {
              result = await DuPicker.assets(
                context: context,
                requestType: RequestType.image,
                selectedAssets: selectedAssets,
              );
            } else if (pickType == PickType.camera) {
              final asset = await DuPicker.takePhoto(context);
              if (asset == null) {
                return;
              }
              result = selectedAssets != null
                  ? [
                      ...selectedAssets!,
                      asset,
                    ]
                  : [asset];
            }

            _popRoute(context, result: result);
          },
        ),
        const DividerWidget(),

        //视频
        _buildBtn(
          Text("视频"),
          onTap: () async {
            List<AssetEntity>? result;

            if (pickType == PickType.asset) {
              result = await DuPicker.assets(
                context: context,
                requestType: RequestType.video,
                selectedAssets: selectedAssets,
                maxAssets: 1,
              );
            } else if (pickType == PickType.camera) {
              final asset = await DuPicker.takeVideo(context);
              if (asset == null) {
                return;
              }

              result = [...selectedAssets!, asset];
            }
            _popRoute(context, result: result);
          },
        ),
        const DividerWidget(height: 6),

        //取消
        _buildBtn(
          Text("取消"),
          onTap: () {
            _popRoute(context);
          },
        ),
      ],
    );
  }

  //返回
  static void _popRoute(BuildContext context, {result}) {
    Navigator.pop(context);
    Navigator.pop(context, result);
  }

  //按钮
  static InkWell _buildBtn(Widget child, {Function()? onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        height: 40,
        child: child,
      ),
    );
  }
}
