import 'dart:convert';

import 'package:crypto/crypto.dart';

class Md5Util{
  static String generateMd5(String data) {
    var content =  const Utf8Encoder().convert(data);
    var digest = md5.convert(content);
    // 这里其实就是 digest.toString()
    return digest.toString();
  }
}