import 'package:dio/dio.dart';
import 'package:zhongdingapp/util/config.dart';

class WxHttpUrl {
  static final WxHttpUrl _instance = WxHttpUrl._internal();
  factory WxHttpUrl() => _instance;
  Dio? _dio;

  WxHttpUrl._internal() {
    if (_dio == null) {
      _dio = Dio();
      _dio?.options = BaseOptions(
        baseUrl: apiUrl,
        connectTimeout: const Duration(milliseconds: 10000), // 与服务器建立连接时间10秒
        receiveTimeout: const Duration(milliseconds: 5000), // 接受数据时间5秒
        headers: {},
        contentType: 'application/json; charset=utf-8',
        responseType: ResponseType.json,
      );
    }
  }

  /// get请求
  Future<Response> get(String url, {Map<String, dynamic>? data}) async {
    Response response = await _dio!.get(url, queryParameters: data);
    return response;
  }

  /// post请求
  Future<Response> post(String url, {Map<String, dynamic>? data}) async {
    Response response = await _dio!.post(url, data: data);
    return response;
  }
}
