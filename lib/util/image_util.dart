import "dart:typed_data";

import "package:dio/dio.dart";
import "package:flutter/material.dart";
import "package:flutter/rendering.dart";
import "package:get/get.dart";
import 'package:dio/dio.dart' as dio;
import 'dart:ui' as ui show  ImageByteFormat, Image;

import "package:image_gallery_saver/image_gallery_saver.dart";

class ImageUtil{

  ///保存截图
  static  saveScreenshotImage(GlobalKey key) async{
    RenderRepaintBoundary repaintBoundary = key.currentContext!.findRenderObject() as RenderRepaintBoundary;
     ui.Image image=await repaintBoundary.toImage(pixelRatio: 3.0);//清晰度
    ByteData byteData = (await image.toByteData(format:ui.ImageByteFormat.png))!;
    Uint8List unit8 = byteData.buffer.asUint8List();
    final path=  await ImageGallerySaver.saveImage(unit8);
      String name= path['filePath'].toString().replaceAll(" content:/", "");
      return name;
  }

  ///保存网络图片到本地相册
  static  saveNetworkImage(String imgUrl) async{
    dio.Response future = await Dio().get(imgUrl,options: Options(responseType: ResponseType.bytes));
         ImageGallerySaver.saveImage(future.data);
         Get.snackbar("提示","保存成功",);
  }




}