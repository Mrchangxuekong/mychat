import 'dart:async';

import 'package:get/get.dart';
import 'package:zhongdingapp/util/bus/cov_bus.dart';

class LogicDemoLogic extends GetxController {

StreamSubscription? covariant;


  @override
  void onInit() {
    covariant=covBus.on<CovBusModel>().listen((event) {
      print("监听到了事件");
    });
  }

@override
  void dispose() {
    covariant?.cancel();
    covariant=null;
//
  }

  //使用事件监听
test(){
    covBus.fire(CovBusModel());
}
}
