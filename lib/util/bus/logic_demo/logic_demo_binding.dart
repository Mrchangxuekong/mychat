import 'package:get/get.dart';

import 'logic_demo_logic.dart';

class LogicDemoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LogicDemoLogic());
  }
}
