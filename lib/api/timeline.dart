import 'package:zhongdingapp/entity/timeline/timeline.dart';
import 'package:zhongdingapp/util/friends_add.dart';
import 'package:zhongdingapp/util/wx_http.dart';

//朋友圈api
class TimelineApi {
  /// 翻页列表
  static Future<List<TimelineModel>> pageList() async {
    var res = await WxHttpUrl().get(
      '/timeline/news',
    );

    List<TimelineModel> items = [];
    for (var item in res.data) {
      items.add(TimelineModel.fromJson(item));
    }
    return items;
  }

  //点赞
  static Future like(String id) async {
    var res = await WxHttpUrl().post(
      '/timeline/$id/like',
      data: {},
    );
    return res;
  }

  //评论
  static Future comment(String id, String content) async {
    var res = await WxHttpUrl().post(
      '/timeline/$id/comment',
      data: {
        'content': content,
      },
    );
    return res;
  }
}

class Friends_addApi {
  static Future<List<TimelineModel>> pageList() async {
    var res = await Friends_addUrl().get(
      '/apply/add',
    );

    List<TimelineModel> items = [];
    for (var item in res.data) {
      items.add(TimelineModel.fromJson(item));
    }
    return items;
  }
}
