enum SmsTypeEnum {
  //注册
  REGISTERED(code: "1",prefix:"chat:code:reg:", timeout:1),
  /// 登录
  LOGIN(code:"2", prefix:"chat:code:login:",  timeout:1),
  /// 忘记密码
  FORGET(code:"3",prefix: "chat:code:forget:",  timeout:1),
  /// PC端登录
  PC_LOGIN(code:"4", prefix:"chat:code:pclogin:", timeout: 1),
  ;
  final String code;
  final String prefix;
  final int timeout;

  const SmsTypeEnum({ required this.code, required this.prefix,required this.timeout});
}