import 'package:json_annotation/json_annotation.dart';
part 'auth_vo02.g.dart';

@JsonSerializable()
class AuthVo02 {
  /**
   * 手机号
   */
  String? phone;

  // ignore: slash_for_doc_comments
  /**
   * 密码
   */
  String? password;


  /**
   * 验证码
   */
  String? cid;

  AuthVo02({required  this.phone,required  this.password,required  this.cid});

  factory AuthVo02.fromJson(Map<String, dynamic> json) =>
      _$AuthVo02FromJson(json);

  Map<String, dynamic> toJson() => _$AuthVo02ToJson(this);
}
