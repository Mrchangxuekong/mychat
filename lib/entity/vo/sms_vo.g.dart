// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sms_vo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SmsVo _$SmsVoFromJson(Map<String, dynamic> json) => SmsVo(
      json['phone'] as String?,
      (json['type'] as num?)?.toInt(),
    );

Map<String, dynamic> _$SmsVoToJson(SmsVo instance) => <String, dynamic>{
      'phone': instance.phone,
      'type': instance.type,
    };
