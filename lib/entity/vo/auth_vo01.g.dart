// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_vo01.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthVo01 _$AuthVo01FromJson(Map<String, dynamic> json) => AuthVo01()
  ..phone = json['phone'] as String?
  ..password = json['password'] as String?
  ..nickName = json['nickName'] as String?
  ..code = json['code'] as String?;

Map<String, dynamic> _$AuthVo01ToJson(AuthVo01 instance) => <String, dynamic>{
      'phone': instance.phone,
      'password': instance.password,
      'nickName': instance.nickName,
      'code': instance.code,
    };
