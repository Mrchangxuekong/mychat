// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_vo03.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthVo03 _$AuthVo03FromJson(Map<String, dynamic> json) => AuthVo03()
  ..phone = json['phone'] as String?
  ..cid = json['cid'] as String?
  ..code = json['code'] as String?;

Map<String, dynamic> _$AuthVo03ToJson(AuthVo03 instance) => <String, dynamic>{
      'phone': instance.phone,
      'cid': instance.cid,
      'code': instance.code,
    };
