import 'package:json_annotation/json_annotation.dart';

part 'sms_vo.g.dart';
@JsonSerializable()
class SmsVo {
  /**
   * 手机号
   */
   String? phone;

  /// 短信类型 SmsTypeEnum
   int? type;
   SmsVo.def();
  SmsVo(this.phone, this.type);


  factory SmsVo.fromJson(Map<String, dynamic> json) =>
      _$SmsVoFromJson(json);

  Map<String, dynamic> toJson() => _$SmsVoToJson(this);



}
