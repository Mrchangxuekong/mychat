import 'package:json_annotation/json_annotation.dart';
part 'auth_vo01.g.dart';

@JsonSerializable()
class AuthVo01 {
  /**
     * 手机号
     */
   String? phone;

  // ignore: slash_for_doc_comments
  /**
     * 密码
     */
   String? password;

  /**
     * 昵称
     */
   String? nickName;

  /**
     * 验证码
     */
   String? code;


   AuthVo01();

  factory AuthVo01.fromJson(Map<String, dynamic> json) =>
      _$AuthVo01FromJson(json);

  Map<String, dynamic> toJson() => _$AuthVo01ToJson(this);
}
