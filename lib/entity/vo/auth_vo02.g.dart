// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_vo02.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthVo02 _$AuthVo02FromJson(Map<String, dynamic> json) => AuthVo02(
      phone: json['phone'] as String?,
      password: json['password'] as String?,
      cid: json['cid'] as String?,
    );

Map<String, dynamic> _$AuthVo02ToJson(AuthVo02 instance) => <String, dynamic>{
      'phone': instance.phone,
      'password': instance.password,
      'cid': instance.cid,
    };
