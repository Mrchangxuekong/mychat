import 'package:json_annotation/json_annotation.dart';
part 'auth_vo03.g.dart';

@JsonSerializable()
class AuthVo03 {
  /**
     * 手机号
     */
   String? phone;

  // ignore: slash_for_doc_comments
  /**
     * 密码
     */
   String? cid;

  /**
     * 验证码
     */
   String? code;


   AuthVo03();

  factory AuthVo03.fromJson(Map<String, dynamic> json) =>
      _$AuthVo03FromJson(json);

  Map<String, dynamic> toJson() => _$AuthVo03ToJson(this);
}
