// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_vo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageVO _$MessageVOFromJson(Map<String, dynamic> json) => MessageVO(
      noRead: (json['noRead'] as num?)?.toInt(),
      userId: json['userId'] as String?,
      portrait: json['portrait'] as String?,
      content: json['content'] as String?,
      nickName: json['nickName'] as String?,
      subTitle: json['subTitle'] as String?,
      dateTime: json['dateTime'] as String?,
      chatNo: json['chatNo'] as String?,
      city: json['city'] as String?,
      provinces: json['provinces'] as String?,
    );

Map<String, dynamic> _$MessageVOToJson(MessageVO instance) => <String, dynamic>{
      'portrait': instance.portrait,
      'nickName': instance.nickName,
      'subTitle': instance.subTitle,
      'dateTime': instance.dateTime,
      'userId': instance.userId,
      'chatNo': instance.chatNo,
      'provinces': instance.provinces,
      'content': instance.content,
      'city': instance.city,
      'noRead': instance.noRead,
    };
