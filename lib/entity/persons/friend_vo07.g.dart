// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend_vo07.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FriendVo07 _$FriendVo07FromJson(Map<String, dynamic> json) => FriendVo07(
      userId: json['userId'] as String?,
      nickName: json['nickName'] as String?,
      portrait: json['portrait'] as String?,
      gender: json['gender'] as String?,
      cover: json['cover'] as String?,
      chatNo: json['chatNo'] as String?,
      provinces: json['provinces'] as String?,
      city: json['city'] as String?,
      intro: json['intro'] as String?,
      isFriend: json['isFriend'] as String?,
      black: json['black'] as String?,
      source: json['source'] as String?,
    );

Map<String, dynamic> _$FriendVo07ToJson(FriendVo07 instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'nickName': instance.nickName,
      'portrait': instance.portrait,
      'gender': instance.gender,
      'cover': instance.cover,
      'chatNo': instance.chatNo,
      'provinces': instance.provinces,
      'city': instance.city,
      'intro': instance.intro,
      'isFriend': instance.isFriend,
      'black': instance.black,
      'source': instance.source,
    };
