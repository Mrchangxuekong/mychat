import 'package:flutter/animation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'friend_vo07.g.dart';

@JsonSerializable()
class FriendVo07 {
  /**
     * 用户id
     */
  String? userId;

  /**
     * 昵称
     */
  String? nickName;

  /**
     * 头像
     */
  String? portrait;

  /**
     * 性别1男0女
     */
  String? gender;

  /**
     * 封面
     */
  String? cover;

  /**
     * 微聊号
     */
  String? chatNo;

  /**
     * 省份
     */
  String? provinces;
  /**
     * 城市
     */
  String? city;

  /**
     * 介绍
     */
  String? intro;

  /**
     * 是否是好友
     */
  String? isFriend;

  /**
     * 是否黑名单
     */
  String? black = "2";

  /**
     * 好友来源
     */
  String? source;
  @JsonKey(includeFromJson: false, includeToJson: false)
  VoidCallback? onTap;

  FriendVo07.init();
  FriendVo07(
      {this.userId,
      this.nickName,
      this.portrait,
      this.gender,
      this.cover,
      this.chatNo,
      this.provinces,
      this.city,
      this.intro,
      this.isFriend,
      this.black,
      this.source,
      this.onTap});

  factory FriendVo07.fromJson(Map<String, dynamic> json) =>
      _$FriendVo07FromJson(json);

  Map<String, dynamic> toJson() => _$FriendVo07ToJson(this);
}
