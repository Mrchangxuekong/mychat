
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'message_vo.g.dart';

@JsonSerializable()
class MessageVO{
  ///头像
  String? portrait;
  ///昵称
  String? nickName;
  ///子标题
  String? subTitle;
  ///时间
  String? dateTime;
  ///用户id
  String? userId;
  ///微信号
  String? chatNo;
  ///省份
  String? provinces;

  ///省份
  String? content;

  ///城市
  String? city;
  ///几条未读信息
  int? noRead;
  ///Json 解析忽略此函数
  @JsonKey(includeFromJson: false,includeToJson: false)
   VoidCallback? onTap;
  MessageVO.init();

  MessageVO({this.onTap,this.noRead,this.userId,this.portrait,this.content,this.nickName,this.subTitle,this.dateTime,this.chatNo,this.city,this.provinces});
  factory MessageVO.fromJson(Map<String, dynamic> json) =>
      _$MessageVOFromJson(json);

  Map<String, dynamic> toJson() => _$MessageVOToJson(this);

}