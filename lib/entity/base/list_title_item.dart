import 'package:flutter/material.dart';

/// 一个url 和标题 实体
class ListTitleItem {
  String title;
  IconData? icon;
  String? titleTip;
  String? url;
  bool? line;
  final VoidCallback? dismissHandle;

  ListTitleItem({
    this.dismissHandle,
    required this.title,
    this.icon,
    this.url,
    this.line,
    this.titleTip,
  });
}
