// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginUser _$LoginUserFromJson(Map<String, dynamic> json) => LoginUser(
      userId: json['userId'] as String?,
      nickName: json['nickName'] as String?,
      intro: json['intro'] as String?,
      gender: json['gender'] as String?,
      portrait: json['portrait'] as String?,
      cover: json['cover'] as String?,
      phone: json['phone'] as String?,
      provinces: json['provinces'] as String?,
      city: json['city'] as String?,
      chatNo: json['chatNo'] as String?,
      token: json['token'] as String?,
      version: json['version'] as String?,
    );

Map<String, dynamic> _$LoginUserToJson(LoginUser instance) => <String, dynamic>{
      'userId': instance.userId,
      'nickName': instance.nickName,
      'intro': instance.intro,
      'gender': instance.gender,
      'portrait': instance.portrait,
      'cover': instance.cover,
      'phone': instance.phone,
      'provinces': instance.provinces,
      'city': instance.city,
      'chatNo': instance.chatNo,
      'token': instance.token,
      'version': instance.version,
    };
