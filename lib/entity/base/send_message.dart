import 'package:json_annotation/json_annotation.dart';

part 'send_message.g.dart';
@JsonSerializable()
class SendMessage{
  int? id;
  String? senderId;/// 发送人
  String? receiverId;/// 接收人
  String? content;/// 发送信息
  String? portrait;/// 接收人图片
  String? nickName;/// 接收人微信号
  String? msg;   ///其他消息

  SendMessage({this.id, this.senderId, this.receiverId, this.content,
      this.portrait, this.nickName, this.msg});

  /// 发送内容


  factory SendMessage.fromJson(Map<String, dynamic> json) =>
      _$SendMessageFromJson(json);

  Map<String, dynamic> toJson() => _$SendMessageToJson(this);

  static  List<SendMessage> fromJsonList(List<dynamic> jsonArray ) {
    return jsonArray.map((json) => SendMessage.fromJson(json)).toList();
  }
}