
import 'package:azlistview/azlistview.dart';
import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:zhongdingapp/util/pingyin_util.dart';

/// 一个url 和标题 实体
class ItemTitle  extends ISuspensionBean{
  String? url;
  String title;
  String? titleTop;
  IconData? icon;
  RxBool? boolSelect=RxBool(false);
  final VoidCallback dismissHandle;
  ItemTitle({required this.dismissHandle, this.url,required this.title,this.icon,  this.titleTop,this.boolSelect});

  @override
  String getSuspensionTag() {
    String tag= PingYinUtil.getFirstWordPinyin(title);
    if(!RegExp("[A-Z]").hasMatch(tag)){
      return "#";
    }
    return tag;
  }
}