// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'send_message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SendMessage _$SendMessageFromJson(Map<String, dynamic> json) => SendMessage(
      id: (json['id'] as num?)?.toInt(),
      senderId: json['senderId'] as String?,
      receiverId: json['receiverId'] as String?,
      content: json['content'] as String?,
      portrait: json['portrait'] as String?,
      nickName: json['nickName'] as String?,
      msg: json['msg'] as String?,
    );

Map<String, dynamic> _$SendMessageToJson(SendMessage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'senderId': instance.senderId,
      'receiverId': instance.receiverId,
      'content': instance.content,
      'portrait': instance.portrait,
      'nickName': instance.nickName,
      'msg': instance.msg,
    };
