// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_common_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseCommonResponse<T> _$BaseCommonResponseFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    BaseCommonResponse<T>(
      data: _$nullableGenericFromJson(json['data'], fromJsonT),
      code: (json['code'] as num?)?.toInt(),
      msg: json['msg'] as String?,
    );

Map<String, dynamic> _$BaseCommonResponseToJson<T>(
  BaseCommonResponse<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'data': _$nullableGenericToJson(instance.data, toJsonT),
      'code': instance.code,
      'msg': instance.msg,
    };

T? _$nullableGenericFromJson<T>(
  Object? input,
  T Function(Object? json) fromJson,
) =>
    input == null ? null : fromJson(input);

Object? _$nullableGenericToJson<T>(
  T? input,
  Object? Function(T value) toJson,
) =>
    input == null ? null : toJson(input);
