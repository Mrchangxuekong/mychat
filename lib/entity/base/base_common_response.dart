
import 'package:json_annotation/json_annotation.dart';

part 'base_common_response.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class BaseCommonResponse<T> {
  late T? data;
  late int? code;
  late String? msg;
  BaseCommonResponse({this.data,this.code,this.msg});
  factory BaseCommonResponse.fromJson(Map<String, dynamic> json,T Function(Object? json)  fromJsonT) =>
      _$BaseCommonResponseFromJson(json,fromJsonT);
  Map<String, dynamic> toJson(T? Function(T value) toJsonT) => _$BaseCommonResponseToJson(this, toJsonT);
}

