class SmsVo2 {
  /**
   * 手机号
   */
  String? phone;

  /// 短信类型 SmsTypeEnum
  String? type;
  SmsVo2.def();
  SmsVo2(this.phone, this.type);


  factory SmsVo2.fromJson(Map<String, dynamic> json) =>
      SmsVo2(
        json['data']['phone'].toString(),
        json['data']['type'].toString(),
      );










}