import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'login_user.g.dart';

@JsonSerializable()
class LoginUser{



  /**
   * 主键
   */
   String? userId;
  /**
   * 昵称
   */
   String? nickName;
  /**
   * 介绍
   */
   String? intro;
  /**
   * 性别1男0女
   */
   String? gender;
  /**
   * 头像
   */
   String? portrait;
  /**
   * 封面
   */
   String? cover;
  /**
   * 手机号
   */
   String? phone;
  /**
   * 省份
   */
   String? provinces;
  /**
   * 城市
   */
   String? city;
  /**
   * 微聊号
   */
   String? chatNo;


  /**
   * 用户token
   */
   String? token;
  /**
   * 版本信息
   */
   String? version;

   LoginUser({
      this.userId,
      this.nickName,
      this.intro,
      this.gender,
      this.portrait,
      this.cover,
      this.phone,
      this.provinces,
      this.city,
      this.chatNo,
      this.token,
      this.version});


   factory LoginUser.fromJson(Map<String, dynamic> json) =>
       _$LoginUserFromJson(json);

   Map<String, dynamic> toJson() => _$LoginUserToJson(this);

   static  List<LoginUser> fromJsonList(List<dynamic> jsonArray ) {
     return jsonArray.map((json) => LoginUser.fromJson(json)).toList();
   }
}