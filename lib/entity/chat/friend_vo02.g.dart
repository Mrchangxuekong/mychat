// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend_vo02.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FriendVo02 _$FriendVo02FromJson(Map<String, dynamic> json) => FriendVo02(
      userId: json['userId'] as String,
      reason: json['reason'] as String? ?? "想添加你好友",
      source: json['source'] as String? ?? "5",
    );

Map<String, dynamic> _$FriendVo02ToJson(FriendVo02 instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'reason': instance.reason,
      'source': instance.source,
    };
