import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'apply_vo02.g.dart';
@JsonSerializable()
class ApplyVo02{
  /// 主键
  String? applyId;
  /// 申请状态0无1同意2拒绝
   String? applyStatus;
  /// 申请来源
  String? applySource;
  /// 申请类型1好友2群组
  String? applyType;
  /// 理由
   String? reason;
  /// 申请时间
  String? createTime;
  /// 用户id
  String? userId;
  /// 用户头像
  String? portrait;
  /// 用户昵称
  String? nickName;

  ApplyVo02.Init();
  ApplyVo02({this.applyId, this.applyStatus, this.applySource, this.applyType,
      this.reason, this.createTime, this.userId, this.portrait, this.nickName});

  factory ApplyVo02.fromJson(Map<String, dynamic> json) =>
      _$ApplyVo02FromJson(json);

  Map<String, dynamic> toJson() => _$ApplyVo02ToJson(this);

  static  List<ApplyVo02> fromJsonList(List<dynamic> jsonArray ) {
    return jsonArray.map((json) => ApplyVo02.fromJson(json)).toList();
  }

}