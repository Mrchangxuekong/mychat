import 'package:json_annotation/json_annotation.dart';

part 'friend_vo02.g.dart';
@JsonSerializable()
class FriendVo02 {
     String userId;
     String reason;
   /*
    SCAN("1", "扫一扫"), CARD("2", "名片"),
    CHAT_NO("3", "微聊号"),
    PHONE("4", "手机号"),
    SHAKE("5", "摇一摇"),
    SYS("6", "系统"),
    GROUP("7", "群聊"),
    NEAR("8", "附近的人"),
    ;*/
     String source;

     FriendVo02({required this.userId, this.reason="想添加你好友",  this.source="5"});

     factory FriendVo02.fromJson(Map<String, dynamic> json) =>
         _$FriendVo02FromJson(json);

     Map<String, dynamic> toJson() => _$FriendVo02ToJson(this);
}