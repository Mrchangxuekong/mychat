// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_group.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatGroup _$ChatGroupFromJson(Map<String, dynamic> json) => ChatGroup(
      json['id'] as String,
      json['name'] as String,
      json['notice'] as String?,
      json['portrait'] as String,
      json['master'] as String,
      DateTime.parse(json['createTime'] as String),
    );

Map<String, dynamic> _$ChatGroupToJson(ChatGroup instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'notice': instance.notice,
      'portrait': instance.portrait,
      'master': instance.master,
      'createTime': instance.createTime.toIso8601String(),
    };
