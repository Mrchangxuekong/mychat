import 'package:json_annotation/json_annotation.dart';

part 'chat_group.g.dart';
@JsonSerializable()
class ChatGroup{

  /// 主键
   String id;
  /// 群名
   String name;
  /// 公告
   String? notice;
  /// 头像
   String portrait;
  /// 群主
   String master;
  /// 创建时间
   DateTime createTime;

   ChatGroup(this.id, this.name, this.notice, this.portrait, this.master,
      this.createTime);
   factory ChatGroup.fromJson(Map<String, dynamic> json) =>
       _$ChatGroupFromJson(json);

   Map<String, dynamic> toJson() => _$ChatGroupToJson(this);

   static  List<ChatGroup> fromJsonList(List<dynamic> jsonArray ) {
     return jsonArray.map((json) => ChatGroup.fromJson(json)).toList();
   }
}