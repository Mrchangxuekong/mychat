// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'apply_vo02.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApplyVo02 _$ApplyVo02FromJson(Map<String, dynamic> json) => ApplyVo02(
      applyId: json['applyId'] as String?,
      applyStatus: json['applyStatus'] as String?,
      applySource: json['applySource'] as String?,
      applyType: json['applyType'] as String?,
      reason: json['reason'] as String?,
      createTime: json['createTime'] as String?,
      userId: json['userId'] as String?,
      portrait: json['portrait'] as String?,
      nickName: json['nickName'] as String?,
    );

Map<String, dynamic> _$ApplyVo02ToJson(ApplyVo02 instance) => <String, dynamic>{
      'applyId': instance.applyId,
      'applyStatus': instance.applyStatus,
      'applySource': instance.applySource,
      'applyType': instance.applyType,
      'reason': instance.reason,
      'createTime': instance.createTime,
      'userId': instance.userId,
      'portrait': instance.portrait,
      'nickName': instance.nickName,
    };
