

import 'package:azlistview/azlistview.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:zhongdingapp/util/pingyin_util.dart';

/**
 * 好友对象
 */

part 'friend_vo06.g.dart';
@JsonSerializable()
class FriendVo06  extends ISuspensionBean{

  /// 用户id
   String? userId;
  /// 头像
   String? portrait;
  /// 微聊号
   String? chatNo;
  /// 昵称
   String? nickName;

   ///好友类型
   String? userType;
     ///是否被选中
   @JsonKey(includeFromJson: false,includeToJson: false)
   bool? selectItem;
   @JsonKey(includeFromJson: false,includeToJson: false)
   final VoidCallback? dismissHandle;

   FriendVo06.Init(this.dismissHandle);

   FriendVo06(
   {this.userId,this.dismissHandle, this.portrait, this.chatNo, this.nickName, this.userType,this.selectItem=false});

  factory FriendVo06.fromJson(Map<String, dynamic> json) =>
       _$FriendVo06FromJson(json);

   Map<String, dynamic> toJson() => _$FriendVo06ToJson(this);

   static  List<FriendVo06> fromJsonList(List<dynamic> jsonArray ) {
     return jsonArray.map((json) => FriendVo06.fromJson(json)).toList();
   }


   @override
   String getSuspensionTag() {
     String tag= PingYinUtil.getFirstWordPinyin(nickName!);
     if(!RegExp("[A-Z]").hasMatch(tag)){
       return "#";
     }
     return tag;
   }
}
