// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend_vo06.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FriendVo06 _$FriendVo06FromJson(Map<String, dynamic> json) => FriendVo06(
      userId: json['userId'] as String?,
      portrait: json['portrait'] as String?,
      chatNo: json['chatNo'] as String?,
      nickName: json['nickName'] as String?,
      userType: json['userType'] as String?,
    );

Map<String, dynamic> _$FriendVo06ToJson(FriendVo06 instance) =>
    <String, dynamic>{
      'isShowSuspension': instance.isShowSuspension,
      'userId': instance.userId,
      'portrait': instance.portrait,
      'chatNo': instance.chatNo,
      'nickName': instance.nickName,
      'userType': instance.userType,
    };
