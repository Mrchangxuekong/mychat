class AddModel {
  String? uid;
  String? nickname;
  String? avator;
  String? conver;

  AddModel({
    this.uid,
    this.nickname,
    this.avator,
    this.conver,
  });

  factory AddModel.fromJson(Map<String, dynamic> json) => AddModel(
        uid: json['uid'] as String?,
        nickname: json['nickname'] as String?,
      );
}
