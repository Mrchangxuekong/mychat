//用户实例
class UserModel {
  String? uid;
  String? nickname;
  String? avator;
  String? conver;

  UserModel({
    this.uid,
    this.nickname,
    this.avator,
    this.conver,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        uid: json['uid'] as String?,
        nickname: json['nickname'] as String?,
        avator: json['avator'] as String?,
        conver: json['conver'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'uid': uid,
        'nickname': nickname,
        'avator': avator,
        'conver': conver,
      };
}
