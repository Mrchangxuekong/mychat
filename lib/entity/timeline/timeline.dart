import 'package:zhongdingapp/entity/timeline/comment.dart';
import 'package:zhongdingapp/entity/timeline/like.dart';
import 'package:zhongdingapp/entity/timeline/video.dart';
import 'package:zhongdingapp/entity/user.dart';

class TimelineModel {
  String? id;
  UserModel? user;
  String? postType;
  String? content;
  String? publishDate;
  String? location;
  VideoModel? video;
  List<String>? images;
  bool? isLike;
  List<LikeModel>? likes;
  List<CommentModel>? comments;

  TimelineModel({
    this.id,
    this.user,
    this.postType,
    this.content,
    this.publishDate,
    this.location,
    this.video,
    this.images,
    this.isLike,
    this.likes,
    this.comments,
  });

  factory TimelineModel.fromJson(Map<String, dynamic> json) => TimelineModel(
        id: json['id'] as String?,
        user: json['user'] == null
            ? null
            : UserModel.fromJson(json['user'] as Map<String, dynamic>),
        postType: json['post_type'] as String?,
        content: json['content'] as String?,
        publishDate: json['publishDate'] as String?,
        location: json['location'] as String?,
        video: json['video'] == null
            ? null
            : VideoModel.fromJson(json['video'] as Map<String, dynamic>),
        images: json['images'].cast<String>(),
        isLike: json['is_like'] as bool?,
        likes: (json['likes'] as List<dynamic>?)
            ?.map((e) => LikeModel.fromJson(e as Map<String, dynamic>))
            .toList(),
        comments: (json['comments'] as List<dynamic>?)
            ?.map((e) => CommentModel.fromJson(e as Map<String, dynamic>))
            .toList(),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'user': user?.toJson(),
        'post_type': postType,
        'content': content,
        'publishDate': publishDate,
        'location': location,
        'video': video?.toJson(),
        'images': images,
        'is_like': isLike,
        'likes': likes?.map((e) => e.toJson()).toList(),
        'comments': comments?.map((e) => e.toJson()).toList(),
      };
}
