import 'package:get/get.dart';
import 'package:zhongdingapp/config/api_url.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/persons/friend_vo07.dart';
import 'package:zhongdingapp/util/http_dio.dart';
import 'package:zhongdingapp/routerHelper.dart';

class SearchUserPhoneController extends GetxController{
  late Rx<FriendVo07> friendVo07=FriendVo07.init().obs;
  var inputKeyWord="".obs;

  void searchUserByPhone()async {
    var res=await HttpDio.getInstance().post(ApiUrl.postFriendFindFriend, {"param":inputKeyWord.value});
        if(res[R.code]==200){
          friendVo07.value=FriendVo07.fromJson(res[R.data]);
          Get.toNamed(RouterHelper.addFriendBySearch,arguments: {R.source:"4"});
        }else{
          Get.snackbar("提示", res[R.msg]);
        }
  }

}