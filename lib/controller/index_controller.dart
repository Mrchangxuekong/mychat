import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/page/home/find/find_page.dart';
import 'package:zhongdingapp/page/home/me/me_page.dart';
import 'package:zhongdingapp/page/home/message/message/message_view.dart';
import 'package:zhongdingapp/page/home/phonebook/communication_page.dart';


class IndexController extends GetxController{
  var clearInput=false.obs;
  var phone="".obs;
  var currPageIndex = 0.obs; //当前页

  var list = [
    {"page":  const MessagePage(), "title": "消息"},
    {"page":  const CommunicationPage(), "title": "通讯录"},
    {"page": const FindPage(), "title": "发现"},
    {"page": const MePage(), "title": "我的"},
  ].obs;
  ///该方法仅支持ttf 文件加载
  getBottomNavigationBarItem() {
    List<BottomNavigationBarItem> list = [];
    BottomNavigationBarItem message = const BottomNavigationBarItem(
      icon: Icon(MyIcons.message),
      label: "消息",
    );
    list.add(message);
    BottomNavigationBarItem book = const BottomNavigationBarItem(
        icon: Icon(MyIcons.phoneBook), label: "通讯录");
    list.add(book);

    BottomNavigationBarItem find =
    const BottomNavigationBarItem(icon: Icon(MyIcons.find), label: "发现");
    list.add(find);
    BottomNavigationBarItem my = const BottomNavigationBarItem(
        icon: Icon(MyIcons.myicon), label: "我", tooltip: "1");
    list.add(my);
    return list;


  }


}