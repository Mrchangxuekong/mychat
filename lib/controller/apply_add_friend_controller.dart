import 'dart:convert';

import 'package:get/get.dart';
import 'package:zhongdingapp/config/api_url.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/chat/friend_vo02.dart';
import 'package:zhongdingapp/util/http_dio.dart';

class ApplyAddFriendController extends GetxController{
var check=false.obs;

  ///发送添加好友申请
  applyAdd(FriendVo02 vo02)async{
    if(vo02.reason.length>=20){
      Get.snackbar("提示", "申请理由不能超过20个字，请重新输入");
      return;
    }
    var res=await HttpDio.getInstance().post(ApiUrl.applyAdd,vo02.toJson());
    if(res[R.code]==200){
      Get.back(closeOverlays: true);
    }
      Get.snackbar("提示", res[R.msg],duration: const Duration(seconds: 1),);
  }

}