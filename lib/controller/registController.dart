import 'package:crypto/crypto.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/config/api_url.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/VO/sms_vo.dart';
import 'package:zhongdingapp/entity/vo/auth_vo01.dart';
import 'package:zhongdingapp/enums/sms_type_enum.dart';
import 'package:zhongdingapp/util/http_dio.dart';
import 'package:zhongdingapp/util/md5_util.dart';

import '../routerHelper.dart';

class RegistController extends GetxController {
  final RxBool checkBtn = false.obs;
  var isCode=false;

  bool getCheckBtn() {
    return checkBtn.value;
  }

  setCheckBtn(bool checkBtn) {
    this.checkBtn.value = checkBtn;
  }

//获取验证码
  getCode(String phone) {
    if(isCode){///防止连续点击
      return ;
    }
    isCode=true;
    //构造参数
    SmsVo smsVo = SmsVo(phone, 0);
    HttpDio.getInstance().post(ApiUrl.sendCode, smsVo.toJson()).then((res) {
      isCode=false;
        try{
          Get.snackbar("提示", res[R.msg]);
        }catch(e){
          Get.snackbar("提示", "当前无法连接服务器，请稍后重试！");
        }


    });
  }

  //注册逻辑
  registerBtn(AuthVo01 reg) {
    bool flag = false;
    String msg = "";
    do {
      if (!checkBtn.value) {
        msg = "请先勾选隐私协议";
        break;
      }
      if (reg.code == null) {
        msg = "验证码不能为空";
        break;
      }

      if (reg.nickName!.isEmpty || reg.nickName!.length > 20) {
        msg = "昵称不能为空或超出长度(20)";
        break;
      }
      if (reg.password!.isEmpty) {
        msg = "密码不能为空";
        break;
      }
      if (reg.phone!.isEmpty) {
        msg = "验证码不能为空";
        break;
      }
      flag = true;
    } while (false);
    if (!flag) {
      Get.snackbar("提示", msg);
      return;
    }
    //密码加密
    reg.password=Md5Util.generateMd5(reg.password!);
    HttpDio.getInstance().post(ApiUrl.register, reg.toJson()).then((res) {
      Get.snackbar("提示", res[R.msg]);
      if (res[R.code] == 200) {
        //注册成功，跳转登录页
        Get.toNamed(RouterHelper.login);
      }
      //fixme  该跳转可删除,调试中存在
      Get.toNamed(RouterHelper.login);
    }).whenComplete(() => {
          //回调完执行的操作
        });
  }

  ///跳转协议接口
  void goToAgreement() {
    String url = "";
    String msg = "";
    HttpDio.getInstance().get(ApiUrl.getAgreement, {}).then((res) {
      if (res[R.code] == 200) {
        url = res[R.data];
        msg = res[R.msg];
      }
    }).whenComplete(() {
      if (url.isEmpty) {
        Get.snackbar("提示", msg);
        return;
      }
      Get.toNamed(RouterHelper.agreement, arguments: {R.keyAgreement: url});
    });
  }
}
