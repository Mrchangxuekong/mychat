import 'package:dio/dio.dart';
import 'package:get/get.dart';

class PageMyController extends GetxController {
  var data = [].obs;
  Dio dio = Dio();

  @override
  void onInit() {
    super.onInit();
    // 示例坐标，根据实际情况替换或动态获取
    String exampleCoordinates = '116.397428,39.90923';

    alibabaMaps(exampleCoordinates).then((fetchedData) {
      data.value = fetchedData;
    }).catchError((error) {
      print("Error fetching data: $error");
    });
  }

  Future alibabaMaps(String latlong) async {
    String appKey = 'YOUR_APP_KEY_HERE'; // 应替换为实际的AppKey
    String url =
        'https://restapi.amap.com/v3/geocode/regeo?output=json&location=$latlong&key=$appKey&radius=1000&extensions=base';

    try {
      var response = await dio.get(url);
      return response.data;
      // ignore: deprecated_member_use
    } on DioError catch (e) {
      print("Dio error: $e");
      throw Exception("Failed to fetch data");
    }
  }
}

void updatePrivacyShow(bool hasContains, bool hasShow) {
  // _methodChannel
  //     .invokeMethod('updatePrivacyStatement', {'hasContains': hasContains, 'hasShow': hasShow});
}

void updatePrivacyAgree(bool hasAgree) {
  // _methodChannel
  //     .invokeMethod('updatePrivacyStatement', {'hasAgree': hasAgree});
}

enum GeoLanguage { DEFAULT, EN, ZH }

enum DesiredAccuracy { Best }

class AMapLocationOption {
  // 是否需要地址信息，默认true
  bool needAddress = true;

  // 逆地理信息语言类型
  late GeoLanguage geoLanguage;

  // 是否单次定位
  bool onceLocation = false;

  // 定位模式
  // AMapLocationMode locationMode;

  // Android端定位间隔，单位：毫秒
  int locationInterval = 2000;

  // iOS端是否允许系统暂停定位
  bool pausesLocationUpdatesAutomatically = false;

  // iOS端期望的定位精度
  DesiredAccuracy desiredAccuracy = DesiredAccuracy.Best;

  // iOS端定位最小更新距离，单位：米
  double distanceFilter = -1;

  // 你应该在这里定义startLocation，stopLocation，destroy和onLocationChanged的实现
  void startLocation() {
    // 实现开始定位的逻辑
  }

  void stopLocation() {
    // 实现停止定位的逻辑
  }

  void destroy() {
    // 实现销毁定位资源的逻辑
  }

  Stream<Map<String, dynamic>> onLocationChanged() {
    // 实现监听位置变化的逻辑
    // 示例：
    // return _locationPlugin.onLocationChanged();
    return Stream.empty(); // 返回一个空的Stream作为示例
  }
}

// 示例：如何使用onLocationChanged
void listenLocationChanges() {
  AMapLocationOption locationOption = AMapLocationOption();
  locationOption.onLocationChanged().listen((Map<String, dynamic> result) {
    // result即为定位结果
  });
}
