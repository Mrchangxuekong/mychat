import 'dart:collection';

import 'package:azlistview/azlistview.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/color/my_icons.dart';
import 'package:zhongdingapp/config/api_url.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/chat/friend_vo06.dart';
import 'package:zhongdingapp/util/http_dio.dart';

import 'homeController.dart';

class CommunicationController extends GetxController{
  var defaultList =<FriendVo06> [].obs;
 var dateList = <FriendVo06>[].obs;
 var kIndexBarData= <String>[].obs;
  var getC=Get.find<HomeController>();

@override
  void onInit() {
  defaultList.add(FriendVo06(nickName: "仅聊天的朋友", portrait:MyIcons.networkImg1 , dismissHandle: () {  }));
  Future.wait([fetchData()]).then((List values) {

    dateList.value=values[0];

    // 处理获取的数据
    SuspensionUtil.sortListBySuspensionTag(dateList.value);
    SuspensionUtil.setShowSuspensionStatus(dateList.value);
    for (int i = 0; i < dateList.value.length; i++) {
      String tag=dateList.value[i].getSuspensionTag();
      if(!kIndexBarData.value.contains(tag)){
        kIndexBarData.value.add(tag);
      }
    }
    ///将#号移动到最后
    if(kIndexBarData.contains("#")){
      kIndexBarData.value.remove("#");
      kIndexBarData.value.add("#");
    }
    SuspensionUtil.sortListBySuspensionTag(dateList.value);
    SuspensionUtil.setShowSuspensionStatus(dateList.value);
    update();
  });

}


///异步获取好友列表
  Future<List<FriendVo06>> fetchData() async {
    var res =await HttpDio.getInstance().post(ApiUrl.getFriendFriendList, {});
    if (res[R.code] == 200) {
     return Future.value(FriendVo06.fromJsonList(res[R.data]));
    }else{
      Get.snackbar("提示",res[R.msg] );
      return  Future.value([]);
    }
  }




}