import 'package:get/get.dart';
import 'package:zhongdingapp/config/api_url.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/chat/apply_vo02.dart';
import 'package:zhongdingapp/page/chat/chat_single/chat_single_logic.dart';
import 'package:zhongdingapp/page/home/find/find_page.dart';
import 'package:zhongdingapp/page/home/me/me_page.dart';
import 'package:zhongdingapp/page/home/me/me_page/me_page_logic.dart';
import 'package:zhongdingapp/page/home/me/me_page/me_page_view.dart';
import 'package:zhongdingapp/page/home/message/message/message_view.dart';
import 'package:zhongdingapp/page/home/phonebook/communication_page.dart';
import 'package:zhongdingapp/sqllite/bean/message_dao.dart';
import 'package:zhongdingapp/sqllite/bean/new_message_record.dart';
import 'package:zhongdingapp/sqllite/db_helper.dart';
import 'package:zhongdingapp/sqllite/table_name.dart';
import 'package:zhongdingapp/util/http_dio.dart';

class HomeController extends GetxController {
  var title = "".obs;
  setTitle(String title) {
    this.title = title.obs;
  }
  ///新的朋友的数量
  var newFriendNum=0.obs;
  ///申请列表
  var applyVo02List = <ApplyVo02>[].obs;
  ///消息列表
  var messageDaoList = <NewMessageRecord>[].obs;

  @override
  void onInit() {
    super.onInit();

  }
  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    getInitData(0);
  }

  var currPageIndex = 0.obs;

  var list = [
    {"page":  const MessagePage(), "title": "消息"},
    {"page":  const CommunicationPage(), "title": "通讯录"},
    {"page": const FindPage(), "title": "发现"},
    {"page": const MePagePage(), "title": "我的"},
  ].obs;

  String getTitle() {
    return title.value;
  }
  getRedFriendNum(){
   if(applyVo02List.isEmpty){
     newFriendNum.value=0;
   }else{
     newFriendNum.value=applyVo02List.where((p0) => p0.applyStatus=="0").length;
   }

  }

  ///初始化数据并跳转页面
  void getInitData(int currIndex) {
    switch (currIndex) {
      case 0:
        initMessage(currIndex);
        break;
      case 1:
        initCommunication(currIndex);
        break;
      case 2:
        currPageIndex.value = currIndex;
        update();
        break;
      case 3:
        initMePagePage(currIndex);
        break;
    }
    currPageIndex.value = currIndex;
  }

  void initCommunication(int currIndex) {
    HttpDio.getInstance().get(ApiUrl.getApplyList, {}).then((res) {
      if (res[R.code] == 200) {
        if(res[R.rows]!=""){///无列表
          applyVo02List.value = ApplyVo02.fromJsonList(res[R.rows]);
          getRedFriendNum();
        }
        currPageIndex.value = currIndex;
        update();

      }
    }).whenComplete(() => {
          //回调完执行的操作
        }).onError((error, stackTrace){
          Get.snackbar("提示", "网络连接失败！");
    });
  }
  ///同意添加好友接口
  void agreeAddFriend(ApplyVo02 e) {
    HttpDio.getInstance().post(ApiUrl.agreeAddFriend, {"applyId":e.applyId}).then((res) {
      if (res[R.code] == 200) {
        initCommunication(1);
      }else{
        Get.snackbar("提示", res[R.msg]);
      }
    }).whenComplete(() => {
      //回调完执行的操作
    });

  }

  void initMessage(int currIndex)async {
        getRedFriendNum();
        String sql="";
        var queryByCurrUserId = await DBHelper().queryAll(TableName.newMessageRecord);
        messageDaoList.value=NewMessageRecord.fromJsonList(queryByCurrUserId);
        currPageIndex.value = currIndex;
        update();


  }
  //初始化我的页面
  void initMePagePage(int currIndex)  {
    final logic = Get.put(MePageLogic());
     logic.onReady();
    currPageIndex.value = currIndex;
    update();
  }

}
