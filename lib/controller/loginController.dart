// ignore: file_names
import 'dart:convert';

import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:zhongdingapp/config/api_url.dart';
import 'package:zhongdingapp/config/r.dart';
import 'package:zhongdingapp/entity/base/login_user.dart';
import 'package:zhongdingapp/entity/vo/auth_vo02.dart';
import 'package:zhongdingapp/entity/vo/auth_vo03.dart';
import 'package:zhongdingapp/util/cache_util.dart';
import 'package:zhongdingapp/util/http_dio.dart';
import 'package:zhongdingapp/util/md5_util.dart';
import 'package:zhongdingapp/util/mqtt_utils.dart';
import 'package:zhongdingapp/routerHelper.dart';

class LoginController extends GetxController {
  final  checkBtn = false.obs;
  final  loginType = true.obs;
  final  longinTip = "密码登录".obs;
  final  phoneType = "密码".obs;
  final  hintType = "请输入密码".obs;
  final MqttServerClient client = MqttServerClient("","");
  bool getCheckBtn() {
    return checkBtn.value;
  }

  setCheckBtn(bool checkBtn) {
    this.checkBtn.value = checkBtn;
  }

  //登录
  void login(AuthVo02 item) {
    bool flag = false;
    String msg = "";
    do {
      if(!checkBtn.value){
        msg = "请先勾选隐私协议";
        break;
      }
      if (item.password!.isEmpty) {
        msg = "密码不能为空";
        break;
      }
      if (item.phone!.isEmpty) {
        msg = "验证码不能为空";
        break;
      }
      flag = true;
    } while (false);
    if(!flag){
      Get.snackbar("提示", msg);
      return;
    }
    //密码加密
    item.password=Md5Util.generateMd5(item.password!);
    HttpDio.getInstance().post(ApiUrl.login, item.toJson()).then((res) async {
      if(res[R.code]==200){
        //登录成功，跳转登录页
        Map<String,dynamic> user=res[R.data]["user"];
        LoginUser login=LoginUser.fromJson(user);
        CacheUtil.putUser(jsonEncode(user));
        CacheUtil.addKey(R.token, login.token!);
        CacheUtil.addKey(R.userId, login.userId!);
        //mqtt初始化
        await Mqttutils.getInstance(login.userId!);
        ///添加订阅
        Mqttutils.addAllSubscribed();

        Get.toNamed(RouterHelper.home);
      }else{
        Get.snackbar("提示", res[R.msg]);
      }
    }).whenComplete(() => {
      //回调完执行的操作
    });

  }
  ///切换登录状态
  void updateLoginType() {
    loginType.value=!loginType.value;
    longinTip.value=loginType.value?"密码登录":"使用验证码登录";
    phoneType.value=loginType.value?"密码":"验证码";
    hintType.value=loginType.value?"请输入密码":"请输入验证码";
  }

  void loginByCode(AuthVo03 authVo03) {
    HttpDio.getInstance().post(ApiUrl.loginByCode, authVo03.toJson()).then((res) async {
      if(res.code==200){
        //登录成功，跳转登录页
        var box = await Hive.openBox(constHiveBox);
        box.put(R.token,  res.data[R.token]);

        Get.toNamed(RouterHelper.home);
      }else{
        Get.snackbar("提示", res.msg!);
      }
    }).whenComplete(() => {
      //回调完执行的操作
    });

  }
}
