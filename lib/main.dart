import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:zhongdingapp/CustomAssetsPicker.dart';
import 'package:zhongdingapp/config/app_context.dart';
import 'package:zhongdingapp/global.dart';
import 'package:zhongdingapp/util/put_binding.dart';
import 'package:zhongdingapp/routerHelper.dart';

//FLUTTER_STORAGE_BASE_URL
void main() {
  AppContext.init().then((value) => runApp(const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      navigatorObservers: [Global.routeObserver],
      debugShowCheckedModeBanner: false,
      initialBinding: PutBinding(),
      initialRoute: RouterHelper.initial,
      getPages: RouterHelper.routes,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,

        //设置底部弹出框的背景色为透明
        bottomSheetTheme: const BottomSheetThemeData(
          backgroundColor: Colors.transparent,
        ),
      ),
      /* locale: const Locale("zh", "CN"), //设置当前区域
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        CustomAssetsPickerTextDelegate(), //自定义处理
      ],
      supportedLocales: const <Locale>[
        /*  Locale('en'), */
        Locale('zh'),
        // 在此处添加应用支持的语言
      ], */
      // 设置语言
      localizationsDelegates: const [
        GlobalMaterialLocalizations
            .delegate, //是Flutter的一个本地化委托，用于提供Material组件库的本地化支持
        GlobalWidgetsLocalizations.delegate, //用于提供通用部件（Widgets）的本地化支持
        GlobalCupertinoLocalizations.delegate, //用于提供Cupertino风格的组件的本地化支持
      ],
      supportedLocales: const [
        Locale('zh', 'CN'), //设置语言为中文
      ],
    );
  }
}
