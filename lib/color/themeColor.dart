import 'dart:ui';

class ThemeColor {
  static const Color gray = Color.fromRGBO(248, 248, 248, 1);
  static const Color gray10 = Color.fromRGBO(248, 248, 248, 1);
  static const Color gray200 = Color.fromRGBO(79, 78, 78, 1.0);
  static const Color appbarBackground = Color.fromRGBO(224, 221, 221, 1.0);
  static const Color mainGreen = Color.fromRGBO(5, 193, 96, 1);
  static const Color hintText = Color.fromRGBO(197, 197, 197, 1);
  static const Color whiteText = Color.fromRGBO(247, 245, 245, 1);
  static const Color white = Color.fromRGBO(247, 245, 245, 1);
  static const Color black1 = Color.fromRGBO(21, 20, 20, 1);
  static const Color blue = Color.fromRGBO(62, 89, 199, 1);
  static const Color black2 = Color.fromRGBO(231, 227, 227, 1.0);
  static const Color hintText20 = Color.fromRGBO(157, 157, 157, 1.0);

  static const Color gray20 = Color.fromRGBO(241, 239, 239, 1.0);
  static const Color gray25 = Color.fromRGBO(215, 213, 213, 1.0);
  static const Color gray50 = Color.fromRGBO(192, 190, 190, 1.0);
  static const Color gray100 = Color.fromRGBO(136, 136, 136, 1.0);
  static const Color green100 = Color.fromRGBO(178, 232, 204, 1.0);
  static const Color green200 = Color.fromRGBO(86, 199, 141, 1.0);
  static const Color green300 = Color.fromRGBO(75, 245, 157, 1.0);

  static const Color black100 = Color.fromRGBO(21, 20, 20, 1);
  static const Color line = Color.fromRGBO(211, 209, 209, 1.0);
  static const Color black50 = Color.fromRGBO(199, 194, 194, 1.0);
  static const Color grayLine100 = Color.fromRGBO(203, 201, 201, 1.0);
  static const Color yellow = Color.fromRGBO(253, 201, 1, 1.0);
  static const Color grayTopBackground = Color.fromRGBO(181, 183, 189, 1.0);
  static const Color red500 = Color.fromRGBO(241, 34, 34, 1.0);

}
