import 'package:flutter/material.dart';

class MyIcons {
  //消息
  static const IconData message = IconData(0xe68d, fontFamily: "myiconfont");

  //通讯录
  static const IconData phoneBook = IconData(0xe655, fontFamily: "myiconfont");

  //发现
  static const IconData find = IconData(0xe636, fontFamily: "myiconfont");

  //我
  static const IconData myicon = IconData(0xe758, fontFamily: "myiconfont");

  //二维码手势
  static const IconData codeHead = IconData(0xe881, fontFamily: "myiconfont");

  //二维码小图标
  static const IconData codepick = IconData(0xe642, fontFamily: "myiconfont");

  //添加群聊
  static const IconData addGroup = IconData(0xe605, fontFamily: "myiconfont");

  //添加朋友
  static const IconData addfriend = IconData(0xe624, fontFamily: "myiconfont");

  //新的朋友
  static const IconData newfriend = IconData(0xe600, fontFamily: "myiconfont");

  //发起群聊
  static const IconData addGroup1 = IconData(0xe605, fontFamily: "myiconfont");

  //设置
  static const IconData settion = IconData(0xe654, fontFamily: "myiconfont");

  //小红点
  static const IconData redPoint = IconData(0xee616, fontFamily: "myiconfont");

  ///键盘
  static const IconData keyboard = IconData(0xeac2, fontFamily: "myiconfont");

  ///语音
  static const IconData speech = IconData(0xe8c0, fontFamily: "myiconfont");
  static const IconData speech1 = IconData(0xe813, fontFamily: "myiconfont");

  ///微笑表情
  static const IconData smilingFace =
      IconData(0xe602, fontFamily: "myiconfont");

  static const IconData add = IconData(0xe622, fontFamily: "myiconfont");

  ///添加带框
  static const IconData addRound = IconData(0xe614, fontFamily: "myiconfont");

  ///添加
  static const IconData addDefault = IconData(0xe65b, fontFamily: "myiconfont");
  //左箭头
  static const IconData left = IconData(0xe627, fontFamily: "myiconfont");
  static const IconData leftMain = IconData(0xe625, fontFamily: "myiconfont");
  static const IconData left1 = IconData(0xe60f, fontFamily: "myiconfont");
  static const IconData left2 = IconData(0xe621, fontFamily: "myiconfont");
  static const IconData more = IconData(0xe73a, fontFamily: "myiconfont");

  ///铃铛
  static const IconData bell = IconData(0xe62e, fontFamily: "myiconfont");

  ///相册
  static const IconData album = IconData(0xe7be, fontFamily: "myiconfont");

  ///拍摄
  static const IconData shot = IconData(0xe615, fontFamily: "myiconfont");

  ///视频
  static const IconData video = IconData(0xe686, fontFamily: "myiconfont");

  ///红包
  static const IconData envelope = IconData(0xe656, fontFamily: "myiconfont");

  ///转账
  static const IconData transfer = IconData(0xe6b0, fontFamily: "myiconfont");

  ///五角星（收藏）
  static const IconData pentagram = IconData(0xe609, fontFamily: "myiconfont");

  ///朋友圈
  static const IconData friendCircle =
      IconData(0xe8b4, fontFamily: "myiconfont");

  ///位置
  static const IconData position = IconData(0xe62a, fontFamily: "myiconfont");

  static const String networkImg1 =
      "https://tenfei04.cfp.cn/creative/vcg/800/new/VCG41N1210205351.jpg";
  static const String networkImg2 =
      "https://q5.itc.cn/q_70/images03/20240402/3a1b55723376460494a5e0011a9328a2.jpeg";
  static const String networkImg3 =
      "https://up.enterdesk.com/2021/edpic_360_360/ed/4a/28/ed4a2866314a54ee84bc9e9e15ec3b4c_1.jpg";
  static const String networkImg4 =
      "https://tenfei04.cfp.cn/creative/vcg/800/new/VCG41N666346290.jpg";

  static const TextStyle msgTime =
      TextStyle(color: Color(0xffA6A6A6), fontSize: 14);

  ///聊天文字样式（自己）
  static const TextStyle chatSelf =
      TextStyle(color: Color(0xff828282), fontSize: 13);

  ///聊天文字样式（别人）
  static const TextStyle chatNoSelf =
      TextStyle(color: Colors.white, fontSize: 13);

  ///视频号
  static const IconData videoID = IconData(0xe670, fontFamily: "myiconfont");

  ///扫一扫
  static const IconData scanit = IconData(0xe604, fontFamily: "myiconfont");

  ///看一看
  static const IconData takelook = IconData(0xe61f, fontFamily: "myiconfont");

  ///购物
  static const IconData shopping = IconData(0xe603, fontFamily: "myiconfont");

  ///小程序
  static const IconData applet = IconData(0xe618, fontFamily: "myiconfont");

  ///游戏
  static const IconData game = IconData(0xe61e, fontFamily: "myiconfont");

  ///收藏
  static const IconData collect = IconData(0xe601, fontFamily: "myiconfont");

  ///表情
  static const IconData expression = IconData(0xe602, fontFamily: "myiconfont");

  ///我的朋友圈  相册
  static const IconData albumclick = IconData(0xe7be, fontFamily: "myiconfont");

  ///我的服务
  static const IconData giveservice =
      IconData(0xe62c, fontFamily: "myiconfont");
}
